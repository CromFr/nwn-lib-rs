#![allow(incomplete_features)]
#![feature(generic_const_exprs)]

use std::{
    fs::{read, write},
    io::Read,
    path::Path,
    process::exit,
};

use clap::Parser;
use nwn_lib_rs::gff;
use nwn_lib_rs::gff::bin_gff;
use nwn_lib_rs::gff::Gff;

use base64ct::{Base64, Encoding};

mod cli;
use cli::*;

enum GffPathNodeMut<'a> {
    Struct(&'a mut gff::Struct),
    FieldValue(&'a mut gff::FieldValue),
}
fn get_gff_node_mut<'a>(gff: &'a mut Gff, path: &[String]) -> Result<GffPathNodeMut<'a>, String> {
    let mut target = GffPathNodeMut::Struct(&mut gff.root);
    for (ipn, path_node) in path.iter().enumerate() {
        match target {
            GffPathNodeMut::Struct(gff_struct) => {
                let field_value = gff_struct.find_mut(path_node).ok_or(format!(
                    "Could not find field {:?} at path {:?}",
                    path_node,
                    &path[..ipn].join("."),
                ))?;
                target = GffPathNodeMut::FieldValue(field_value);
            }
            GffPathNodeMut::FieldValue(gff_field) => match gff_field {
                gff::FieldValue::List(l) => {
                    let mut index = path_node.parse::<i64>().map_err(|e| {
                        format!(
                            "List at path {:?} can only be indexed with integers: {}",
                            &path[..ipn].join("."),
                            e
                        )
                    })?;

                    if index < 0 {
                        index = l.len() as i64 - index;
                    }

                    if index < 0 || index >= l.len() as i64 {
                        return Err(format!(
                            "List at path {:?} cannot be indexed with value {}. List length is {}",
                            &path[..ipn].join("."),
                            index,
                            l.len(),
                        ));
                    }

                    target = GffPathNodeMut::Struct(&mut l[index as usize]);
                }
                gff::FieldValue::Struct(s) => {
                    let field_value = s.find_mut(path_node).ok_or(format!(
                        "Could not find field {:?} at path {:?}",
                        path_node,
                        &path[..ipn].join("."),
                    ))?;
                    target = GffPathNodeMut::FieldValue(field_value);
                }
                _ => {
                    return Err(format!(
                        "Field at path {:?} of type {:?} cannot not contain any sub-value",
                        &path[..ipn].join("."),
                        gff_field.get_type_str(),
                    ))
                }
            },
        }
    }
    Ok(target)
}

fn main() {
    let mut args = Args::parse();

    // if let Some(shell) = args.generate_completions {
    //     generate(
    //         shell,
    //         &mut Args::command(),
    //         "nwn-gff",
    //         &mut std::io::stdout(),
    //     );
    //     return;
    // }

    if args.in_place {
        args.output = Some(args.input.clone());
        args.output_format = match args.input_format {
            InputFormat::Auto => OutputFormat::Auto,
            InputFormat::Gff => OutputFormat::Gff,
            InputFormat::Json => OutputFormat::Json,
            InputFormat::Yaml => OutputFormat::Yaml,
        }
    }

    // Detect output formats
    if matches!(args.output_format, OutputFormat::Auto) {
        if let Some(file_path) = &args.output {
            match OutputFormat::guess(file_path) {
                Some(t) => args.output_format = t,
                None => {
                    eprintln!("Cannot guess output file format. Try providing -O argument.");
                    exit(1);
                }
            }
        } else {
            args.output_format = OutputFormat::Text;
        }
    }

    // Read input
    let mut gff = || -> Result<Gff, Box<dyn std::error::Error>> {
        let format = if matches!(args.input_format, InputFormat::Auto) {
            InputFormat::guess(&args.input).ok_or::<Box<dyn std::error::Error>>(
                format!("Cannot guess format for file {:?}", args.input).into(),
            )?
        } else {
            args.input_format
        };

        let data = if args.input == Path::new("-") {
            let mut buf = Vec::<u8>::new();
            std::io::stdin().read_to_end(&mut buf)?;
            buf
        } else {
            read(&args.input)?
        };

        match format {
            InputFormat::Auto => panic!(),
            InputFormat::Gff => {
                if let OutputFormat::Dump = &args.output_format {
                    // Shortcut for dumping GFF files directly, without instantiating a dynamic GFF tree
                    let (_, bingff) = bin_gff::Gff::from_bytes(&data)?;
                    let data = bingff.dump().into_bytes();

                    let output = args.output.clone().unwrap_or("-".into());
                    let write_res = if output != Path::new("-") {
                        write(output, data)
                            .map_err(|err| Box::new(err) as Box<dyn std::error::Error>)
                    } else {
                        use std::io::Write;
                        std::io::stdout()
                            .write(&data)
                            .map(|_res| ())
                            .map_err(|err| Box::new(err) as Box<dyn std::error::Error>)
                    };

                    write_res.unwrap_or_else(|err| {
                        eprintln!("Failed to serialize data: {}", err);
                        exit(1);
                    });
                    exit(0);
                }

                let (_, gff) = Gff::from_bytes(&data)?;
                Ok(gff)
            }
            InputFormat::Json => serde_json::from_slice::<Gff>(&data)
                .map_err(|err| Box::new(err) as Box<dyn std::error::Error>),
            InputFormat::Yaml => serde_yml::from_slice::<Gff>(&data)
                .map_err(|err| Box::new(err) as Box<dyn std::error::Error>),
        }
    }()
    .unwrap_or_else(|err| {
        eprintln!("Failed to parse GFF: {}", err);
        exit(1);
    });

    || -> Result<(), Box<dyn std::error::Error>> {
        for set in args.set_yaml {
            match get_gff_node_mut(&mut gff, &set.path)? {
                GffPathNodeMut::Struct(s) => {
                    *s = serde_yml::from_str::<gff::Struct>(&set.value)?;
                }
                GffPathNodeMut::FieldValue(f) => {
                    *f = serde_yml::from_str::<gff::FieldValue>(&set.value)?;
                }
            }
        }
        for set in args.set {
            match get_gff_node_mut(&mut gff, &set.path)? {
                GffPathNodeMut::Struct(_) => {
                    return Err(format!(
                        "GFF Value at path {} is a struct and must be set using --set-yaml",
                        set.path.join(" ")
                    )
                    .into());
                }
                GffPathNodeMut::FieldValue(f) => match f {
                    gff::FieldValue::Byte(v) => *v = set.value.parse::<u8>()?,
                    gff::FieldValue::Char(v) => *v = set.value.parse::<i8>()?,
                    gff::FieldValue::Word(v) => *v = set.value.parse::<u16>()?,
                    gff::FieldValue::Short(v) => *v = set.value.parse::<i16>()?,
                    gff::FieldValue::DWord(v) => *v = set.value.parse::<u32>()?,
                    gff::FieldValue::Int(v) => *v = set.value.parse::<i32>()?,
                    gff::FieldValue::DWord64(v) => *v = set.value.parse::<u64>()?,
                    gff::FieldValue::Int64(v) => *v = set.value.parse::<i64>()?,
                    gff::FieldValue::Float(v) => *v = set.value.parse::<f32>()?,
                    gff::FieldValue::Double(v) => *v = set.value.parse::<f64>()?,
                    gff::FieldValue::String(v) => *v = set.value,
                    gff::FieldValue::ResRef(v) => *v = set.value,
                    gff::FieldValue::LocString(v) => {
                        // TODO: check if correct behaviour
                        v.strref = u32::MAX;
                        v.strings = vec![(0, set.value.to_string())];
                    }
                    gff::FieldValue::Void(v) => {
                        *v = Base64::decode_vec(&set.value).map_err(|e| e.to_string())?;
                    }
                    gff::FieldValue::Struct(_) | gff::FieldValue::List(_) => {
                        return Err(format!(
                            "GFF value at path {} is a {} and must be set using --set-yaml",
                            set.path.join(" "),
                            f.get_type_str()
                        )
                        .into());
                    }
                },
            }
        }

        Ok(())
    }()
    .unwrap_or_else(|err| {
        eprintln!("Failed to modify GFF: {}", err);
        exit(1);
    });

    // Serialize data
    || -> Result<(), Box<dyn std::error::Error>> {
        let data: Vec<u8> = match args.output_format {
            OutputFormat::Auto => panic!(),
            OutputFormat::Gff => gff.to_bytes(),
            OutputFormat::Json => serde_json::to_string_pretty(&gff)?.into_bytes(),
            OutputFormat::JsonMini => serde_json::to_string(&gff)?.into_bytes(),
            OutputFormat::Yaml => serde_yml::to_string(&gff)?.into_bytes(),
            OutputFormat::Text => gff.to_string_pretty().into_bytes(),
            OutputFormat::Dump => gff.to_bin_gff().dump().as_bytes().to_vec(),
        };

        let output = args.output.unwrap_or("-".into());
        if output != Path::new("-") {
            write(output, data).map_err(|err| Box::new(err) as Box<dyn std::error::Error>)
        } else {
            use std::io::Write;
            std::io::stdout()
                .write_all(&data)
                .map(|_res| ())
                .map_err(|err| Box::new(err) as Box<dyn std::error::Error>)?;
            Ok(())
        }
    }()
    .unwrap_or_else(|err| {
        eprintln!("Failed to serialize data: {}", err);
        exit(1);
    });
}

#[test]
fn verify_cli() {
    use clap::CommandFactory;
    Args::command().debug_assert()
}
