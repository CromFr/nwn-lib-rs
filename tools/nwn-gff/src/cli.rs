use std::path::{Path, PathBuf};

use clap::Parser;

#[derive(Debug, Clone, clap::ValueEnum)]
pub enum InputFormat {
    /// Detect based on file extension
    Auto,
    /// Binary GFF file format
    Gff,
    /// JSON
    Json,
    /// YAML
    Yaml,
}
impl InputFormat {
    pub fn guess(file_path: &Path) -> Option<Self> {
        let extension = file_path
            .extension()?
            .to_ascii_lowercase()
            .into_string()
            .ok()?;
        match extension.as_str() {
            "gff"
            | "are"|"gic"|"git" //areas
            | "dlg" //dialogs
            | "fac"|"ifo"|"jrl" //module files
            | "cam" //campaign files
            | "bic" //characters
            | "ult"|"upe"|"usc"|"utc"|"utd"|"ute"|"uti"|"utm"|"utp"|"utr"|"uts"|"utt"|"utw"|"pfb" //blueprints
              => Some(Self::Gff),
            "json" => Some(Self::Json),
            "yaml" | "yml" => Some(Self::Yaml),
            _ => None,
        }
    }
}
#[derive(Debug, Clone, clap::ValueEnum)]
pub enum OutputFormat {
    /// Detect based on file extension
    Auto,
    /// Binary GFF file format
    Gff,
    /// Beautiful JSON
    Json,
    /// Minified JSON
    JsonMini,
    /// YAML
    Yaml,
    /// Compact user-readable format
    Text,
    /// Internal GFF file data structure, to help debugging parsers
    Dump,
}
impl OutputFormat {
    pub fn guess(file_path: &Path) -> Option<Self> {
        if file_path == Path::new("-") {
            return Some(Self::Text);
        }
        let extension = file_path
            .extension()?
            .to_ascii_lowercase()
            .into_string()
            .ok()?;
        match extension.as_str() {
            "gff"
            |"are"|"gic"|"git" // areas
            |"dlg" // dialogs
            |"fac"|"ifo"|"jrl" // module files
            |"cam" // campaign files
            |"bic" // characters
            |"ult"|"upe"|"usc"|"utc"|"utd"|"ute"|"uti"|"utm"|"utp"|"utr"|"uts"|"utt"|"utw"|"pfb" // blueprints
            => Some(Self::Gff),
            "json" => Some(Self::Json),
            "yaml" | "yml" => Some(Self::Yaml),
            "txt" => Some(Self::Text),
            "dump" => Some(Self::Dump),
            _ => None,
        }
    }
}

/// Read and write Neverwinter Nights TLK files
#[derive(Parser, Debug)]
#[command(author, version=nwn_lib_rs::NWN_LIB_RS_VERSION, about, long_about = None)]
pub struct Args {
    /// Input file. '-' for reading from stdin
    #[arg()]
    pub input: PathBuf,
    /// Input file format.
    ///
    /// 'auto' to guess based on the file extension
    #[arg(short = 'I', long, default_value = "auto")]
    pub input_format: InputFormat,

    /// Output file. Omit or set to '-' for writing to stdout
    #[arg(short, long)]
    pub output: Option<PathBuf>,
    /// Output file format.
    ///
    /// 'auto' to guess based on the file extension
    #[arg(short = 'O', long, default_value = "auto")]
    pub output_format: OutputFormat,

    /// Edit the file in-place, keeping the same file name and file format
    #[arg(long, conflicts_with = "output", conflicts_with = "output_format")]
    pub in_place: bool,

    /// Set an existing GFF value
    ///
    /// Example: --set ACLtArm.Accessory=32
    /// See --help-set for more information
    #[arg(short, long, value_parser = parse_set_args)]
    pub set: Vec<SetArg>,

    /// Set a GFF value defined in YAML or JSON format for setting more complex values or changing value types
    ///
    /// Example: --set-yaml 'ACLtArm.Accessory={"type": "ExoString", "value": "Hello"}'
    /// See --help-set for more information
    #[arg(short = 'S', long, value_parser = parse_set_args)]
    pub set_yaml: Vec<SetArg>,
}

#[derive(Debug, Clone)]
pub struct SetArg {
    pub path: Vec<String>,
    pub value: String,
}

fn parse_set_args(s: &str) -> Result<SetArg, clap::Error> {
    let (path, value) = s.split_once('=').ok_or(clap::Error::raw(
        clap::error::ErrorKind::ValueValidation,
        "Cannot find '=' character",
    ))?;

    let path = path.split('.').map(|s| s.to_string()).collect::<Vec<_>>();

    Ok(SetArg {
        path,
        value: value.to_string(),
    })
}
