#!/bin/bash
set -euo pipefail

function chksum() {
	sha1sum "$1" | cut -d' ' -f1
}

function fix_krogar() {
	if [[ "$1" == "unittest/krogar.bic" ]]; then
		printf '\xff\xff\xff\xff' | dd of=/tmp/nwn-lib-rs-gff.gff seek=57708 bs=1 count=4 conv=notrunc
	fi
}

set -x

for INPUT in unittest/{nwn1_towncrier.utc,nwn2_doge.utc,krogar.bic}; do
	GFF_SHASUM=$(chksum "$INPUT")

	nwn-gff "$INPUT" > /dev/null

	# GFF ser/deser
	rm -f /tmp/nwn-lib-rs-gff.gff
	nwn-gff "$INPUT" -o /tmp/nwn-lib-rs-gff.gff
	fix_krogar "$INPUT"
	[[ "$(chksum /tmp/nwn-lib-rs-gff.gff)" == "$GFF_SHASUM" ]]

	rm -f /tmp/nwn-lib-rs-gff.gff
	# nwn-gff "$INPUT" -O gff -o /tmp/test-o
	# nwn-gff "$INPUT" -O gff > /tmp/test-stdout
	# sync
	nwn-gff "$INPUT" -O gff | nwn-gff - -I gff -o /tmp/nwn-lib-rs-gff.gff
	fix_krogar "$INPUT"
	[[ "$(chksum /tmp/nwn-lib-rs-gff.gff)" == "$GFF_SHASUM" ]]

	# Json ser/deser
	for FMT in json json-mini; do
		rm -f /tmp/nwn-lib-rs-gff.{json,gff}

		nwn-gff "$INPUT" -o /tmp/nwn-lib-rs-gff.json -O $FMT
		nwn-gff /tmp/nwn-lib-rs-gff.json -o /tmp/nwn-lib-rs-gff.gff
		fix_krogar "$INPUT"
		[[ "$(chksum /tmp/nwn-lib-rs-gff.gff)" == "$GFF_SHASUM" ]]

		rm -f /tmp/nwn-lib-rs-gff.gff
		nwn-gff "$INPUT" -O $FMT | nwn-gff - -I json -o /tmp/nwn-lib-rs-gff.gff
		fix_krogar "$INPUT"
		[[ "$(chksum /tmp/nwn-lib-rs-gff.gff)" == "$GFF_SHASUM" ]]
	done

	# Yaml ser/deser
	rm -f /tmp/nwn-lib-rs-gff.{yaml,gff}

	nwn-gff "$INPUT" -o /tmp/nwn-lib-rs-gff.yaml -O yaml
	nwn-gff /tmp/nwn-lib-rs-gff.yaml -o /tmp/nwn-lib-rs-gff.gff
	fix_krogar "$INPUT"
	[[ "$(chksum /tmp/nwn-lib-rs-gff.gff)" == "$GFF_SHASUM" ]]

	rm -f /tmp/nwn-lib-rs-gff.gff
	nwn-gff "$INPUT" -O yaml | nwn-gff - -I yaml -o /tmp/nwn-lib-rs-gff.gff
	fix_krogar "$INPUT"
	[[ "$(chksum /tmp/nwn-lib-rs-gff.gff)" == "$GFF_SHASUM" ]]

done

# Set
nwn-gff "unittest/nwn2_doge.utc" -o /tmp/nwn-lib-rs-gff.json \
	--set Description="Test ExoLocString" \
	--set Subrace=22 \
	--set CompanionName="Test ExoString" \
	--set DecayTime=42 \
	--set ScriptOnNotice=TestResRef \
	--set Appearance_Type=1000 \
	--set ScriptHidden=-320000 \
	--set refbonus=-32000 \
	--set XpMod=13.37 \
	--set ACRtHip.Tintable.Tint.1.r=123 \
	--set SkillList.2.Rank=12

[[ "$(jq -r '.root.fields[] | select(.label == "Description") | .value.strings[0][0]' /tmp/nwn-lib-rs-gff.json)" == '0' ]]
[[ "$(jq -r '.root.fields[] | select(.label == "Description") | .value.strings[0][1]' /tmp/nwn-lib-rs-gff.json)" == 'Test ExoLocString' ]]
[[ "$(jq -r '.root.fields[] | select(.label == "Subrace") | .value' /tmp/nwn-lib-rs-gff.json)" == '22' ]]
[[ "$(jq -r '.root.fields[] | select(.label == "CompanionName") | .value' /tmp/nwn-lib-rs-gff.json)" == 'Test ExoString' ]]
[[ "$(jq -r '.root.fields[] | select(.label == "DecayTime") | .value' /tmp/nwn-lib-rs-gff.json)" == '42' ]]
[[ "$(jq -r '.root.fields[] | select(.label == "ScriptOnNotice") | .value' /tmp/nwn-lib-rs-gff.json)" == 'TestResRef' ]]
[[ "$(jq -r '.root.fields[] | select(.label == "Appearance_Type") | .value' /tmp/nwn-lib-rs-gff.json)" == '1000' ]]
[[ "$(jq -r '.root.fields[] | select(.label == "ScriptHidden") | .value' /tmp/nwn-lib-rs-gff.json)" == '-320000' ]]
[[ "$(jq -r '.root.fields[] | select(.label == "refbonus") | .value' /tmp/nwn-lib-rs-gff.json)" == '-32000' ]]
[[ "$(jq -r '.root.fields[] | select(.label == "XpMod") | .value' /tmp/nwn-lib-rs-gff.json)" == '13.37' ]]
[[ "$(jq -r '.root.fields[] | select(.label == "ACRtHip") | .value.fields[] | select(.label == "Tintable") | .value.fields[] | select(.label == "Tint") | .value.fields[] | select(.label == "1") | .value.fields[] | select(.label == "r") | .value' /tmp/nwn-lib-rs-gff.json)" == '123' ]]
[[ "$(jq -r '.root.fields[] | select(.label == "SkillList") | .value[2].fields[] | select(.label == "Rank") | .value' /tmp/nwn-lib-rs-gff.json)" == '12' ]]

# Set yaml
nwn-gff "unittest/nwn2_doge.utc" -o /tmp/nwn-lib-rs-gff.json \
	--set-yaml Description='{type: byte, value: 123}' \
	--set-yaml ACRtHip='{type: struct, value: {id: 111, fields: []}}'

[[ "$(jq -r '.root.fields[] | select(.label == "Description") | .type' /tmp/nwn-lib-rs-gff.json)" == 'byte' ]]
[[ "$(jq -r '.root.fields[] | select(.label == "Description") | .value' /tmp/nwn-lib-rs-gff.json)" == '123' ]]
[[ "$(jq -r '.root.fields[] | select(.label == "ACRtHip") | .type' /tmp/nwn-lib-rs-gff.json)" == 'struct' ]]
[[ "$(jq -r '.root.fields[] | select(.label == "ACRtHip") | .value.id' /tmp/nwn-lib-rs-gff.json)" == '111' ]]
[[ "$(jq -r '.root.fields[] | select(.label == "ACRtHip") | .value.fields | length' /tmp/nwn-lib-rs-gff.json)" == '0' ]]


rm -f /tmp/nwn-lib-rs-gff.{json,yaml,gff}
echo "Test success"