#!/bin/bash
set -euo pipefail

function chksum() {
	sha1sum "$1" | cut -d' ' -f1
}

set -x

rm -rf /tmp/nwn-lib-rs-trn
mkdir -p /tmp/nwn-lib-rs-trn

INF=$(nwn-trn info unittest/eauprofonde-portes.trx)
[[ "$(grep "file_type:" <<< "$INF")" == "file_type: \"NWN2\"" ]]
[[ "$(grep "version_major:" <<< "$INF")" == "version_major: 2" ]]
[[ "$(grep "version_minor:" <<< "$INF")" == "version_minor: 3" ]]
[[ "$(grep "packets:" <<< "$INF")" == "packets: 117 packets" ]]
[[ "$(grep "TRWH" <<< "$INF")" == "  0 TRWH: ID: 4370, Area size: 10x10 megatiles" ]]
[[ "$(grep "ASWM" <<< "$INF")" == "116 ASWM: 23123 vertices, 43398 triangles, 588 islands" ]]


# Unpack / Repack
(( $(nwn-trn unpack unittest/eauprofonde-portes.trx -o /tmp/nwn-lib-rs-trn/eauprofonde-portes -v | wc -l) == 117 ))
(( $(find /tmp/nwn-lib-rs-trn/eauprofonde-portes/ -type f | wc -l) == 117 ))

nwn-trn pack /tmp/nwn-lib-rs-trn/eauprofonde-portes.trx /tmp/nwn-lib-rs-trn/eauprofonde-portes/*

INF=$(nwn-trn info /tmp/nwn-lib-rs-trn/eauprofonde-portes.trx)
[[ "$(grep "file_type:" <<< "$INF")" == "file_type: \"NWN2\"" ]]
[[ "$(grep "version_major:" <<< "$INF")" == "version_major: 2" ]]
[[ "$(grep "version_minor:" <<< "$INF")" == "version_minor: 3" ]]
[[ "$(grep "packets:" <<< "$INF")" == "packets: 117 packets" ]]
[[ "$(grep "TRWH" <<< "$INF")" == "  0 TRWH: ID: 4370, Area size: 10x10 megatiles" ]]
[[ "$(grep "ASWM" <<< "$INF")" == "116 ASWM: 23123 vertices, 43398 triangles, 588 islands" ]]

rm -rf /tmp/nwn-lib-rs-trn

echo "Test success"