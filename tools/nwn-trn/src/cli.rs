use std::path::PathBuf;

use clap::{Parser, Subcommand};

use nwn_lib_rs::trn::PacketType;

/// Read and write Neverwinter Nights TRN files (trn, trx)
#[derive(Debug, Parser)]
#[command(author, version=nwn_lib_rs::NWN_LIB_RS_VERSION, about, long_about = None)]
pub struct Args {
    #[command(subcommand)]
    pub command: Commands,
}
#[derive(Debug, Subcommand)]
pub enum Commands {
    /// Validate and print TRN and basic packets information
    Info(InfoArgs),
    /// Unpack all packets as separate files without any modification
    Unpack(UnpackArgs),
    /// Create a TRN file from a list of packets
    Pack(PackArgs),
}

#[derive(Debug, Copy, Clone, clap::ValueEnum)]
pub enum ArgPacketType {
    Trwh,
    Trrn,
    Watr,
    Aswm,
}
impl ArgPacketType {
    pub fn to_std(&self) -> PacketType {
        match self {
            ArgPacketType::Trwh => PacketType::Trwh,
            ArgPacketType::Trrn => PacketType::Trrn,
            ArgPacketType::Watr => PacketType::Watr,
            ArgPacketType::Aswm => PacketType::Aswm,
        }
    }
}
#[derive(Debug, clap::Args)]
pub struct InfoArgs {
    /// TRN file to read
    #[arg()]
    pub input_file: PathBuf,
}
#[derive(Debug, clap::Args)]
pub struct UnpackArgs {
    /// TRN file to unpack
    #[arg()]
    pub input_file: PathBuf,

    /// Output folder for storing packet files. By default packets are written in the current directory
    #[arg(short, long)]
    pub output_folder: Option<PathBuf>,

    /// Overwrite existing files
    #[arg(long)]
    pub overwrite: bool,

    /// Print the name and location of each unpacked file
    #[arg(short, long)]
    pub verbose: bool,

    /// Only extract files matching these IDs
    #[arg(long)]
    pub filter_id: Vec<u32>,

    /// Only extract packets matching these types
    #[arg(long)]
    pub filter_type: Vec<ArgPacketType>,
}
#[derive(Debug, clap::Args)]
pub struct PackArgs {
    /// Output TRN file
    #[arg()]
    pub output_file: PathBuf,

    /// List of unpacked packets
    #[arg()]
    pub packet_files: Vec<PathBuf>,

    /// Sort all packets alphabetically
    #[arg(long)]
    pub sort: bool,

    /// Parse and validate each packet (and clean padding bytes)
    #[arg(short, long)]
    pub validate: bool,

    /// TRN file type string
    #[arg(short = 't', long = "type", default_value = "NWN2")]
    pub file_type: String,

    /// TRN file major version
    #[arg(long, default_value = "2")]
    pub version_maj: u16,

    /// TRN file minor version
    #[arg(long, default_value = "3")]
    pub version_min: u16,
}
