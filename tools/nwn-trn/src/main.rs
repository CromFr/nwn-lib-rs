#![allow(incomplete_features)]
#![feature(generic_const_exprs)]

use std::path::{Path, PathBuf};

use clap::Parser;

use nwn_lib_rs::tlk::FixedSizeString;
use nwn_lib_rs::trn::*;

mod cli;
use cli::*;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args = Args::parse();
    match args.command {
        Commands::Info(args) => info_trn(args)?,
        Commands::Unpack(args) => unpack_trn(args)?,
        Commands::Pack(args) => pack_trn(args)?,
    }

    Ok(())
}

fn info_trn(args: InfoArgs) -> Result<(), Box<dyn std::error::Error>> {
    let (_, trn) =
        Trn::from_bytes(&std::fs::read(args.input_file)?).expect("Failed to parse TRN file");

    println!("file_type: {:?}", trn.file_type);
    println!("version_major: {:?}", trn.version_major);
    println!("version_minor: {:?}", trn.version_minor);
    println!("packets: {} packets", trn.packets.len());
    println!("--------");
    for (i, packet) in trn.packets.iter().enumerate() {
        let PacketData::Parsed(packet) = packet else {
            panic!()
        };
        match packet.as_ref() {
            Packet::Trwh(trwh) => {
                println!(
                    "{:3} TRWH: ID: {}, Area size: {}x{} megatiles",
                    i, trwh.id, trwh.width, trwh.height
                );
            }
            Packet::Trrn(trrn) => {
                println!("{:3} TRRN: Name: {:?}", i, trrn.name);
            }
            Packet::Watr(watr) => {
                println!(
                    "{:3} WATR: Megatile position: {}x{}",
                    i, watr.megatile_position[0], watr.megatile_position[1]
                );
            }
            Packet::Aswm(aswm) => {
                println!(
                    "{:3} ASWM: {} vertices, {} triangles, {} islands",
                    i,
                    aswm.vertices.len(),
                    aswm.triangles.len(),
                    aswm.islands.len(),
                );
            }
        }
    }

    Ok(())
}

fn unpack_trn(args: UnpackArgs) -> Result<(), Box<dyn std::error::Error>> {
    let data = std::fs::read(&args.input_file)?;
    let (_, mut trn) = Trn::from_bytes_head(&data).expect("Failed to parse TRN file");
    trn.load_offsets(&data, 0)?;

    let file_name = Path::new(&args.input_file)
        .file_name()
        .expect("Could not find input file name")
        .to_str()
        .unwrap();

    let output_folder = if let Some(path) = args.output_folder {
        path
    } else {
        ".".into()
    };

    if !output_folder.exists() {
        std::fs::create_dir(&output_folder)?;
    }

    for (i, packet) in trn.packets.iter().enumerate() {
        let pack_type = packet.get_type();

        if !args.filter_id.is_empty() && args.filter_id.iter().all(|&index| index as usize != i) {
            continue;
        }
        if !args.filter_type.is_empty() && args.filter_type.iter().all(|t| pack_type == t.to_std())
        {
            continue;
        }

        let extension = match pack_type {
            PacketType::Trwh => "trwh",
            PacketType::Trrn => "trrn",
            PacketType::Watr => "watr",
            PacketType::Aswm => "aswm",
        };

        let out_path: PathBuf = [
            &output_folder,
            &PathBuf::from(format!("{}.{:03}.{}", file_name, i, extension)),
        ]
        .iter()
        .collect();

        // Avoid overwriting
        if !args.overwrite && out_path.exists() {
            return Err(format!(
                "The file already exists: {:?}. Use --overwrite to overwrite existing files.",
                out_path
            )
            .into());
        }

        // Write file
        if args.verbose {
            println!(" Packet {} => {:?}", i, out_path);
        }

        let data = packet.to_bytes();
        std::fs::write(out_path, data)?;
    }

    Ok(())
}

fn pack_trn(args: PackArgs) -> Result<(), Box<dyn std::error::Error>> {
    let mut trn = Trn {
        file_type: FixedSizeString::from_str(&args.file_type)?,
        version_major: args.version_maj,
        version_minor: args.version_min,
        packets: vec![],
    };

    let mut sorted;
    let packet_files = if args.sort {
        sorted = args.packet_files.clone();
        sorted.sort();
        &sorted
    } else {
        &args.packet_files
    };

    // Collect packets
    trn.packets = packet_files
        .iter()
        .map(|path| -> Result<PacketData, Box<dyn std::error::Error>> {
            let data = std::fs::read(path)?;
            match args.validate {
                true => Ok(PacketData::from_bytes_parse(&data, None)?.1),
                false => Ok(PacketData::from_bytes_clone(&data, None)?.1),
            }
        })
        .collect::<Result<_, _>>()?;

    // Write file
    let trn_data = trn.to_bytes()?;
    std::fs::write(args.output_file, trn_data)?;

    Ok(())
}

#[test]
fn verify_cli() {
    use clap::CommandFactory;
    Args::command().debug_assert()
}
