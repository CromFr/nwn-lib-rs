#!/bin/bash
set -euo pipefail

function chksum() {
	sha1sum "$1" | cut -d' ' -f1
}

set -x

for INPUT in unittest/dialog.tlk unittest/user.tlk; do
	TLK_SHASUM=$(chksum "$INPUT")

	nwn-tlk "$INPUT" > /dev/null

	# TLK ser/deser
	rm -f /tmp/nwn-lib-rs-tlk.tlk
	nwn-tlk "$INPUT" -o /tmp/nwn-lib-rs-tlk.tlk
	[[ "$(chksum /tmp/nwn-lib-rs-tlk.tlk)" == "$TLK_SHASUM" ]]

	rm -f /tmp/nwn-lib-rs-tlk.tlk
	nwn-tlk "$INPUT" -O tlk | nwn-tlk - -I tlk -o /tmp/nwn-lib-rs-tlk.tlk
	[[ "$(chksum /tmp/nwn-lib-rs-tlk.tlk)" == "$TLK_SHASUM" ]]

	# Json ser/deser
	for FMT in json json-mini; do
		rm -f /tmp/nwn-lib-rs-tlk.{json,tlk}

		nwn-tlk "$INPUT" -o /tmp/nwn-lib-rs-tlk.json -O $FMT
		nwn-tlk /tmp/nwn-lib-rs-tlk.json -o /tmp/nwn-lib-rs-tlk.tlk
		[[ "$(chksum /tmp/nwn-lib-rs-tlk.tlk)" == "$TLK_SHASUM" ]]

		rm -f /tmp/nwn-lib-rs-tlk.tlk
		nwn-tlk "$INPUT" -O $FMT | nwn-tlk - -I json -o /tmp/nwn-lib-rs-tlk.tlk
		[[ "$(chksum /tmp/nwn-lib-rs-tlk.tlk)" == "$TLK_SHASUM" ]]
	done

	# Yaml ser/deser
	rm -f /tmp/nwn-lib-rs-tlk.{yaml,tlk}

	nwn-tlk "$INPUT" -o /tmp/nwn-lib-rs-tlk.yaml -O yaml
	nwn-tlk /tmp/nwn-lib-rs-tlk.yaml -o /tmp/nwn-lib-rs-tlk.tlk
	[[ "$(chksum /tmp/nwn-lib-rs-tlk.tlk)" == "$TLK_SHASUM" ]]

	rm -f /tmp/nwn-lib-rs-tlk.tlk
	nwn-tlk "$INPUT" -O yaml | nwn-tlk - -I yaml -o /tmp/nwn-lib-rs-tlk.tlk
	[[ "$(chksum /tmp/nwn-lib-rs-tlk.tlk)" == "$TLK_SHASUM" ]]

done

rm -f /tmp/nwn-lib-rs-tlk.{json,yaml,tlk}

# STRREF hints
VAL=$(nwn-tlk unittest/user.tlk -O json | jq -r '.entries[3].__hint_strref')
[[ "$VAL" == '3' ]]
VAL=$(nwn-tlk unittest/user.tlk --user -O json | jq -r '.entries[3].__hint_strref')
[[ "$VAL" == '16777219' ]]

# Set existing values
VAL=$(nwn-tlk unittest/user.tlk --set '1.sound_resref=Yolo' -O json | jq -r '.entries[1].sound_resref')
[[ "$VAL" == 'Yolo' ]]
VAL=$(nwn-tlk unittest/user.tlk --set '1._volume_variance=34' -O json | jq -r '.entries[1]._volume_variance')
[[ "$VAL" == '34' ]]
VAL=$(nwn-tlk unittest/user.tlk --set '1._pitch_variance=52' -O json | jq -r '.entries[1]._pitch_variance')
[[ "$VAL" == '52' ]]
VAL=$(nwn-tlk unittest/user.tlk --set '1.sound_length=12.235' -O json | jq -r '.entries[1].sound_length')
[[ "$VAL" == '12.23'* ]]
VAL=$(nwn-tlk unittest/user.tlk --set '1.text=Hello world !' -O json | jq -r '.entries[1].text')
[[ "$VAL" == 'Hello world !' ]]

# Auto extend
VAL=$(nwn-tlk unittest/user.tlk --set '5.text=New entry' -O json | jq -r '.entries[5].text')
[[ "$VAL" == 'New entry' ]]
VAL=$(nwn-tlk unittest/user.tlk --set '5._pitch_variance=43' -O json | jq -r '.entries[5]._pitch_variance')
[[ "$VAL" == '43' ]]

# Using user indices
VAL=$(nwn-tlk unittest/user.tlk --user --set '16777216.text=this is some text with = sign and .' -O json | jq -r '.entries[0].text')
[[ "$VAL" == 'this is some text with = sign and .' ]]

# Shrinking
VAL=$(nwn-tlk unittest/user.tlk --set '3.text=' --set '3.sound_resref=' --shrink -O json | jq -r '.entries | length')
[[ "$VAL" == '2' ]]

# Merging
nwn-tlk unittest/dialog.tlk --merge unittest/user.tlk --merge-keep a -o /tmp/nwn-lib-rs-tlk.json
[[ "$(jq -r '.entries[0].text' /tmp/nwn-lib-rs-tlk.json)" == 'Bad Strref' ]]
[[ "$(jq -r '.entries[1].text' /tmp/nwn-lib-rs-tlk.json)" == 'Barbares' ]]
[[ "$(jq -r '.entries[2].text' /tmp/nwn-lib-rs-tlk.json)" == 'Barde' ]]
[[ "$(jq -r '.entries[3].sound_resref' /tmp/nwn-lib-rs-tlk.json)" == 'null' ]]
nwn-tlk unittest/dialog.tlk --merge unittest/user.tlk --merge-keep b -o /tmp/nwn-lib-rs-tlk.json
[[ "$(jq -r '.entries[0].text' /tmp/nwn-lib-rs-tlk.json)" == 'Hello world' ]]
[[ "$(jq -r '.entries[1].text' /tmp/nwn-lib-rs-tlk.json)" == 'Café liégeois' ]]
[[ "$(jq -r '.entries[2].text' /tmp/nwn-lib-rs-tlk.json)" == 'Barde' ]]
[[ "$(jq -r '.entries[3].sound_resref' /tmp/nwn-lib-rs-tlk.json)" == 'snd_custom' ]]
rm -f /tmp/nwn-lib-rs-tlk.json


echo "Test success"
