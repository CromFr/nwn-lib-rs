use std::path::{Path, PathBuf};

use clap::Parser;
use nwn_lib_rs::parsing::parse_detect_u32;

#[derive(Debug, Clone, clap::ValueEnum)]
pub enum InputFormat {
    Auto,
    Tlk,
    Json,
    Yaml,
}
impl InputFormat {
    pub fn guess(file_path: &Path) -> Option<Self> {
        let extension = file_path
            .extension()?
            .to_ascii_lowercase()
            .into_string()
            .ok()?;
        match extension.as_str() {
            "tlk" => Some(Self::Tlk),
            "json" => Some(Self::Json),
            "yaml" | "yml" => Some(Self::Yaml),
            _ => None,
        }
    }
}
#[derive(Debug, Clone, clap::ValueEnum)]
pub enum OutputFormat {
    Auto,
    Tlk,
    Json,
    JsonMini,
    Yaml,
    Text,
}
impl OutputFormat {
    pub fn guess(file_path: &Path) -> Option<Self> {
        if file_path == Path::new("-") {
            return Some(Self::Text);
        }
        let extension = file_path
            .extension()?
            .to_ascii_lowercase()
            .into_string()
            .ok()?;
        match extension.as_str() {
            "tlk" => Some(Self::Tlk),
            "json" => Some(Self::Json),
            "yaml" | "yml" => Some(Self::Yaml),
            "txt" => Some(Self::Text),
            _ => None,
        }
    }
}

#[derive(Debug, Clone)]
pub enum SetArgValue {
    SoundResref(Option<nwn_lib_rs::tlk::FixedSizeString<16>>),
    VolumeVariance(u32),
    PitchVariance(u32),
    SoundLength(Option<f32>),
    Text(Option<String>),
}

#[derive(Debug, Clone)]
pub struct SetArg {
    pub index: u32,
    pub value: SetArgValue,
}
#[derive(Debug)]
pub struct SetArgParseError {
    pub msg: String,
}
impl std::error::Error for SetArgParseError {}
impl std::fmt::Display for SetArgParseError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.msg)
    }
}

pub fn parse_setarg(s: &str) -> Result<SetArg, SetArgParseError> {
    let eq = s.find('=').ok_or(SetArgParseError {
        msg: "'=' not found".to_string(),
    })?;
    let path: Vec<&str> = s[..eq].split('.').collect();
    let value = s.get(eq + 1..).ok_or(SetArgParseError {
        msg: "No value found after '='".to_string(),
    })?;

    if path.len() != 2 {
        return Err(SetArgParseError {
            msg: "Malformed value path. Must be <index>.<property>".to_string(),
        });
    }
    let (index, property) = (path[0], path[1]);

    let index = parse_detect_u32(index).map_err(|err| SetArgParseError {
        msg: err.to_string(),
    })?;

    let prop_value = match property {
        "sound_resref" => SetArgValue::SoundResref(if !value.is_empty() {
            Some(
                nwn_lib_rs::tlk::FixedSizeString::from_str(value).map_err(|err| {
                    SetArgParseError {
                        msg: err.to_string(),
                    }
                })?,
            )
        } else {
            None
        }),
        "_volume_variance" => {
            SetArgValue::VolumeVariance(parse_detect_u32(value).map_err(|err| {
                SetArgParseError {
                    msg: err.to_string(),
                }
            })?)
        }
        "_pitch_variance" => {
            SetArgValue::PitchVariance(parse_detect_u32(value).map_err(|err| SetArgParseError {
                msg: err.to_string(),
            })?)
        }
        "sound_length" => SetArgValue::SoundLength(if !value.is_empty() {
            Some(value.parse::<f32>().map_err(|err| SetArgParseError {
                msg: err.to_string(),
            })?)
        } else {
            None
        }),
        "text" => SetArgValue::Text(if !value.is_empty() {
            Some(value.to_owned())
        } else {
            None
        }),
        _ => {
            return Err(SetArgParseError {
                msg: format!("Unknown property '{}'", property),
            })
        }
    };

    Ok(SetArg {
        index,
        value: prop_value,
    })
}
#[derive(Debug, Copy, Clone, clap::ValueEnum)]
pub enum MergeKeep {
    A,
    B,
}

/// Read and write Neverwinter Nights TLK files
#[derive(Parser, Debug)]
#[command(author, version=nwn_lib_rs::NWN_LIB_RS_VERSION, about, long_about = None)]
pub struct Args {
    /// Input file. '-' for reading from stdin
    #[arg()]
    pub input: PathBuf,
    /// Input file format.
    ///
    /// 'auto' to guess based on the file extension
    #[arg(short = 'I', long, default_value = "auto")]
    pub input_format: InputFormat,

    /// Output file. Omit or set to '-' for writing to stdout
    #[arg(short, long)]
    pub output: Option<PathBuf>,
    /// Output file format.
    ///
    /// 'auto' to guess based on the file extension
    #[arg(short = 'O', long, default_value = "auto")]
    pub output_format: OutputFormat,

    /// Use TLK user indices (starting at 16777216)
    #[arg(short, long)]
    pub user: bool,

    /// Parse TLKs in strict mode
    ///
    /// Official TLK files sometimes contain invalid data and may be refused if parsed in strict mode.
    /// By default nwn-tlk will attempt to repair bad TLK data (invalid UTF-8, bad pointers)
    #[arg(long)]
    pub strict: bool,

    /// Set a specific value. Entry flags are updated accordingly.
    ///
    /// Must follow the form: <strref>.<property>=<value>
    ///
    /// strref    Entry index
    ///
    /// property  Any of: flags sound_resref _volume_variance _pitch_variance sound_length text
    ///
    /// value     Value to set
    #[arg(short, long, value_parser = parse_setarg)]
    pub set: Vec<SetArg>,

    /// Shrink the TLK to only contain useful entries
    #[arg(long)]
    pub shrink: bool,

    /// Merge the loaded TLK (a) with another TLK (b) provided with --merge
    #[arg(long)]
    pub merge: Option<PathBuf>,
    /// Merge file format
    #[arg(long, default_value = "auto")]
    pub merge_format: InputFormat,
    /// If the two TLKs to merge have different text for a same strref, always chose the text from TLK a or b
    #[arg(long)]
    pub merge_keep: Option<MergeKeep>,
}
