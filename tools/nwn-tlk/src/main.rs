#![allow(incomplete_features)]
#![feature(generic_const_exprs)]

use std::{
    fs::{read, write},
    io::Read,
    path::Path,
    process::exit,
};

use clap::Parser;
use nwn_lib_rs::tlk::{Tlk, TLK_STRREF_USER_OFFSET};

mod cli;
use cli::*;

fn load_tlk(
    file: &Path,
    format: InputFormat,
    strict_parsing: bool,
) -> Result<Tlk, Box<dyn std::error::Error>> {
    let format = if matches!(format, InputFormat::Auto) {
        InputFormat::guess(file).ok_or::<Box<dyn std::error::Error>>(
            format!("Cannot guess format for file {:?}", file).into(),
        )?
    } else {
        format
    };

    let data = if file == Path::new("-") {
        let mut buf = Vec::<u8>::new();
        std::io::stdin().read_to_end(&mut buf)?;
        buf
    } else {
        read(file)?
    };

    match format {
        InputFormat::Auto => {
            panic!();
        }
        InputFormat::Tlk => {
            let (_, tlk) = Tlk::from_bytes(&data, !strict_parsing)?;
            Ok(tlk)
        }
        InputFormat::Json => serde_json::from_slice::<Tlk>(&data)
            .map_err(|err| Box::new(err) as Box<dyn std::error::Error>),
        InputFormat::Yaml => serde_yml::from_slice::<Tlk>(&data)
            .map_err(|err| Box::new(err) as Box<dyn std::error::Error>),
    }
}

fn main() {
    let mut args = Args::parse();

    // Detect output formats
    if matches!(args.output_format, OutputFormat::Auto) {
        if let Some(file_path) = &args.output {
            match OutputFormat::guess(file_path) {
                Some(t) => args.output_format = t,
                None => {
                    eprintln!("Cannot guess output file format. Try providing -O argument.");
                    exit(1);
                }
            }
        } else {
            args.output_format = OutputFormat::Text;
        }
    }

    // Read input
    let mut tlk = load_tlk(&args.input, args.input_format, args.strict).unwrap_or_else(|err| {
        eprintln!("Failed to parse input file: {}", err);
        exit(1);
    });
    tlk.hint_is_user = args.user;

    if args.merge.is_some() {
        let merge_tlk = load_tlk(&args.merge.unwrap(), args.merge_format, args.strict)
            .unwrap_or_else(|err| {
                eprintln!("Failed to parse input file: {}", err);
                exit(1);
            });
        merge_tlks(&mut tlk, merge_tlk, args.merge_keep).unwrap_or_else(|err| {
            eprintln!("Failed to merge TLKs: {}", err);
            exit(1);
        });
    }

    for set in args.set {
        let index = if tlk.hint_is_user {
            if set.index < TLK_STRREF_USER_OFFSET {
                eprintln!(
                    "Bad strref for a user TLK: {}. User indices must start from {}",
                    set.index, TLK_STRREF_USER_OFFSET
                );
                exit(1);
            }
            set.index - TLK_STRREF_USER_OFFSET
        } else {
            set.index
        };

        match set.value {
            SetArgValue::SoundResref(value) => {
                let entry = tlk.get_entry_mut(index);
                entry.sound_resref = value;
            }
            SetArgValue::VolumeVariance(value) => {
                tlk.get_entry_mut(index)._volume_variance = value;
            }
            SetArgValue::PitchVariance(value) => tlk.get_entry_mut(index)._pitch_variance = value,
            SetArgValue::SoundLength(value) => {
                let entry = tlk.get_entry_mut(index);
                entry.sound_length = value;
            }
            SetArgValue::Text(value) => tlk.set_string(index, value),
        }
    }

    if args.shrink {
        tlk.shrink_to_fit();
    }

    // Serialize data
    || -> Result<(), Box<dyn std::error::Error>> {
        let data: Vec<u8> = match args.output_format {
            OutputFormat::Auto => panic!(),
            OutputFormat::Tlk => tlk.to_bytes(),
            OutputFormat::Json => serde_json::to_string_pretty(&tlk)?.into_bytes(),
            OutputFormat::JsonMini => serde_json::to_string(&tlk)?.into_bytes(),
            OutputFormat::Yaml => serde_yml::to_string(&tlk)?.into_bytes(),
            OutputFormat::Text => tlk.to_string_pretty().into_bytes(),
        };

        let output = args.output.unwrap_or("-".into());
        if output != Path::new("-") {
            write(output, data).map_err(|err| Box::new(err) as Box<dyn std::error::Error>)
        } else {
            use std::io::Write;
            std::io::stdout()
                .write_all(&data)
                .map(|_res| ())
                .map_err(|err| Box::new(err) as Box<dyn std::error::Error>)
        }
    }()
    .unwrap_or_else(|err: Box<dyn std::error::Error>| {
        eprintln!("Failed to serialize data: {}", err);
        exit(1);
    });
}

fn merge_tlks(
    dest: &mut Tlk,
    to_merge: Tlk,
    keep: Option<MergeKeep>,
) -> Result<(), Box<dyn std::error::Error>> {
    if to_merge.len() > dest.len() {
        dest.resize(to_merge.len());
    }
    let index_padding = (dest.len().ilog10() + 1) as usize;

    for (i, to_merge_entry) in to_merge.entries.into_iter().enumerate() {
        let dest_entry = dest.get_entry(i as u32).unwrap();

        if !to_merge_entry.is_empty() {
            if dest_entry.is_empty() {
                *dest.get_entry_mut(i as u32) = to_merge_entry.into_owned(&to_merge.strings_data);
            } else {
                // Check if entries are really different
                if dest_entry.sound_resref != to_merge_entry.sound_resref
                    || dest_entry._volume_variance != to_merge_entry._volume_variance
                    || dest_entry._pitch_variance != to_merge_entry._pitch_variance
                    || dest_entry.sound_length != to_merge_entry.sound_length
                    || dest_entry.get_str(&dest.strings_data)
                        != to_merge_entry.get_str(&to_merge.strings_data)
                {
                    // Entries are different, there is a conflict
                    // Conflict
                    if let Some(keep) = keep {
                        // Automatically resolve
                        eprintln!(
                            "Conflict at strref {}: automatically chose version {:?}",
                            i, keep
                        );
                        match keep {
                            MergeKeep::A => {}
                            MergeKeep::B => {
                                *dest.get_entry_mut(i as u32) =
                                    to_merge_entry.into_owned(&to_merge.strings_data);
                            }
                        }
                    } else {
                        // Prompt
                        eprintln!("Conflict at strref {}:", i);
                        eprintln!("================== Version A ================");
                        eprintln!(
                            "{}",
                            dest_entry.to_string_pretty(
                                i as u32,
                                index_padding,
                                false,
                                &dest.strings_data
                            )
                        );
                        eprintln!("================== Version B ================");
                        eprintln!(
                            "{}",
                            to_merge_entry.to_string_pretty(
                                i as u32,
                                index_padding,
                                false,
                                &to_merge.strings_data
                            )
                        );
                        loop {
                            eprint!("=> Select version to keep (a|b): ");
                            let mut res = String::new();
                            std::io::stdin()
                                .read_line(&mut res)
                                .expect("Failed to read from stdin");

                            match res.as_str().trim() {
                                "A" | "a" => break,
                                "B" | "b" => {
                                    *dest.get_entry_mut(i as u32) =
                                        to_merge_entry.into_owned(&to_merge.strings_data);
                                    break;
                                }
                                _ => eprintln!("Unknown response {:?}", res.as_str().trim()),
                            }
                        }
                    }
                }
            }
        }
    }

    Ok(())
}

#[test]
fn verify_cli() {
    use clap::CommandFactory;
    Args::command().debug_assert()
}
