#!/bin/bash
set -euo pipefail

function chksum() {
	sha1sum "$1" | cut -d' ' -f1
}

set -x

rm -rf /tmp/nwn-lib-rs-2da
mkdir -p /tmp/nwn-lib-rs-2da

# Invalid number of columns on row 10
nwn-2da unittest/restsys_wm_tables.2da >&/dev/null && exit 1

# Note: rows that are modified with the merge process are not verified in this function
function check_restsys_wm_tables(){
	DATA=$(cat)

	grep -E '^0\s+INVALID_TABLE\s+",start w/ comma \(makes excel add quotes!\) Dont_change this row!"\s+83306\s+83307\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}$' <<< "$DATA"
	grep -E '^10\s+c_cave\s+,beasts\s+\*{4}\s+\*{4}\s+c_wolverinedire_module,c_wolverinedire_module\s+50\s+c_snowleopard_module,c_snowleopard_module\s+50\s+\*{4}$' <<< "$DATA"
	grep -E '^21\s+b_wells_of_lurue\s+",Various Telthors and Elementals"\s+\*{4}\s+\*{4}\s+b10_telthor_direbear,b10_telthor_direbear,b10_telthor_direbear\s+33\s+b10_telthor_wolf,b10_telthor_wolf\s+33\s+b10_earth_elemental,b10_orglash$' <<< "$DATA"

	grep -E '^23\s+test-crom-encoding\s+"test\\n\\"string\\t\\tencoding'\''"\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}$' <<< "$DATA"
	grep -E '^24\s+test-crom-extra\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}\s+last_column$' <<< "$DATA"
	grep -E '^25\s+test-crom-bad-index\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}\s+hello\s+world$' <<< "$DATA"
	grep -E '^26\s+test-crom-good-index\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}\s+one\s+two$' <<< "$DATA"
	grep -E '^27\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}$' <<< "$DATA"
}

echo "========================================== PARSING"
nwn-2da unittest/restsys_wm_tables.2da --repair | check_restsys_wm_tables

# Compact 2DA
nwn-2da unittest/restsys_wm_tables.2da --repair -O 2da-mini -o /tmp/nwn-lib-rs-2da/restsys_wm_tables.2da
nwn-2da unittest/restsys_wm_tables.2da --repair | check_restsys_wm_tables
grep -E '^22\s\*{4}$' /tmp/nwn-lib-rs-2da/restsys_wm_tables.2da

echo "========================================== CONVERSIONS"

# json
nwn-2da unittest/restsys_wm_tables.2da --repair -o /tmp/nwn-lib-rs-2da/restsys_wm_tables.json
nwn-2da /tmp/nwn-lib-rs-2da/restsys_wm_tables.json | check_restsys_wm_tables
# yaml
nwn-2da unittest/restsys_wm_tables.2da --repair -o /tmp/nwn-lib-rs-2da/restsys_wm_tables.yaml
nwn-2da /tmp/nwn-lib-rs-2da/restsys_wm_tables.yaml | check_restsys_wm_tables

echo "========================================== MERGING A"
# Merging
echo -e "A\nA" | nwn-2da unittest/restsys_wm_tables.2da --repair --merge unittest/restsys_wm_tables_merge.2da --merge-repair -o /tmp/nwn-lib-rs-2da/restsys_wm_tables.2da
check_restsys_wm_tables < /tmp/nwn-lib-rs-2da/restsys_wm_tables.2da
grep -E '^11\s+e_thaymount\s+,gnolls\s+\*{4}\s+\*{4}\s+e_gnoll1,e_gnoll2\s+50\s+e_gnoll1,egnoll1\s+50\s+\*{4}$' /tmp/nwn-lib-rs-2da/restsys_wm_tables.2da
grep -E '^12\s+e_academy\s+,students\s+\*{4}\s+\*{4}\s+e_student_female_hostile,e_student_male_hostile\s+50\s+e_student_female_hostile2,e_student_female_hostile,e_student_male_hostile2\s+25\s+e_student_female_hostile,e_student_male_hostile,e_student_male_hostile2$' <<< "$DATA"
grep -E '^19\s+b_upper_vault\s+",Death Knights, Mummified Priests, Dread Wraiths"\s+"hello world"\s+\*{4}\s+b07_deathknight_hostile,b07_deathknight_hostile\s+33\s+b07_mummy_priest,b07_mummy_priest,b07_mummy_priest\s+33\s+b07_dreadwraith,b07_dreadwraith,b07_dreadwraith,b07_dreadwraith$' <<< "$DATA"
grep -E '^20\s+b_lower_vault\s+",Death Lords, Vampire Ancients, Shadow of the Void"\s+\*{4}\s+e1rref\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}$' <<< "$DATA"
grep -E '^22\s+non\s+conflicting\s+row\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}$' /tmp/nwn-lib-rs-2da/restsys_wm_tables.2da
grep -E '^30\s+non\s+conflicting\s+extra\s+row\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}$' /tmp/nwn-lib-rs-2da/restsys_wm_tables.2da


EXP_CHKSUM=$(chksum /tmp/nwn-lib-rs-2da/restsys_wm_tables.2da)
nwn-2da unittest/restsys_wm_tables.2da --repair --merge unittest/restsys_wm_tables_merge.2da --merge-repair --conflict-use a -o /tmp/nwn-lib-rs-2da/restsys_wm_tables.2da
[[ "$(chksum /tmp/nwn-lib-rs-2da/restsys_wm_tables.2da)" == "$EXP_CHKSUM" ]]

echo "========================================== MERGING B"

echo -e "B\nB"  | nwn-2da unittest/restsys_wm_tables.2da --repair --merge unittest/restsys_wm_tables_merge.2da --merge-repair -o /tmp/nwn-lib-rs-2da/restsys_wm_tables.2da
check_restsys_wm_tables < /tmp/nwn-lib-rs-2da/restsys_wm_tables.2da
grep -E '^11\s+conflicting\s+row\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}$' /tmp/nwn-lib-rs-2da/restsys_wm_tables.2da
grep -E '^12\s+force\s+field\s+conflict\s+\*{4}\s+e_student_female_hostile,e_student_male_hostile\s+50\s+e_student_female_hostile2,e_student_female_hostile,e_student_male_hostile2\s+25\s+e_student_female_hostile,e_student_male_hostile,e_student_male_hostile2$' <<< "$DATA"
grep -E '^19\s+b_upper_vault\s+",Death Knights, Mummified Priests, Dread Wraiths"\s+"hello world"\s+\*{4}\s+b07_deathknight_hostile,b07_deathknight_hostile\s+33\s+b07_mummy_priest,b07_mummy_priest,b07_mummy_priest\s+33\s+b07_dreadwraith,b07_dreadwraith,b07_dreadwraith,b07_dreadwraith$' <<< "$DATA"
grep -E '^20\s+b_lower_vault\s+",Death Lords, Vampire Ancients, Shadow of the Void"\s+\*{4}\s+e1rref\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}$' <<< "$DATA"
grep -E '^22\s+non\s+conflicting\s+row\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}$' /tmp/nwn-lib-rs-2da/restsys_wm_tables.2da
grep -E '^30\s+non\s+conflicting\s+extra\s+row\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}$' /tmp/nwn-lib-rs-2da/restsys_wm_tables.2da

EXP_CHKSUM=$(chksum /tmp/nwn-lib-rs-2da/restsys_wm_tables.2da)
nwn-2da unittest/restsys_wm_tables.2da --repair --merge unittest/restsys_wm_tables_merge.2da --merge-repair --conflict-use b -o /tmp/nwn-lib-rs-2da/restsys_wm_tables.2da
[[ "$(chksum /tmp/nwn-lib-rs-2da/restsys_wm_tables.2da)" == "$EXP_CHKSUM" ]]

echo "========================================== MERGING CUSTOM"

nwn-2da unittest/restsys_wm_tables.2da --repair --merge unittest/restsys_wm_tables_merge.2da --merge-repair -o /tmp/nwn-lib-rs-2da/restsys_wm_tables.2da <<< "11 hello world **** **** **** **** **** **** ****
12 yolo hey **** **** **** **** **** **** ****"
check_restsys_wm_tables < /tmp/nwn-lib-rs-2da/restsys_wm_tables.2da
grep -E '^11\s+hello\s+world\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}$' /tmp/nwn-lib-rs-2da/restsys_wm_tables.2da
grep -E '^12\s+yolo\s+hey\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}$' /tmp/nwn-lib-rs-2da/restsys_wm_tables.2da
grep -E '^19\s+b_upper_vault\s+",Death Knights, Mummified Priests, Dread Wraiths"\s+"hello world"\s+\*{4}\s+b07_deathknight_hostile,b07_deathknight_hostile\s+33\s+b07_mummy_priest,b07_mummy_priest,b07_mummy_priest\s+33\s+b07_dreadwraith,b07_dreadwraith,b07_dreadwraith,b07_dreadwraith$' <<< "$DATA"
grep -E '^20\s+b_lower_vault\s+",Death Lords, Vampire Ancients, Shadow of the Void"\s+\*{4}\s+e1rref\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}$' <<< "$DATA"
grep -E '^22\s+non\s+conflicting\s+row\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}$' /tmp/nwn-lib-rs-2da/restsys_wm_tables.2da
grep -E '^30\s+non\s+conflicting\s+extra\s+row\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}\s+\*{4}$' /tmp/nwn-lib-rs-2da/restsys_wm_tables.2da


rm -rf /tmp/nwn-lib-rs-2da

echo "Test success"