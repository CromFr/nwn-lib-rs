use std::path::{Path, PathBuf};

use clap::Parser;

#[derive(Debug, Copy, Clone, clap::ValueEnum)]
pub enum InputFormat {
    /// Detect based on file extension
    Auto,
    /// 2DA text format
    #[clap(name = "2da")]
    TwoDA,
    /// JSON object
    Json,
    /// YAML document
    Yaml,
}
impl InputFormat {
    pub fn guess(file_path: &Path) -> Option<Self> {
        let extension = file_path
            .extension()?
            .to_ascii_lowercase()
            .into_string()
            .ok()?;
        match extension.as_str() {
            "2da" => Some(Self::TwoDA),
            "json" => Some(Self::Json),
            "yaml" | "yml" => Some(Self::Yaml),
            _ => None,
        }
    }
}
#[derive(Debug, Clone, clap::ValueEnum)]
pub enum OutputFormat {
    /// Detect based on file extension
    Auto,
    /// 2DA text format (default for the .2da extension)
    #[clap(name = "2da")]
    TwoDA,
    /// Compact 2DA text format (removed extra spaces and fields)
    #[clap(name = "2da-mini")]
    TwoDAMini,
    /// Beautiful JSON (default for the .json extension)
    Json,
    /// Minified JSON
    JsonMini,
    /// YAML
    Yaml,
}
impl OutputFormat {
    pub fn guess(file_path: &Path) -> Option<Self> {
        if file_path == Path::new("-") {
            return Some(Self::TwoDA);
        }
        let extension = file_path
            .extension()?
            .to_ascii_lowercase()
            .into_string()
            .ok()?;
        match extension.as_str() {
            "2da" => Some(Self::TwoDA),
            "json" => Some(Self::Json),
            "yaml" | "yml" => Some(Self::Yaml),
            _ => None,
        }
    }
}

#[derive(Debug, Clone, clap::ValueEnum)]
pub enum ConflictAns {
    /// Chose version from input file
    A,
    /// Chose version from merge file
    B,
}

/// Read and write Neverwinter Nights 2DA files
#[derive(Parser, Debug)]
#[command(author, version=nwn_lib_rs::NWN_LIB_RS_VERSION, about, long_about = None)]
pub struct Args {
    /// Input file. '-' for reading from stdin
    #[arg()]
    pub input: PathBuf,
    /// Input file format.
    ///
    /// 'auto' to guess based on the file extension
    #[arg(short = 'I', long, default_value = "auto")]
    pub input_format: InputFormat,

    /// Output file. Omit or set to '-' for writing to stdout
    #[arg(short, long)]
    pub output: Option<PathBuf>,
    /// Output file format.
    ///
    /// 'auto' to guess based on the file extension
    #[arg(short = 'O', long, default_value = "auto")]
    pub output_format: OutputFormat,

    /// Edit the file in-place, keeping the same file name and file format
    #[arg(long, conflicts_with = "output", conflicts_with = "output_format")]
    pub in_place: bool,

    /// Repair inconsistent 2DA files (bad number of columns)
    #[arg(short, long)]
    pub repair: bool,

    /// Merge another 2DA content into this one. See --help-2dam for the 2DAM format that is better suited for merging 2DA files
    #[arg(short, long)]
    pub merge: Option<PathBuf>,

    #[arg(short = 'M', long, default_value = "auto")]
    pub merge_format: InputFormat,

    /// Repair 2DA to merge before merging
    #[arg(long)]
    pub merge_repair: bool,

    /// Use version A or B for each merge conflict (non interactive)
    #[arg(long)]
    pub conflict_use: Option<ConflictAns>,

    /// Show help related to the 2DAM file format, for helping with 2DA merging
    #[arg(long)]
    pub help_2dam: bool,
}
