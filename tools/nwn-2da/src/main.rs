#![allow(incomplete_features)]
#![feature(generic_const_exprs)]

use std::{
    borrow::Cow,
    fs::{read, write},
    io::Read,
    path::{Path, PathBuf},
    process::exit,
};

use clap::Parser;
use nom::{error::convert_error, Finish};

use nwn_lib_rs::twoda::*;

mod cli;
use cli::*;

fn load_twoda(
    file: &PathBuf,
    file_format: &InputFormat,
    repair: bool,
) -> Result<TwoDA, Box<dyn std::error::Error>> {
    let format = if matches!(file_format, InputFormat::Auto) {
        InputFormat::guess(file).ok_or::<Box<dyn std::error::Error>>(
            format!("Cannot guess format for file {:?}", file).into(),
        )?
    } else {
        *file_format
    };

    let data = if file == Path::new("-") {
        let mut buf = Vec::<u8>::new();
        std::io::stdin().read_to_end(&mut buf)?;
        buf
    } else {
        read(file)?
    };

    let data_str: Cow<_> = match std::str::from_utf8(&data) {
        Ok(data) => data.into(),
        Err(e) => {
            if repair {
                // Fallback to utf8 lossy
                String::from_utf8_lossy(&data)
            } else {
                return Err(e.into());
            }
        }
    };

    match format {
        InputFormat::Auto => panic!(),
        InputFormat::TwoDA => {
            let (_, twoda) = TwoDA::from_str(&data_str, repair)
                .finish()
                .map_err(|e| convert_error(data_str.as_ref(), e))?;
            Ok(twoda)
        }
        InputFormat::Json => serde_json::from_str::<TwoDA>(&data_str)
            .map_err(|err| Box::new(err) as Box<dyn std::error::Error>),
        InputFormat::Yaml => serde_yml::from_str::<TwoDA>(&data_str)
            .map_err(|err| Box::new(err) as Box<dyn std::error::Error>),
    }
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    if std::env::args().any(|arg| arg == "--help-2dam") {
        println!(
            "\
=============================== 2DAM FILE FORMAT ===============================

This file format is very similar to standard 2DA file format, but is better
suited for defining a list of rows to be merged to a destination 2DA file.

The differences are:
- The first line must be '2DAMV1.0'
- Row numbers can be non-consecutive, and they are used for indexing rows
  (instead of being ignored)
- If the row number starts with a '?' character, fields will be individually
  merged. Each field must start with a special character (before any quote):
  - '-': Ignore this field. Any existing value will be kept as is.
  - '?<value>': Expect the provided value, and generate a merge conflict if
    the existing value don't match
  - '=<value>': Set the value, overwriting any existing value

Example 2DAM file:
--------------------------------------------------------------------------------
2DAMV1.0

    Label       Name      Description Icon           Untrained KeyAbility ArmorCheckPenalty
3   Discipline  343       347         isk_discipline 1         STR        0
28  Ride        ****      ****        isk_pocket     0         DEX        0
?29 ?Survival   =16777334 =16777336   -              -         -          -
100 CustomSkill 16777327  16777328    myicon         1         STR        0
--------------------------------------------------------------------------------

Merging this file with replace rows 3 and 28, change the Name and Description
columns of row 29, and extend the 2DA table to 101 rows if needed while
replacing row 100.

Note that two 2DAs can only be merged if their columns are the same."
        );
        exit(0)
    }

    let mut args = Args::parse();

    if args.in_place {
        args.output = Some(args.input.clone());
        args.output_format = match args.input_format {
            InputFormat::Auto => OutputFormat::Auto,
            InputFormat::TwoDA => OutputFormat::TwoDA,
            InputFormat::Json => OutputFormat::Json,
            InputFormat::Yaml => OutputFormat::Yaml,
        };
    }

    // Detect output formats
    if matches!(args.output_format, OutputFormat::Auto) {
        if let Some(file_path) = &args.output {
            match OutputFormat::guess(file_path) {
                Some(t) => args.output_format = t,
                None => {
                    eprintln!("Cannot guess output file format. Try providing -O argument.");
                    exit(1);
                }
            }
        } else {
            args.output_format = OutputFormat::TwoDA;
        }
    }

    // Read input
    let mut twoda = load_twoda(&args.input, &args.input_format, args.repair).unwrap_or_else(
        |err: Box<dyn std::error::Error>| {
            eprintln!("Failed to parse 2DA: {}", err);
            exit(1);
        },
    );

    // 2DA edit
    || -> Result<(), Box<dyn std::error::Error>> {
        if let Some(merge) = args.merge {
            let to_merge = load_twoda(&merge, &args.merge_format, args.merge_repair)?;

            let columns_len = twoda.get_columns().len();
            if twoda.get_columns() != to_merge.get_columns() {
                return Err(format!(
                    "Mismatched columns between the two 2DA files\nBase 2DA:  {:?}\nMerge 2DA: \
                     {:?}",
                    twoda.get_columns(),
                    to_merge.get_columns(),
                )
                .into());
            }

            twoda.resize_rows(std::cmp::max(twoda.len_rows(), to_merge.len_rows()));

            for (irow, src_row) in to_merge.iter().enumerate() {
                enum RowMergeAction {
                    Skip,
                    Overwrite,
                    Conflict,
                }
                fn get_row_action(
                    dst: &[Option<String>],
                    src: &[Option<String>],
                    merge_actions: &Option<&[MergeAction]>,
                ) -> RowMergeAction {
                    if src.iter().all(Option::is_none) {
                        return RowMergeAction::Skip;
                    }

                    // Check expected values
                    if let Some(merge_actions) = merge_actions {
                        if (0..dst.len())
                            .any(|i| merge_actions[i] == MergeAction::Expect && dst[i] != src[i])
                        {
                            return RowMergeAction::Conflict;
                        } else {
                            return RowMergeAction::Overwrite;
                        }
                    }

                    // All dest fields are empty => no conflict
                    if dst.iter().all(Option::is_none) {
                        if src.iter().all(Option::is_none) {
                            return RowMergeAction::Skip;
                        } else {
                            return RowMergeAction::Overwrite;
                        }
                    }

                    // Fields are identical (only for standard merge)  => no conflict
                    if merge_actions.is_none() && (0..dst.len()).all(|i| dst[i] == src[i]) {
                        return RowMergeAction::Skip;
                    }

                    RowMergeAction::Conflict
                }

                let dest_row = twoda.get_row(irow).unwrap();
                let merge_actions = to_merge.get_row_merge_actions(irow);

                match get_row_action(dest_row, src_row, &merge_actions) {
                    RowMergeAction::Skip => continue,
                    RowMergeAction::Overwrite => {
                        if let Some(merge_actions) = merge_actions {
                            twoda
                                .get_row_mut(irow)
                                .unwrap()
                                .iter_mut()
                                .zip(src_row.iter())
                                .zip(merge_actions.iter())
                                .for_each(|((dst, src), action)| {
                                    if *action == MergeAction::Set {
                                        *dst = src.clone();
                                    }
                                });
                        } else {
                            twoda.get_row_mut(irow).unwrap().clone_from_slice(src_row);
                        }
                    }
                    RowMergeAction::Conflict => {
                        let b_row = if let Some(merge_actions) = merge_actions {
                            twoda
                                .get_row(irow)
                                .unwrap()
                                .iter()
                                .zip(src_row.iter())
                                .zip(merge_actions.iter())
                                .map(|((dst, src), action)| match action {
                                    MergeAction::Set | MergeAction::Expect => src.clone(),
                                    MergeAction::Ignore => dst.clone(),
                                })
                                .collect::<Vec<_>>()
                        } else {
                            src_row.to_vec()
                        };

                        eprintln!("Conflict on row {}:", irow);
                        eprintln!(
                            "{}",
                            TwoDA::encode_rows(
                                &[(irow, dest_row), (irow, &b_row)],
                                Some(twoda.get_columns()),
                                false
                            )
                        );

                        loop {
                            // executes once, unless `continue` is called
                            eprintln!(
                                "'A' or 'B' to choose first (input 2DA) or second (merge 2DA) \
                                 version, or write the full row to set"
                            );

                            let ans = if let Some(ans) = &args.conflict_use {
                                // Auto answer
                                match ans {
                                    ConflictAns::A => Cow::from("A"),
                                    ConflictAns::B => Cow::from("B"),
                                }
                            } else {
                                // Read ans from stdin
                                use std::io::BufRead;
                                let mut line = String::new();
                                std::io::stdin()
                                    .lock()
                                    .read_line(&mut line)
                                    .expect("Could not read line");
                                Cow::from(line)
                            };

                            match ans.trim() {
                                "A" | "a" => {}
                                "B" | "b" => {
                                    let row = twoda.get_row_mut(irow).unwrap();
                                    row.clone_from_slice(b_row.as_ref());
                                }
                                _ => {
                                    if !ans.chars().all(char::is_whitespace) {
                                        match TwoDA::parse_row(&ans) {
                                            Ok((_, new_row)) => {
                                                if new_row.len() != columns_len + 1 {
                                                    eprintln!(
                                                        "Bad number of columns: Expected {}, got \
                                                         {}",
                                                        columns_len + 1,
                                                        new_row.len()
                                                    );
                                                    continue;
                                                }
                                                let row = twoda.get_row_mut(irow).unwrap();
                                                row.clone_from_slice(&new_row[1..]);
                                            }
                                            Err(e) => {
                                                eprintln!("Failed to parse provided row: {}", e);
                                                continue;
                                            }
                                        }
                                    }
                                }
                            }

                            break;
                        }
                    }
                }
            }
        }
        Ok(())
    }()
    .unwrap_or_else(|err| {
        eprintln!("Failed to modify 2DA: {}", err);
        exit(1);
    });

    // Serialize data
    || -> Result<(), Box<dyn std::error::Error>> {
        let data: Vec<u8> = match args.output_format {
            OutputFormat::Auto => panic!(),
            OutputFormat::TwoDA => twoda.to_string(false).into_bytes(),
            OutputFormat::TwoDAMini => twoda.to_string(true).into_bytes(),
            OutputFormat::Json => serde_json::to_string_pretty(&twoda)?.into_bytes(),
            OutputFormat::JsonMini => serde_json::to_string(&twoda)?.into_bytes(),
            OutputFormat::Yaml => serde_yml::to_string(&twoda)?.into_bytes(),
        };

        let output = args.output.unwrap_or("-".into());
        if output != Path::new("-") {
            write(output, data).map_err(|err| Box::new(err) as Box<dyn std::error::Error>)
        } else {
            use std::io::Write;
            std::io::stdout()
                .write_all(&data)
                .map(|_res| ())
                .map_err(|err| Box::new(err) as Box<dyn std::error::Error>)?;
            Ok(())
        }
    }()
    .unwrap_or_else(|err| {
        eprintln!("Failed to serialize 2DA: {}", err);
        exit(1);
    });

    Ok(())
}

#[test]
fn verify_cli() {
    use clap::CommandFactory;
    Args::command().debug_assert()
}
