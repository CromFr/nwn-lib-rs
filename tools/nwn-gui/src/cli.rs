use std::path::{Path, PathBuf};

use clap::Parser;

#[derive(Debug, Clone, clap::ValueEnum)]
pub enum OutputFormat {
    /// Guess based on file extension
    Auto,
    /// NWN2 XML format, readable by NWN2 and some XML parsers that support multiple roots
    NWN2Xml,
    /// Standard XML format, readable any XML parser (but not compatible with NWN2)
    StdXml,
    /// PugJS format (see https://pugjs.org)
    Pug,
}
impl OutputFormat {
    pub fn guess(file_path: &Path) -> Option<Self> {
        if file_path == Path::new("-") {
            return Some(Self::NWN2Xml);
        }
        let extension = file_path
            .extension()?
            .to_ascii_lowercase()
            .into_string()
            .ok()?;
        match extension.as_str() {
            "xml" => Some(Self::NWN2Xml),
            "pug" => Some(Self::Pug),
            _ => None,
        }
    }
}

/// Read and write Neverwinter Nights XML files
#[derive(Parser, Debug)]
#[command(author, version=nwn_lib_rs::NWN_LIB_RS_VERSION, about, long_about = None)]
pub struct Args {
    /// Input file. '-' for reading from stdin
    #[arg()]
    pub input: PathBuf,

    /// Output file. Set to '-' for writing to stdout
    #[arg(short, long)]
    pub output: Option<PathBuf>,

    /// Output file format.
    #[arg(short = 'O', long, default_value = "auto")]
    pub output_format: OutputFormat,

    /// Edit the file in-place, overwriting the same file
    #[arg(long, conflicts_with = "output")]
    pub in_place: bool,

    /// Do not write anything, only parse input file
    #[arg(long, conflicts_with = "output", conflicts_with = "in_place")]
    pub check: bool,
}
