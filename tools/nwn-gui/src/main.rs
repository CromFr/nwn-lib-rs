#![allow(incomplete_features)]
#![feature(generic_const_exprs)]

use clap::Parser;

use nwn_lib_rs::gui::*;

mod cli;
use cli::*;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let mut args = Args::parse();

    if args.in_place {
        args.output = Some(args.input.clone());
    }

    if matches!(args.output_format, OutputFormat::Auto) {
        if let Some(file_path) = &args.output {
            match OutputFormat::guess(file_path) {
                Some(t) => args.output_format = t,
                None => {
                    return Err(
                        "Cannot guess output file format. Try providing -O argument.".into(),
                    );
                }
            }
        } else {
            args.output_format = OutputFormat::NWN2Xml;
        }
    }

    let reader = GuiXmlEventReader::new(std::fs::File::open(args.input)?, true);

    let (mut final_output, retain_output): (Box<dyn std::io::Write>, bool) =
        if let Some(out) = args.output {
            match out.to_str().expect("non utf8 path") {
                "-" => (Box::new(std::io::stdout()), true),
                path => (Box::new(std::fs::File::create(path)?), false),
            }
        } else {
            (Box::new(std::io::stdout()), true)
        };

    let retain_output = retain_output || args.check || args.in_place;

    if retain_output {
        let mut retained_output = Box::new(std::io::Cursor::new(vec![]));

        match args.output_format {
            OutputFormat::NWN2Xml => write_nwn2xml(reader, &mut retained_output)?,
            OutputFormat::StdXml => write_stdxml(reader, &mut retained_output)?,
            OutputFormat::Pug => write_pug(reader, &mut retained_output)?,
            _ => panic!(),
        }

        if !args.check {
            final_output.write_all(retained_output.into_inner().as_slice())?;
            final_output.write(b"\n")?;
        }
    } else {
        match args.output_format {
            OutputFormat::NWN2Xml => write_nwn2xml(reader, &mut final_output)?,
            OutputFormat::StdXml => write_stdxml(reader, &mut final_output)?,
            OutputFormat::Pug => write_pug(reader, &mut final_output)?,
            _ => panic!(),
        }
        final_output.write(b"\n")?;
    };

    Ok(())
}

fn write_nwn2xml<W: std::io::Write, R: std::io::Read>(
    mut reader: GuiXmlEventReader<R>,
    output: &mut W,
) -> Result<(), Box<dyn std::error::Error>> {
    let mut writer = xml::writer::EmitterConfig::new()
        .perform_indent(true)
        .create_writer(output);
    loop {
        let ev = reader.next()?;
        if let XmlEvent::EndDocument = ev {
            break;
        }
        if let Some(ev) = ev.as_writer_event() {
            writer.write(ev)?;
        }
    }
    Ok(())
}

fn write_stdxml<W: std::io::Write, R: std::io::Read>(
    mut reader: GuiXmlEventReader<R>,
    output: &mut W,
) -> Result<(), Box<dyn std::error::Error>> {
    let mut writer = xml::writer::EmitterConfig::new()
        .perform_indent(true)
        .create_writer(output);

    loop {
        let ev = reader.next()?;

        if let XmlEvent::EndDocument = ev {
            writer.write(xml::writer::XmlEvent::EndElement {
                name: Some(xml::name::Name::local("NWN2")),
            })?;
            break;
        }

        if let Some(ev) = ev.as_writer_event() {
            writer.write(ev)?;
        }

        if let XmlEvent::StartDocument {
            version: _,
            encoding: _,
            standalone: _,
        } = ev
        {
            writer.write(xml::writer::XmlEvent::StartElement {
                name: xml::name::Name::local("NWN2"),
                attributes: vec![].into(),
                namespace: std::borrow::Cow::Owned(xml::namespace::Namespace::empty()),
            })?;
        }
    }
    Ok(())
}

fn write_pug<W: std::io::Write, R: std::io::Read>(
    mut reader: GuiXmlEventReader<R>,
    output: &mut W,
) -> Result<(), Box<dyn std::error::Error>> {
    let mut indent = String::new();
    loop {
        let ev = reader.next()?;
        match ev {
            XmlEvent::StartDocument {
                version: _,
                encoding: _,
                standalone: _,
            } => output.write_all("doctype xml\n".as_bytes())?,
            XmlEvent::EndDocument {} => {
                break;
            }
            XmlEvent::StartElement {
                name,
                attributes,
                namespace: _,
            } => {
                let attr = attributes
                    .iter()
                    .map(|attr| format!("{}={:?}", attr.name.local_name, attr.value))
                    .collect::<Vec<_>>()
                    .join(" ");
                output
                    .write_all(format!("{}{}({})\n", indent, name.local_name, attr).as_bytes())?;
                indent.push_str("  ");
            }
            XmlEvent::EndElement { name: _ } => {
                indent.pop();
                indent.pop();
            }
            XmlEvent::Comment(text) => {
                for line in text.lines() {
                    output.write_all(format!("{}// {}\n", indent, line).as_bytes())?;
                }
            }
            _ => {}
        }
    }
    Ok(())
}

#[test]
fn verify_cli() {
    use clap::CommandFactory;
    Args::command().debug_assert()
}
