#!/bin/bash
set -euo pipefail

function chksum() {
	sha1sum "$1" | cut -d' ' -f1
}

set -x

for F in unittest/*.xml; do
    nwn-gui --check "$F"

    nwn-gui "$F" -O std-xml | xmllint --noout -
done

[[ $(nwn-gui unittest/campaign.xml -O std-xml | xmllint - --xpath 'string(NWN2/UIScene/@name)') == SCREEN_CAMPAIGNLIST ]]
[[ $(nwn-gui unittest/campaign.xml -O std-xml | xmllint - --xpath 'string(NWN2/UIScene/@OnAdd)') == 'UIScene_OnAdd_CreateCampaignList("CampaignListBox")' ]]
[[ $(nwn-gui unittest/campaign.xml -O std-xml | xmllint - --xpath 'string(NWN2/UIScene/@fullscreen)') == 'true' ]]

[[ $(nwn-gui unittest/campaign.xml -O std-xml | xmllint - --xpath 'string(//UIIcon[@img = "main_sub_bg.tga"]/@width)') == '1024' ]]
[[ $(nwn-gui unittest/campaign.xml -O std-xml | xmllint - --xpath 'string(//UIButton[@name = "CANCEL_BUTTON"]/@OnLeftClick)') == 'UIButton_Input_ScreenOpen("SCREEN_GAMECHOICE","true")' ]]

echo "Test success"
