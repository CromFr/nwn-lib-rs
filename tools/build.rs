use clap_complete::{generate_to, shells, Generator};

use clap::{Command, CommandFactory};

macro_rules! PKG_NAME {
    () => {
        std::env!("CARGO_PKG_NAME")
    };
}

include!(concat!(PKG_NAME!(), "/src/cli.rs"));

fn write_compl<Sh: Generator + std::fmt::Debug>(
    shell: Sh,
    cmd: &mut Command,
    out_path: &std::path::Path,
) -> Result<(), Box<dyn std::error::Error>> {
    // let out_path = out_path.join(dir_name);
    if !std::fs::exists(&out_path).unwrap_or(false) {
        std::fs::create_dir_all(&out_path)?;
    }

    let shell_name = format!("{:?}", &shell);

    let res_path = generate_to(shell, cmd, PKG_NAME!(), &out_path)?;
    println!(
        "cargo:warning={} completions generated to: {:?}",
        shell_name,
        std::fs::canonicalize(res_path)?
    );
    Ok(())
}

fn main() {
    let outdir = match std::env::var_os("OUT_DIR") {
        None => return,
        Some(outdir) => {
            std::path::Path::new(outdir.to_str().expect("invalid path")).join("../../../share")
        }
    };

    let mut cmd = Args::command();

    write_compl(
        shells::Bash,
        &mut cmd,
        &outdir.join("bash-completion/completions"),
    )
    .unwrap();
    write_compl(
        shells::Fish,
        &mut cmd,
        &outdir.join("fish/vendor_completions.d"),
    )
    .unwrap();
    write_compl(shells::Zsh, &mut cmd, &outdir.join("zsh/site-functions")).unwrap();
    write_compl(
        shells::PowerShell,
        &mut cmd,
        &outdir.join("powershell-completion"),
    )
    .unwrap();
}
