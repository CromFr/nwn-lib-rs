#!/bin/bash
set -euo pipefail

function chksum() {
	sha1sum "$1" | cut -d' ' -f1
}

set -x

# Hak
(( $(nwn-erf list unittest/test.hak | wc -l) == 2 ))
[[ "$(nwn-erf info unittest/test.hak | grep file_type:)" == 'file_type: "HAK "' ]]

rm -rf /tmp/nwn-lib-rs-erf
mkdir -p /tmp/nwn-lib-rs-erf
nwn-erf unpack unittest/test.hak -o /tmp/nwn-lib-rs-erf
(( $(find /tmp/nwn-lib-rs-erf/ -type f | wc -l) == 2 ))
[[ "$(chksum /tmp/nwn-lib-rs-erf/eye.tga)" == "9be888518c8f8e03037f1de5a019b16d5f838a65" ]]
[[ "$(chksum /tmp/nwn-lib-rs-erf/test.txt)" == "33ab5639bfd8e7b95eb1d8d0b87781d4ffea4d5d" ]]


# Repacking
nwn-erf pack /tmp/nwn-lib-rs-erf.hak /tmp/nwn-lib-rs-erf --date="2016-06-08" --sort
[[ "$(nwn-erf info /tmp/nwn-lib-rs-erf.hak | grep file_type:)" == 'file_type: "HAK "' ]]
[[ "$(nwn-erf info /tmp/nwn-lib-rs-erf.hak | grep build_date:)" == 'build_date: 2016-06-08' ]]
[[ "$(chksum /tmp/nwn-lib-rs-erf.hak)" == "$(chksum /tmp/nwn-lib-rs-erf.hak)" ]]

nwn-erf pack /tmp/nwn-lib-rs-erf.hak /tmp/nwn-lib-rs-erf --type=mod --desc="1:Hello world"
[[ "$(nwn-erf info /tmp/nwn-lib-rs-erf.hak | grep 'file_type:')" == 'file_type: "MOD "' ]]
[[ "$(nwn-erf info /tmp/nwn-lib-rs-erf.hak | grep 'build_date:')" == 'build_date: 1900-01-01' ]]
[[ "$(nwn-erf info /tmp/nwn-lib-rs-erf.hak | grep 'description:1')" == 'description:1:"Hello world"' ]]

rm -rf /tmp/nwn-lib-rs-erf /tmp/nwn-lib-rs-erf.hak
mkdir -p /tmp/nwn-lib-rs-erf

# Repairing
cp unittest/nwn2_module.mod /tmp/nwn-lib-rs-erf.mod
truncate --size=-5K /tmp/nwn-lib-rs-erf.mod
nwn-erf list /tmp/nwn-lib-rs-erf.mod >/dev/null # Works since only the file head is parsed
nwn-erf unpack /tmp/nwn-lib-rs-erf.mod -o /tmp/nwn-lib-rs-erf > /dev/null && exit 1  # Fails without --repair
rm -rf /tmp/nwn-lib-rs-erf/*
nwn-erf unpack /tmp/nwn-lib-rs-erf.mod -o /tmp/nwn-lib-rs-erf --continue >/dev/null

echo "Test success"