#![allow(incomplete_features)]
#![feature(generic_const_exprs)]

use std::path::{Path, PathBuf};
use std::process::exit;

use clap::Parser;
use nwn_lib_rs::erf::{Erf, Resource, ResourceData};

mod cli;
use cli::*;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args = Args::parse();
    match args.command {
        Commands::Pack(args) => pack_erf(args)?,
        Commands::Unpack(args) => unpack_erf(args)?,
        Commands::Info(args) => info_erf(args)?,
        Commands::List(args) => list_erf(args)?,
    }

    Ok(())
}

fn list_files_recurse(dir: &Path) -> std::io::Result<Vec<PathBuf>> {
    let mut paths = vec![];
    if dir.is_dir() {
        for entry in std::fs::read_dir(dir)? {
            let entry = entry?;
            let path = entry.path();
            if path.is_dir() {
                paths.extend(list_files_recurse(&path)?);
            } else {
                paths.push(path)
            }
        }
    }
    Ok(paths)
}
fn pack_erf(args: PackArgs) -> Result<(), Box<dyn std::error::Error>> {
    use chrono::prelude::*;

    let build_date = match args.date.unwrap_or("".to_string()).as_str() {
        "" => NaiveDate::from_yo_opt(1900, 1).unwrap(),
        "now" => chrono::Local::now().date_naive(),
        datestr => chrono::NaiveDate::parse_from_str(datestr, "%Y-%m-%d")?,
    };

    let erf_type = args
        .erf_type
        .unwrap_or(ErfFileType::guess(&args.output_file).ok_or(format!(
            "could not guess ERF file type based on name {:?}",
            &args.output_file
        ))?);

    let mut erf = Erf::new(
        erf_type.to_fixed_string(),
        args.nwn1,
        build_date,
        args.desc_strref,
    );

    for desc in args.desc {
        || -> Result<(), Box<dyn std::error::Error>> {
            let sep = desc.find(':').ok_or("missing ':' separator")?;

            let (lang_id, text) = desc.split_at(sep);
            let text = &text[sep..];
            let lang_id = u32::from_str_radix(lang_id, 10)?;
            erf.description.insert(lang_id, text.to_string());

            Ok(())
        }()
        .unwrap_or_else(|err| {
            eprintln!(
                "failed to set ERF localized description with arg {:?}: {}",
                desc, err
            );
            exit(1);
        });
    }

    for path in args.file {
        let res_list = if path.is_dir() {
            list_files_recurse(&path)?
        } else {
            vec![path]
        };

        let res: Result<Vec<_>, _> = res_list
            .into_iter()
            .map(|p| -> Result<_, _> { Resource::from_file(&p, None) })
            .collect();

        erf.resources.extend(res?);
    }

    if args.sort {
        erf.resources.sort_by_cached_key(Resource::get_filename)
    }

    let mut out_file = std::fs::File::create(args.output_file)?;
    erf.write_stream(&mut out_file)?;

    Ok(())
}

fn unpack_erf(args: UnpackArgs) -> Result<(), Box<dyn std::error::Error>> {
    let mut erf_file = std::fs::File::open(&args.input_file)?;
    let erf = Erf::from_stream_head(&mut erf_file)?;

    let output_folder = if let Some(path) = args.output_folder {
        path
    } else {
        let folder: PathBuf = args
            .input_file
            .file_stem()
            .expect("could not get input file name")
            .into();
        if !folder.is_dir() {
            std::fs::create_dir(&folder)?;
        }
        folder
    };

    for resource in erf.resources {
        let file_name: PathBuf = resource.get_filename().into();

        // Filter by file name
        if args.filter.len() > 0 {
            let matching = args
                .filter
                .iter()
                .any(|pattern| pattern.matches_path(&file_name));
            if !matching {
                continue;
            }
        }

        let out_path: PathBuf = [&output_folder, &file_name].iter().collect();

        // Avoid overwriting
        if !args.check && !args.overwrite && out_path.exists() {
            return Err(format!(
                "The file already exists: {:?}. Use --overwrite to overwrite existing files.",
                out_path
            )
            .into());
        }

        // Write file
        match resource.data.archiveoffset_seek(&mut erf_file) {
            Ok(data_res) => {
                if args.verbose {
                    println!(" {:?} => {:?}", file_name, out_path);
                }
                if !args.check {
                    // Write file
                    std::fs::write(out_path, data_res)?;
                }
            }
            Err(e) => {
                eprintln!("Failed to fetch data for {:?}: {}", resource, e);
                if args.continue_ {
                    continue;
                } else {
                    return Err(e.into());
                }
            }
        }
    }

    Ok(())
}

fn info_erf(args: InfoArgs) -> Result<(), Box<dyn std::error::Error>> {
    let mut erf_file = std::fs::File::open(&args.input_file)?;
    let erf = Erf::from_stream_head(&mut erf_file)?;

    println!("file_type: {:?}", erf.header.file_type.as_str(),);
    println!("file_version: {:?}", erf.header.file_version.as_str());
    println!(
        "build_date: {}",
        erf.header.build_date.format("%Y-%m-%d").to_string()
    );
    println!("description_strref: {}", erf.header.description_strref);
    for (lang_id, text) in &erf.description {
        println!("description:{}:{:?}", lang_id, text);
    }
    println!("resources: {} files", erf.resources.len());

    Ok(())
}

fn list_erf(args: ListArgs) -> Result<(), Box<dyn std::error::Error>> {
    let mut erf_file = std::fs::File::open(&args.input_file)?;
    let erf = Erf::from_stream_head(&mut erf_file)?;

    for resource in erf.resources {
        let suffix = if args.offsets {
            let ResourceData::ArchiveOffset { offset, size } = resource.data else {
                panic!()
            };
            format!(", start={} end={}", offset, offset + size)
        } else {
            "".to_string()
        };
        println!(
            "{:35} {} bytes{}",
            &resource.get_filename(),
            resource.data.len().expect("Unknown length"),
            suffix
        );
    }

    Ok(())
}
