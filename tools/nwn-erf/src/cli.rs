use std::path::{Path, PathBuf};

use clap::{Parser, Subcommand};
use nwn_lib_rs::erf::FileType;

use nwn_lib_rs::parsing::FixedSizeString;

/// Read and write Neverwinter Nights ERF files (erf, hak, mod, pwc)
#[derive(Debug, Parser)]
#[command(author, version=nwn_lib_rs::NWN_LIB_RS_VERSION, about, long_about = None)]
pub struct Args {
    #[command(subcommand)]
    pub command: Commands,
}
#[derive(Debug, Subcommand)]
pub enum Commands {
    /// Create a new ERF archive, containing multiple files
    Pack(PackArgs),
    /// Unpack all files from a ERF archive
    Unpack(UnpackArgs),
    /// Print ERF head information
    Info(InfoArgs),
    /// List all files in the ERF archive
    List(ListArgs),
}

#[derive(Debug, Clone, clap::ValueEnum)]
pub enum ErfFileType {
    Erf,
    Hak,
    Mod,
    Pwc,
}
impl ErfFileType {
    pub fn to_fixed_string(&self) -> FixedSizeString<4> {
        match self {
            ErfFileType::Erf => FileType::Erf.to_fixed_string(),
            ErfFileType::Hak => FileType::Hak.to_fixed_string(),
            ErfFileType::Mod => FileType::Mod.to_fixed_string(),
            ErfFileType::Pwc => FileType::Pwc.to_fixed_string(),
        }
    }
    pub fn guess(file_name: &Path) -> Option<Self> {
        Some(match FileType::guess(file_name)? {
            FileType::Erf => ErfFileType::Erf,
            FileType::Hak => ErfFileType::Hak,
            FileType::Mod => ErfFileType::Mod,
            FileType::Pwc => ErfFileType::Pwc,
        })
    }
}
#[derive(Debug, clap::Args)]
pub struct PackArgs {
    /// Output ERF file
    #[arg()]
    pub output_file: PathBuf,

    /// Files or folders to add into the ERF file
    #[arg()]
    pub file: Vec<PathBuf>,

    /// Generate NWN1 ERF, with 16-char resource names (instead of 32-char)
    #[arg(long)]
    pub nwn1: bool,

    /// Set ERF creation date. Leave unset for reproducible ERF files
    #[arg(long)]
    pub date: Option<String>,

    /// Set ERF description strref (only used by .mod files)
    #[arg(long)]
    pub desc_strref: Option<u32>,

    /// Set a ERF localized description entry (only used by .mod files). Format: --desc=<lang_id>:<text>
    #[arg(long)]
    pub desc: Vec<String>,

    /// Output file type. Automatically detect based on file name if unset.
    #[arg(short = 't', long = "type")]
    pub erf_type: Option<ErfFileType>,

    /// Sort by file name. Required for reproducible ERF files
    #[arg(long)]
    pub sort: bool,
}
#[derive(Debug, clap::Args)]
pub struct UnpackArgs {
    /// ERF file to unpack
    #[arg()]
    pub input_file: PathBuf,

    /// Output folder for storing ERF files. By default a folder with the same
    /// name as the ERF file will be created
    #[arg(short, long)]
    pub output_folder: Option<PathBuf>,

    /// Continue unpacking the file even if some files cannot be recovered
    #[arg(long = "continue")]
    pub continue_: bool,

    /// Overwrite existing files
    #[arg(long)]
    pub overwrite: bool,

    /// Print the name and location of each unpacked file
    #[arg(short, long)]
    pub verbose: bool,

    /// Only extract files matching these glob patterns. Example: 'underdark_*.utc'
    #[arg()]
    pub filter: Vec<glob::Pattern>,

    /// Only check all files can be unpacked, without writing any file on disk
    #[arg(short, long)]
    pub check: bool,
}
#[derive(Debug, clap::Args)]
pub struct InfoArgs {
    /// ERF file to read
    #[arg()]
    pub input_file: PathBuf,
}
#[derive(Debug, clap::Args)]
pub struct ListArgs {
    /// ERF file to read
    #[arg()]
    pub input_file: PathBuf,

    /// Show file start & stop byte offsets
    #[arg(long)]
    pub offsets: bool,
}

#[test]
fn verify_cli() {
    use clap::CommandFactory;
    Args::command().debug_assert()
}
