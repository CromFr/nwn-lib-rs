
# nwn-lib-rs

Parsing library and command-line tools for Neverwinter Nights 1 and 2 data files

# Downloads

See the [Release page](https://gitlab.com/CromFr/nwn-lib-rs/-/releases)

# Tools

### nwn-gff

- Read and write GFF files (ARE, GIT, DLG, BIC, UTC, UTI, ...)
- Convert to and from JSON, YAML
- Display content as text (easily diff-able with git)
- Set GFF values
- Dump internal GFF structure (for debugging only)

### nwn-erf

- Create ERF archives (ERF, PWC, HAK, MOD)
- Unpack files inside a ERF archive
- Recover files when the ERF file has been truncated (when possible)
- List files in a ERF archive
- View ERF metadata

### nwn-tlk

- Read and write TLK files (and repair)
- Convert to and from JSON, YAML
- Display content as text (easily diff-able with git)
- Edit TLK entries (text, sounds, ...)
- Merge TLK files together (with basic conflict management)

### nwn-2da

- Read and write 2DA files (and repair)
- Convert to and from JSON, YAML
- Merge 2DA files together (with basic conflict management)
- Supports 2DAM, a custom file format for easier merge operations

### nwn-trn

- Read and write NWN2 TRN files (TRX, TRN)
- Print TRN packets information
- Pack / unpack TRN packets into separate files


# Tips & tricks

## Improve git diff and merge tool

nwn-lib-rs can be used to improve git diffs to view changes made inside GFF files (blueprints, journal, areas, ..) and TLK files, and also quickly merge TLK content in case of merge conflicts.

Assuming the nwn-lib-rs tools have been installed in `C:\Program Files\nwn-lib-rs\`, open a terminal (git bash on windows) and run:
```bash
git config --global diff.gff.textconv '"C:/Program Files/nwn-lib-rs/nwn-gff"'

git config --global diff.tlk.textconv '"C:/Program Files/nwn-lib-rs/nwn-tlk"'
git config --global diff.tlk.xfuncname '^\s*([0-9]+)>'
git config --global mergetool.tlk.cmd '"C:/Program Files/nwn-lib-rs/nwn-tlk" "$LOCAL" -I tlk --merge "$REMOTE" --merge-format tlk -o "$MERGED" -O tlk'
```

In your module directory, or any git directory containing NWN files, create a file named `.gitattributes`:
```gitattributes
# Areas
*.[aA][rR][eE] diff=gff
*.[gG][iI][cC] diff=gff
*.[gG][iI][tT] diff=gff

# Dialogs
*.[dD][lL][gG] diff=gff

# Module
*.[fF][aA][cC] diff=gff
*.[iI][fF][oO] diff=gff
*.[jJ][rR][lL] diff=gff

# Blueprints
*.[uU][lL][tT] diff=gff
*.[uU][pP][eE] diff=gff
*.[uU][tT][cCdDeEiImMpPrRtTwW] diff=gff

# Others
*.[pP][fF][bB] diff=gff
*.[tT][lL][kK] diff=tlk merge=tlk
```
