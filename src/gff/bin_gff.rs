//! GFF file format. Read-only binary representation of files.
//!
//! Provides fast and memory-efficient parsing of GFF data, by keeping the
//! flat memory layout of the GFF file

#![allow(dead_code)]

use encoding_rs::WINDOWS_1252;
use nom::error::ErrorKind;
use nom::multi::fill;
use nom::number::streaming::*;
use std::collections::HashMap;
use std::mem;

use crate::parsing::*;

/// GFF file header
#[derive(Debug)]
pub struct Header {
    pub file_type: FixedSizeString<4>,
    pub file_version: FixedSizeString<4>,
    struct_offset: u32,
    struct_count: u32,
    field_offset: u32,
    field_count: u32,
    label_offset: u32,
    label_count: u32,
    field_data_offset: u32,
    field_data_size: u32,
    field_indices_offset: u32,
    field_indices_size: u32,
    list_indices_offset: u32,
    list_indices_size: u32,
}
impl Header {
    pub fn new(file_type: FixedSizeString<4>, file_version: FixedSizeString<4>) -> Self {
        Self {
            file_type,
            file_version,
            struct_offset: 0,
            struct_count: 0,
            field_offset: 0,
            field_count: 0,
            label_offset: 0,
            label_count: 0,
            field_data_offset: 0,
            field_data_size: 0,
            field_indices_offset: 0,
            field_indices_size: 0,
            list_indices_offset: 0,
            list_indices_size: 0,
        }
    }
    fn from_bytes(input: &[u8]) -> NWNParseResult<Self> {
        let (input, file_type) = FixedSizeString::from_bytes(input)?;
        let (input, file_version) = FixedSizeString::from_bytes(input)?;
        let (input, struct_offset) = le_u32(input)?;
        let (input, struct_count) = le_u32(input)?;
        let (input, field_offset) = le_u32(input)?;
        let (input, field_count) = le_u32(input)?;
        let (input, label_offset) = le_u32(input)?;
        let (input, label_count) = le_u32(input)?;
        let (input, field_data_offset) = le_u32(input)?;
        let (input, field_data_size) = le_u32(input)?;
        let (input, field_indices_offset) = le_u32(input)?;
        let (input, field_indices_size) = le_u32(input)?;
        let (input, list_indices_offset) = le_u32(input)?;
        let (input, list_indices_size) = le_u32(input)?;

        Ok((
            input,
            Self {
                file_type,
                file_version,
                struct_offset,
                struct_count,
                field_offset,
                field_count,
                label_offset,
                label_count,
                field_data_offset,
                field_data_size,
                field_indices_offset,
                field_indices_size,
                list_indices_offset,
                list_indices_size,
            },
        ))
    }
    fn to_bytes(&self) -> Vec<u8> {
        let mut ret = Vec::with_capacity(14 * mem::size_of::<u32>());
        ret.extend(self.file_type.as_bytes());
        ret.extend(self.file_version.as_bytes());
        ret.extend(self.struct_offset.to_le_bytes());
        ret.extend(self.struct_count.to_le_bytes());
        ret.extend(self.field_offset.to_le_bytes());
        ret.extend(self.field_count.to_le_bytes());
        ret.extend(self.label_offset.to_le_bytes());
        ret.extend(self.label_count.to_le_bytes());
        ret.extend(self.field_data_offset.to_le_bytes());
        ret.extend(self.field_data_size.to_le_bytes());
        ret.extend(self.field_indices_offset.to_le_bytes());
        ret.extend(self.field_indices_size.to_le_bytes());
        ret.extend(self.list_indices_offset.to_le_bytes());
        ret.extend(self.list_indices_size.to_le_bytes());
        ret
    }
    pub fn dump(&self) -> String {
        let mut ret = String::new();
        ret += &format!("file_type: {:?}\n", self.file_type);
        ret += &format!("file_version: {:?}\n", self.file_version);
        ret += &format!(
            "structs: offset={} count={}\n",
            self.struct_offset, self.struct_count
        );
        ret += &format!(
            "fields: offset={} count={}\n",
            self.field_offset, self.field_count
        );
        ret += &format!(
            "labels: offset={} count={}\n",
            self.label_offset, self.label_count
        );
        ret += &format!(
            "field_data: offset={} size={}\n",
            self.field_data_offset, self.field_data_size
        );
        ret += &format!(
            "field_indices: offset={} size={}\n",
            self.field_indices_offset, self.field_indices_size
        );
        ret += &format!(
            "list_indices: offset={} size={}",
            self.list_indices_offset, self.list_indices_size
        );
        ret
    }

    /// Update header offsets and sizes and return total file size
    pub fn update(&mut self, gff: &Gff) -> usize {
        let mut total_len = 14 * mem::size_of::<u32>(); // Header

        self.struct_offset = total_len as u32;
        self.struct_count = gff.structs.len() as u32;
        total_len += gff.structs.len() * 3 * mem::size_of::<u32>();

        self.field_offset = total_len as u32;
        self.field_count = gff.fields.len() as u32;
        total_len += gff.fields.len() * 3 * mem::size_of::<u32>();

        self.label_offset = total_len as u32;
        self.label_count = gff.labels.len() as u32;
        total_len += gff.labels.len() * 16;

        self.field_data_offset = total_len as u32;
        self.field_data_size = gff.field_data.len() as u32;
        total_len += gff.field_data.len() * mem::size_of::<u8>();

        self.field_indices_offset = total_len as u32;
        self.field_indices_size = (gff.field_indices.len() * mem::size_of::<u32>()) as u32;
        total_len += gff.field_indices.len() * mem::size_of::<u32>();

        self.list_indices_offset = total_len as u32;
        self.list_indices_size = (gff.list_indices.len() * mem::size_of::<u32>()) as u32;
        total_len += gff.list_indices.len() * mem::size_of::<u32>();

        total_len
    }
}

/// GFF struct
#[derive(Debug, Default)]
pub struct Struct {
    pub id: u32,
    pub data_or_data_offset: u32,
    pub field_count: u32,
}

impl Struct {
    fn from_bytes(input: &[u8]) -> NWNParseResult<Self> {
        let (input, id) = le_u32(input)?;
        let (input, data_or_data_offset) = le_u32(input)?;
        let (input, field_count) = le_u32(input)?;
        Ok((
            input,
            Self {
                id,
                data_or_data_offset,
                field_count,
            },
        ))
    }
    fn to_bytes(&self) -> Vec<u8> {
        let mut ret = self.id.to_le_bytes().to_vec();
        ret.extend(self.data_or_data_offset.to_le_bytes());
        ret.extend(self.field_count.to_le_bytes());
        ret
    }

    pub fn get_field_indices<'a>(&'a self, field_indices: &'a [u32]) -> Option<&'a [u32]> {
        match self.field_count {
            0 => Some(&[]),
            1 => Some(std::array::from_ref(&self.data_or_data_offset)),
            _ => {
                let start = self.data_or_data_offset as usize / mem::size_of::<u32>();
                let end = start + self.field_count as usize;
                field_indices.get(start..end)
            }
        }
    }
}

/// GFF Field (label + value)
#[derive(Debug)]
pub struct Field {
    pub label_index: u32,
    pub value: FieldValue,
}
impl Default for Field {
    fn default() -> Self {
        Self {
            label_index: 0,
            value: FieldValue::Invalid,
        }
    }
}

impl Field {
    fn from_bytes<'a>(input: &'a [u8], header: &Header) -> NWNParseResult<'a, Self> {
        let (input, type_id) = le_u32(input)?;
        let (data_input, label_index) = le_u32(input)?;
        let (input, data_or_data_offset) = le_u32(data_input)?;

        if label_index >= header.label_count {
            return Err(nom::Err::Error(NWNParseError::from_string(format!(
                "field label index {} is out of bounds",
                label_index
            ))));
        }

        let value = match type_id {
            0 => FieldValue::Byte(le_u8(data_input)?.1),
            1 => FieldValue::Char(le_i8(data_input)?.1),
            2 => FieldValue::Word(le_u16(data_input)?.1),
            3 => FieldValue::Short(le_i16(data_input)?.1),
            4 => FieldValue::DWord(le_u32(data_input)?.1),
            5 => FieldValue::Int(le_i32(data_input)?.1),
            8 => FieldValue::Float(le_f32(data_input)?.1),
            6..=7 | 9..=13 => {
                // Complex types: data_or_data_offset is an offset in field_data
                if data_or_data_offset >= header.field_data_size {
                    return Err(nom::Err::Error(NWNParseError::from_string(format!(
                        "field data_or_data_offset {} points to out of bounds field_data",
                        data_or_data_offset
                    ))));
                }
                match type_id {
                    6 => FieldValue::DWord64 {
                        data_offset: data_or_data_offset,
                    },
                    7 => FieldValue::Int64 {
                        data_offset: data_or_data_offset,
                    },
                    9 => FieldValue::Double {
                        data_offset: data_or_data_offset,
                    },
                    10 => FieldValue::String {
                        data_offset: data_or_data_offset,
                    },
                    11 => FieldValue::ResRef {
                        data_offset: data_or_data_offset,
                    },
                    12 => FieldValue::LocString {
                        data_offset: data_or_data_offset,
                    },
                    13 => FieldValue::Void {
                        data_offset: data_or_data_offset,
                    },
                    _ => {
                        return Err(nom::Err::Error(NWNParseError::from_string(format!(
                            "invalid field type ID {}",
                            type_id
                        ))));
                    }
                }
            }
            14 => {
                if data_or_data_offset >= header.struct_count {
                    return Err(nom::Err::Error(NWNParseError::from_string(format!(
                        "field data_or_data_offset {} points to out of bounds struct",
                        data_or_data_offset
                    ))));
                }
                FieldValue::Struct {
                    struct_index: data_or_data_offset,
                }
            }
            15 => {
                if data_or_data_offset >= header.list_indices_size {
                    return Err(nom::Err::Error(NWNParseError::from_string(format!(
                        "field data_or_data_offset {} points to out of bounds list_indices",
                        data_or_data_offset
                    ))));
                }
                FieldValue::List {
                    list_offset: data_or_data_offset,
                }
            }
            _ => FieldValue::Invalid,
        };
        Ok((input, Field { label_index, value }))
    }

    fn to_bytes(&self) -> Vec<u8> {
        let mut ret = Vec::with_capacity(3 * mem::size_of::<u32>());
        ret.extend(self.value.get_type_id().to_le_bytes());
        ret.extend(self.label_index.to_le_bytes());
        let val_slice = match self.value {
            FieldValue::Invalid => panic!(),
            FieldValue::Byte(v) => {
                let mut vec = v.to_le_bytes().to_vec();
                vec.resize(4, 0);
                vec
            }
            FieldValue::Char(v) => {
                let mut vec = v.to_le_bytes().to_vec();
                vec.resize(4, 0);
                vec
            }
            FieldValue::Word(v) => {
                let mut vec = v.to_le_bytes().to_vec();
                vec.resize(4, 0);
                vec
            }
            FieldValue::Short(v) => {
                let mut vec = v.to_le_bytes().to_vec();
                vec.resize(4, 0);
                vec
            }
            FieldValue::DWord(v) => v.to_le_bytes().to_vec(),
            FieldValue::Int(v) => v.to_le_bytes().to_vec(),
            FieldValue::DWord64 { data_offset } => data_offset.to_le_bytes().to_vec(),
            FieldValue::Int64 { data_offset } => data_offset.to_le_bytes().to_vec(),
            FieldValue::Float(v) => v.to_le_bytes().to_vec(),
            FieldValue::Double { data_offset } => data_offset.to_le_bytes().to_vec(),
            FieldValue::String { data_offset } => data_offset.to_le_bytes().to_vec(),
            FieldValue::ResRef { data_offset } => data_offset.to_le_bytes().to_vec(),
            FieldValue::LocString { data_offset } => data_offset.to_le_bytes().to_vec(),
            FieldValue::Void { data_offset } => data_offset.to_le_bytes().to_vec(),
            FieldValue::Struct { struct_index } => struct_index.to_le_bytes().to_vec(),
            FieldValue::List { list_offset } => list_offset.to_le_bytes().to_vec(),
        };
        assert_eq!(val_slice.len(), 4);
        ret.extend(val_slice);

        ret
    }
}

/// GFF Field value
#[derive(Debug, Default)]
pub enum FieldValue {
    #[default]
    Invalid, // TODO: remove Invalid ?
    Byte(u8),
    Char(i8),
    Word(u16),
    Short(i16),
    DWord(u32),
    Int(i32),
    DWord64 {
        data_offset: u32,
    },
    Int64 {
        data_offset: u32,
    },
    Float(f32),
    Double {
        data_offset: u32,
    },
    String {
        data_offset: u32,
    },
    ResRef {
        data_offset: u32,
    },
    LocString {
        data_offset: u32,
    },
    Void {
        data_offset: u32,
    },
    Struct {
        struct_index: u32,
    },
    List {
        list_offset: u32,
    },
}
impl FieldValue {
    pub fn get_type_id(&self) -> u32 {
        match self {
            FieldValue::Byte(_) => 0,
            FieldValue::Char(_) => 1,
            FieldValue::Word(_) => 2,
            FieldValue::Short(_) => 3,
            FieldValue::DWord(_) => 4,
            FieldValue::Int(_) => 5,
            FieldValue::DWord64 { data_offset: _ } => 6,
            FieldValue::Int64 { data_offset: _ } => 7,
            FieldValue::Float(_) => 8,
            FieldValue::Double { data_offset: _ } => 9,
            FieldValue::String { data_offset: _ } => 10,
            FieldValue::ResRef { data_offset: _ } => 11,
            FieldValue::LocString { data_offset: _ } => 12,
            FieldValue::Void { data_offset: _ } => 13,
            FieldValue::Struct { struct_index: _ } => 14,
            FieldValue::List { list_offset: _ } => 15,
            FieldValue::Invalid => u32::max_value(),
        }
    }

    pub fn get_dword64(&self, field_data: &[u8]) -> Option<u64> {
        if let FieldValue::DWord64 { data_offset } = self {
            let data: &[u8] = field_data
                .get(*data_offset as usize..)
                .expect("bad data offset"); // TODO: check at parsing

            let (_, value) =
                le_u64::<_, (_, ErrorKind)>(data).expect("size should have been checked");
            Some(value)
        } else {
            None
        }
    }
    pub fn get_int64(&self, field_data: &[u8]) -> Option<i64> {
        if let FieldValue::DWord64 { data_offset } = self {
            let data: &[u8] = field_data
                .get(*data_offset as usize..)
                .expect("bad data offset"); // TODO: check at parsing

            let (_, value) =
                le_i64::<_, (_, ErrorKind)>(data).expect("size should have been checked");
            Some(value)
        } else {
            None
        }
    }
    pub fn get_double(&self, field_data: &[u8]) -> Option<f64> {
        if let FieldValue::DWord64 { data_offset } = self {
            let data: &[u8] = field_data
                .get(*data_offset as usize..)
                .expect("bad data offset"); // TODO: check at parsing

            let (_, value) =
                le_f64::<_, (_, ErrorKind)>(data).expect("size should have been checked");
            Some(value)
        } else {
            None
        }
    }
    pub fn get_string<'a>(&self, field_data: &'a [u8]) -> Option<&'a str> {
        if let FieldValue::String { data_offset } = self {
            let data = field_data
                .get(*data_offset as usize..)
                .expect("bad data offset"); // TODO: check at parsing

            let (_data, str_data) =
                nom::multi::length_data(le_u32::<_, (_, ErrorKind)>)(data).ok()?;

            Some(std::str::from_utf8(str_data).ok()?)
        } else {
            None
        }
    }
    pub fn get_resref<'a>(&self, field_data: &'a [u8]) -> Option<std::borrow::Cow<'a, str>> {
        if let FieldValue::ResRef { data_offset } = self {
            let data = field_data
                .get(*data_offset as usize..)
                .expect("bad data offset"); // TODO: check at parsing

            let (_data, str_data) =
                nom::multi::length_data(le_u8::<_, (_, ErrorKind)>)(data).ok()?;

            let (str_data, _, replaced) = WINDOWS_1252.decode(str_data);
            if replaced {
                None
            } else {
                Some(str_data)
            }
        } else {
            None
        }
    }
    pub fn get_locstring<'a>(&self, field_data: &'a [u8]) -> Option<(u32, Vec<(i32, &'a str)>)> {
        if let FieldValue::LocString { data_offset } = self {
            let data = field_data
                .get(*data_offset as usize..)
                .expect("bad data offset"); // TODO: check at parsing
            let (data, _total_size) = le_u32::<_, (_, ErrorKind)>(data).ok()?;
            let (data, strref) = le_u32::<_, (_, ErrorKind)>(data).ok()?;

            let (_data, strings) =
                nom::multi::length_count(le_u32::<_, (_, ErrorKind)>, |data: &'a [u8]| {
                    let (data, id) = le_i32::<_, (_, ErrorKind)>(data)?;
                    let (data, str_data) =
                        nom::multi::length_data(le_u32::<_, (_, ErrorKind)>)(data)?;
                    Ok((
                        data,
                        (
                            id,
                            std::str::from_utf8(str_data).or(Err(nom::Err::Failure(
                                nom::error::ParseError::from_error_kind(data, ErrorKind::Fail),
                            )))?,
                        ),
                    ))
                })(data)
                .ok()?;

            Some((strref, strings))
        } else {
            None
        }
    }
    pub fn get_void<'a>(&self, field_data: &'a [u8]) -> Option<&'a [u8]> {
        if let FieldValue::Void { data_offset } = self {
            let data = field_data
                .get(*data_offset as usize..)
                .expect("bad data offset"); // TODO: check at parsing
            let (_data, void_data) =
                nom::multi::length_data(le_u32::<_, (_, ErrorKind)>)(data).ok()?;

            Some(void_data)
        } else {
            None
        }
    }
    pub fn get_struct<'a>(&self, structs: &'a [Struct]) -> Option<&'a Struct> {
        if let FieldValue::Struct { struct_index } = self {
            structs.get(*struct_index as usize)
        } else {
            None
        }
    }
    pub fn get_list_struct_indices<'a>(&self, list_indices: &'a [u32]) -> Option<&'a [u32]> {
        if let FieldValue::List { list_offset } = self {
            // assert_eq!(list_offset % 4, 0);
            let list_size = list_indices.get((list_offset / 4) as usize)?;
            let start = (list_offset / 4) as usize + 1;
            let end = start + *list_size as usize;
            Some(list_indices.get(start..end)?)
        } else {
            None
        }
    }
}

#[derive(Debug)]
pub struct Gff {
    pub header: Header,
    pub structs: Vec<Struct>,
    pub fields: Vec<Field>,
    pub labels: Vec<FixedSizeString<16>>,
    /// Fields raw data, containing complex types like big numbers, strings, void, ...
    pub field_data: Vec<u8>,
    /// Contains lists of Field indices for Structs
    pub field_indices: Vec<u32>,
    /// Contains lists of Struct indices for GffLists
    pub list_indices: Vec<u32>,
}

impl Gff {
    fn fill_at_offset<'a, T, F>(
        input: &'a [u8],
        parser: F,
        offset: u32,
        count: u32,
    ) -> NWNParseResult<'a, Vec<T>>
    where
        T: Default,
        F: Fn(&'a [u8]) -> NWNParseResult<'a, T>,
    {
        // Advance to offset
        let input =
            &input
                .get(offset as usize..)
                .ok_or(nom::Err::Error(NWNParseError::from_string(format!(
                    "block offset {} is out of bounds",
                    offset
                ))))?;

        // Fill
        let mut vec: Vec<T> = vec![];
        vec.resize_with(count as usize, Default::default);
        fill(parser, vec.as_mut_slice())(input)?;

        Ok((input, vec))
    }

    pub fn from_bytes(input: &[u8]) -> NWNParseResult<Self> {
        let full_input = input;
        let (input, header) = Header::from_bytes(input)?;

        if header.struct_count == 0 {
            return Err(nom::Err::Error(NWNParseError::from_string(
                "empty struct list".to_string(),
            )));
        }

        let (_, structs) = Self::fill_at_offset(
            full_input,
            Struct::from_bytes,
            header.struct_offset,
            header.struct_count,
        )?;

        let (_, fields) = Self::fill_at_offset(
            full_input,
            |input| Field::from_bytes(input, &header),
            header.field_offset,
            header.field_count,
        )?;

        let (_, labels) = Self::fill_at_offset(
            full_input,
            FixedSizeString::from_bytes,
            header.label_offset,
            header.label_count,
        )?;

        let (_, field_data) = Self::fill_at_offset(
            full_input,
            le_u8,
            header.field_data_offset,
            header.field_data_size,
        )?;

        let (_, field_indices) = Self::fill_at_offset(
            full_input,
            le_u32,
            header.field_indices_offset,
            header.field_indices_size / 4,
        )?;

        let (_, list_indices) = Self::fill_at_offset(
            full_input,
            le_u32,
            header.list_indices_offset,
            header.list_indices_size / 4,
        )?;

        Ok((
            input,
            Self {
                header,
                structs,
                fields,
                labels,
                field_data,
                field_indices,
                list_indices,
            },
        ))
    }

    pub fn get_label_str(&self, index: u32) -> Option<&str> {
        Some(self.labels.get(index as usize)?.as_str())
    }

    pub fn to_bytes(&self) -> Vec<u8> {
        let mut header = Header::new(self.header.file_type, self.header.file_version);
        let total_len = header.update(self);

        let mut ret = Vec::with_capacity(total_len);
        ret.extend(header.to_bytes());
        ret.extend(self.structs.iter().flat_map(Struct::to_bytes));
        ret.extend(self.fields.iter().flat_map(Field::to_bytes));
        ret.extend(self.labels.iter().flat_map(FixedSizeString::as_bytes));
        ret.extend(&self.field_data);
        ret.extend(self.field_indices.iter().flat_map(|v| v.to_le_bytes()));
        ret.extend(self.list_indices.iter().flat_map(|v| v.to_le_bytes()));

        ret
    }

    /// Dumps internal GFF data, for debugging purposes
    pub fn dump(&self) -> String {
        let mut ret = String::new();
        ret += "================ HEADER ================\n";
        ret += &format!("{}\n", self.header.dump());

        ret += "================ STRUCTS ================\n";
        for (i, struct_) in self.structs.iter().enumerate() {
            ret += &format!("{}: {:?}\n", i, struct_);
        }

        ret += "================ FIELDS ================\n";
        for (i, field) in self.fields.iter().enumerate() {
            ret += &format!("{}: {:?}\n", i, field);
        }

        ret += "================ LABELS ================\n";
        for (i, label) in self.labels.iter().enumerate() {
            ret += &format!("{}: {:?}\n", i, label);
        }

        // Field data
        let field_data_refs = self
            .fields
            .iter()
            .enumerate()
            .filter_map(|(field_index, field)| match field.value {
                FieldValue::DWord64 { data_offset } => Some((data_offset, field_index)),
                FieldValue::Int64 { data_offset } => Some((data_offset, field_index)),
                FieldValue::Double { data_offset } => Some((data_offset, field_index)),
                FieldValue::String { data_offset } => Some((data_offset, field_index)),
                FieldValue::ResRef { data_offset } => Some((data_offset, field_index)),
                FieldValue::LocString { data_offset } => Some((data_offset, field_index)),
                FieldValue::Void { data_offset } => Some((data_offset, field_index)),
                _ => None,
            })
            .collect::<HashMap<u32, usize>>();

        ret += "================ FIELD_DATA ================";
        for (i, byte) in self.field_data.iter().enumerate() {
            if let Some(field_index) = field_data_refs.get(&(i as u32)) {
                ret += &format!("\n{} [ref by field {}]: {:02x}", i, field_index, byte);
            } else {
                ret += &format!(" {:02x}", byte);
            }
        }
        ret += "\n";

        // Field indices
        let field_indices_refs = self
            .structs
            .iter()
            .enumerate()
            .filter_map(|(struct_index, struct_)| {
                if struct_.field_count > 1 {
                    Some((struct_.data_or_data_offset, struct_index))
                } else {
                    None
                }
            })
            .collect::<HashMap<u32, usize>>();

        ret += "================ FIELD_INDICES ================";
        for (i, s) in self.field_indices.iter().enumerate() {
            if let Some(struct_index) = field_indices_refs.get(&(i as u32)) {
                ret += &format!("\n{} [ref by struct {}]: {:4}", i, struct_index, s);
            } else {
                ret += &format!(" {:4}", s);
            }
        }
        ret += "\n";

        // Field indices
        let list_indices_refs = self
            .fields
            .iter()
            .enumerate()
            .filter_map(|(field_index, field)| match field.value {
                FieldValue::List { list_offset } => Some((list_offset / 4, field_index)),
                _ => None,
            })
            .collect::<HashMap<u32, usize>>();

        ret += "================ LIST_INDICES ================";
        for (i, s) in self.list_indices.iter().enumerate() {
            if let Some(field_index) = list_indices_refs.get(&(i as u32)) {
                ret += &format!("\n{} [ref by list field {}]: {:4}", i, field_index, s);
            } else {
                ret += &format!(" {:4}", s);
            }
        }

        ret
    }
}
