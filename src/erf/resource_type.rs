#[derive(Debug, PartialEq)]
pub enum ResourceType {
    /// Invalid resource type
    Invalid = 0xFFFF,
    ///
    Res = 0,
    /// Windows BMP file
    Bmp = 1,
    ///
    Mve = 2,
    /// TGA image format
    Tga = 3,
    /// WAV sound file
    Wav = 4,
    ///
    Wfx = 5,
    /// Bioware Packed Layered Texture, used for player character skins, allows for multiple color layers
    Plt = 6,
    /// Windows INI file format
    Ini = 7,
    /// MP3 or BMU audio file
    Bmu = 8,
    ///
    Mpg = 9,
    /// Text file
    Txt = 10,
    ///
    Plh = 2000,
    /// LaTeX? (guess)
    Tex = 2001,
    /// Aurora model
    Mdl = 2002,
    ///
    Thg = 2003,
    ///
    Fnt = 2005,
    /// LUA scripts? (guess)
    Lua = 2007,
    ///
    Slt = 2008,
    /// NWScript Source
    Nss = 2009,
    /// NWScript Compiled Script
    Ncs = 2010,
    /// Module archive (erf-like)
    Mod = 2011,
    /// BioWare Aurora Engine Area file. Contains information on what tiles are located in an area, as well as other static area properties that cannot change via scripting. For each .are file in a .mod, there must also be a corresponding .git and .gic file having the same ResRef.
    Are = 2012,
    /// BioWare Aurora Engine Tileset
    Set = 2013,
    /// Module Info File. See the IFO Format document.
    Ifo = 2014,
    /// Character/Creature
    Bic = 2015,
    /// Walkmesh
    Wok = 2016,
    /// 2-D Array (2DA)
    Twoda = 2017,
    /// Talk table
    Tlk = 2018,
    /// Extra Texture Info
    Txi = 2022,
    /// Game Instance File
    Git = 2023,
    ///
    Bti = 2024,
    /// Item Blueprint
    Uti = 2025,
    ///
    Btc = 2026,
    /// Creature Blueprint
    Utc = 2027,
    /// Conversation File
    Dlg = 2029,
    /// Tile/Blueprint Palette File
    Itp = 2030,
    ///
    Btt = 2031,
    /// Trigger Blueprint
    Utt = 2032,
    /// Compressed texture file
    Dds = 2033,
    ///
    Bts = 2034,
    /// Sound Blueprint
    Uts = 2035,
    /// Letter-combo probability info for name generation
    Ltr = 2036,
    /// Generic File Format. Used when undesirable to create a new file extension for a resource, but the resource is a GFF. (Examples of GFFs include itp, utc, uti, ifo, are, git)
    Gff = 2037,
    /// Faction File
    Fac = 2038,
    ///
    Bte = 2039,
    /// Encounter Blueprint
    Ute = 2040,
    ///
    Btd = 2041,
    /// Door Blueprint
    Utd = 2042,
    ///
    Btp = 2043,
    /// Placeable Object Blueprint
    Utp = 2044,
    /// Default Values file. Used by area properties dialog
    Dft = 2045,
    /// Game Instance Comments. Comments on instances are not used by the game, only the toolset, so they are stored in a gic instead of in the git with the other instance properties.
    Gic = 2046,
    /// Graphical User Interface layout used by game
    Gui = 2047,
    /// Cascading Style Sheet? (guess)
    Css = 2048,
    ///
    Ccs = 2049,
    ///
    Btm = 2050,
    /// Store/Merchant Blueprint
    Utm = 2051,
    /// Door walkmesh
    Dwk = 2052,
    /// Placeable Object walkmesh
    Pwk = 2053,
    ///
    Btg = 2054,
    ///
    Utg = 2055,
    /// Journal File
    Jrl = 2056,
    ///
    Sav = 2057,
    /// Waypoint Blueprint. See Waypoint GFF document.
    Utw = 2058,
    /// DirectX texture file
    Fourpc = 2059,
    /// Sound Set File. See Sound Set File Format document
    Ssf = 2060,
    /// HAK ERF resource container
    Hak = 2061,
    ///
    Nwm = 2062,
    /// BIK Video
    Bik = 2063,
    /// Script Debugger File
    Ndb = 2064,
    /// Plot Manager file/Plot Instance
    Ptm = 2065,
    /// Plot Wizard Blueprint
    Ptt = 2066,
    ///
    Bak = 2067,
    ///
    Osc = 3000,
    ///
    Usc = 3001,
    /// Area terrain info
    Trn = 3002,
    /// Tree blueprint
    Utr = 3003,
    ///
    Uen = 3004,
    /// Light blueprint
    Ult = 3005,
    /// Special Effect XML File (NWN2)
    Sef = 3006,
    /// Particle effect XML file (NWN2)
    Pfx = 3007,
    /// Campaign definition GFF file
    Cam = 3008,
    /// Lightning effect XML file (NWN2)
    Lfx = 3009,
    /// Beam effect XML file (NWN2)
    Bfx = 3010,
    /// Encounter blueprint
    Upe = 3011,
    ///
    Ros = 3012,
    ///
    Rst = 3013,
    /// Linear particle effect XML file (NWN2)
    Ifx = 3014,
    /// Prefab definition file
    Pfb = 3015,
    /// Compressed zip file
    Zip = 3016,
    ///
    Wmp = 3017,
    /// Billboard effect XML file (NWN2)
    Bbx = 3018,
    /// Trail effect XML file (NWN2)
    Tfx = 3019,
    /// Walkmesh 3D object
    Wlk = 3020,
    /// XML
    Xml = 3021,
    ///
    Scc = 3022,
    ///
    Ptx = 3033,
    /// Lightning effect XML file (NWN2)
    Ltx = 3034,
    /// Baked terrain data
    Trx = 3035,
    /// 3D MDB object
    Mdb = 4000,
    ///
    Mda = 4001,
    /// Lipsync files (NWN2)
    Spt = 4002,
    /// Skeleton file
    Gr2 = 4003,
    ///
    Fxa = 4004,
    ///
    Fxe = 4005,
    /// Jpeg image
    Jpg = 4007,
    /// Persistent World Content? (guess) / ERF resource archive
    Pwc = 4008,
    ///
    Ids = 9996,
    /// Encapsulated Resource Format
    Erf = 9997,
    /// NWN1 resource container
    Bif = 9998,
    ///
    Key = 9999,
}
impl ResourceType {
    #[coverage(off)]
    pub fn ext(&self) -> &'static str {
        match self {
            ResourceType::Invalid => "",
            ResourceType::Res => "res",
            ResourceType::Bmp => "bmp",
            ResourceType::Mve => "mve",
            ResourceType::Tga => "tga",
            ResourceType::Wav => "wav",
            ResourceType::Wfx => "wfx",
            ResourceType::Plt => "plt",
            ResourceType::Ini => "ini",
            ResourceType::Bmu => "bmu",
            ResourceType::Mpg => "mpg",
            ResourceType::Txt => "txt",
            ResourceType::Plh => "plh",
            ResourceType::Tex => "tex",
            ResourceType::Mdl => "mdl",
            ResourceType::Thg => "thg",
            ResourceType::Fnt => "fnt",
            ResourceType::Lua => "lua",
            ResourceType::Slt => "slt",
            ResourceType::Nss => "nss",
            ResourceType::Ncs => "ncs",
            ResourceType::Mod => "mod",
            ResourceType::Are => "are",
            ResourceType::Set => "set",
            ResourceType::Ifo => "ifo",
            ResourceType::Bic => "bic",
            ResourceType::Wok => "wok",
            ResourceType::Twoda => "2da",
            ResourceType::Tlk => "tlk",
            ResourceType::Txi => "txi",
            ResourceType::Git => "git",
            ResourceType::Bti => "bti",
            ResourceType::Uti => "uti",
            ResourceType::Btc => "btc",
            ResourceType::Utc => "utc",
            ResourceType::Dlg => "dlg",
            ResourceType::Itp => "itp",
            ResourceType::Btt => "btt",
            ResourceType::Utt => "utt",
            ResourceType::Dds => "dds",
            ResourceType::Bts => "bts",
            ResourceType::Uts => "uts",
            ResourceType::Ltr => "ltr",
            ResourceType::Gff => "gff",
            ResourceType::Fac => "fac",
            ResourceType::Bte => "bte",
            ResourceType::Ute => "ute",
            ResourceType::Btd => "btd",
            ResourceType::Utd => "utd",
            ResourceType::Btp => "btp",
            ResourceType::Utp => "utp",
            ResourceType::Dft => "dft",
            ResourceType::Gic => "gic",
            ResourceType::Gui => "gui",
            ResourceType::Css => "css",
            ResourceType::Ccs => "ccs",
            ResourceType::Btm => "btm",
            ResourceType::Utm => "utm",
            ResourceType::Dwk => "dwk",
            ResourceType::Pwk => "pwk",
            ResourceType::Btg => "btg",
            ResourceType::Utg => "utg",
            ResourceType::Jrl => "jrl",
            ResourceType::Sav => "sav",
            ResourceType::Utw => "utw",
            ResourceType::Fourpc => "4pc",
            ResourceType::Ssf => "ssf",
            ResourceType::Hak => "hak",
            ResourceType::Nwm => "nwm",
            ResourceType::Bik => "bik",
            ResourceType::Ndb => "ndb",
            ResourceType::Ptm => "ptm",
            ResourceType::Ptt => "ptt",
            ResourceType::Bak => "bak",
            ResourceType::Osc => "osc",
            ResourceType::Usc => "usc",
            ResourceType::Trn => "trn",
            ResourceType::Utr => "utr",
            ResourceType::Uen => "uen",
            ResourceType::Ult => "ult",
            ResourceType::Sef => "sef",
            ResourceType::Pfx => "pfx",
            ResourceType::Cam => "cam",
            ResourceType::Lfx => "lfx",
            ResourceType::Bfx => "bfx",
            ResourceType::Upe => "upe",
            ResourceType::Ros => "ros",
            ResourceType::Rst => "rst",
            ResourceType::Ifx => "ifx",
            ResourceType::Pfb => "pfb",
            ResourceType::Zip => "zip",
            ResourceType::Wmp => "wmp",
            ResourceType::Bbx => "bbx",
            ResourceType::Tfx => "tfx",
            ResourceType::Wlk => "wlk",
            ResourceType::Xml => "xml",
            ResourceType::Scc => "scc",
            ResourceType::Ptx => "ptx",
            ResourceType::Ltx => "ltx",
            ResourceType::Trx => "trx",
            ResourceType::Mdb => "mdb",
            ResourceType::Mda => "mda",
            ResourceType::Spt => "spt",
            ResourceType::Gr2 => "gr2",
            ResourceType::Fxa => "fxa",
            ResourceType::Fxe => "fxe",
            ResourceType::Jpg => "jpg",
            ResourceType::Pwc => "pwc",
            ResourceType::Ids => "ids",
            ResourceType::Erf => "erf",
            ResourceType::Bif => "bif",
            ResourceType::Key => "key",
        }
    }
}

impl From<&str> for ResourceType {
    #[coverage(off)]
    fn from(ext: &str) -> Self {
        match ext {
            "res" => ResourceType::Res,
            "bmp" => ResourceType::Bmp,
            "mve" => ResourceType::Mve,
            "tga" => ResourceType::Tga,
            "wav" => ResourceType::Wav,
            "wfx" => ResourceType::Wfx,
            "plt" => ResourceType::Plt,
            "ini" => ResourceType::Ini,
            "bmu" => ResourceType::Bmu,
            "mpg" => ResourceType::Mpg,
            "txt" => ResourceType::Txt,
            "plh" => ResourceType::Plh,
            "tex" => ResourceType::Tex,
            "mdl" => ResourceType::Mdl,
            "thg" => ResourceType::Thg,
            "fnt" => ResourceType::Fnt,
            "lua" => ResourceType::Lua,
            "slt" => ResourceType::Slt,
            "nss" => ResourceType::Nss,
            "ncs" => ResourceType::Ncs,
            "mod" => ResourceType::Mod,
            "are" => ResourceType::Are,
            "set" => ResourceType::Set,
            "ifo" => ResourceType::Ifo,
            "bic" => ResourceType::Bic,
            "wok" => ResourceType::Wok,
            "2da" => ResourceType::Twoda,
            "tlk" => ResourceType::Tlk,
            "txi" => ResourceType::Txi,
            "git" => ResourceType::Git,
            "bti" => ResourceType::Bti,
            "uti" => ResourceType::Uti,
            "btc" => ResourceType::Btc,
            "utc" => ResourceType::Utc,
            "dlg" => ResourceType::Dlg,
            "itp" => ResourceType::Itp,
            "btt" => ResourceType::Btt,
            "utt" => ResourceType::Utt,
            "dds" => ResourceType::Dds,
            "bts" => ResourceType::Bts,
            "uts" => ResourceType::Uts,
            "ltr" => ResourceType::Ltr,
            "gff" => ResourceType::Gff,
            "fac" => ResourceType::Fac,
            "bte" => ResourceType::Bte,
            "ute" => ResourceType::Ute,
            "btd" => ResourceType::Btd,
            "utd" => ResourceType::Utd,
            "btp" => ResourceType::Btp,
            "utp" => ResourceType::Utp,
            "dft" => ResourceType::Dft,
            "gic" => ResourceType::Gic,
            "gui" => ResourceType::Gui,
            "css" => ResourceType::Css,
            "ccs" => ResourceType::Ccs,
            "btm" => ResourceType::Btm,
            "utm" => ResourceType::Utm,
            "dwk" => ResourceType::Dwk,
            "pwk" => ResourceType::Pwk,
            "btg" => ResourceType::Btg,
            "utg" => ResourceType::Utg,
            "jrl" => ResourceType::Jrl,
            "sav" => ResourceType::Sav,
            "utw" => ResourceType::Utw,
            "4pc" => ResourceType::Fourpc,
            "ssf" => ResourceType::Ssf,
            "hak" => ResourceType::Hak,
            "nwm" => ResourceType::Nwm,
            "bik" => ResourceType::Bik,
            "ndb" => ResourceType::Ndb,
            "ptm" => ResourceType::Ptm,
            "ptt" => ResourceType::Ptt,
            "bak" => ResourceType::Bak,
            "osc" => ResourceType::Osc,
            "usc" => ResourceType::Usc,
            "trn" => ResourceType::Trn,
            "utr" => ResourceType::Utr,
            "uen" => ResourceType::Uen,
            "ult" => ResourceType::Ult,
            "sef" => ResourceType::Sef,
            "pfx" => ResourceType::Pfx,
            "cam" => ResourceType::Cam,
            "lfx" => ResourceType::Lfx,
            "bfx" => ResourceType::Bfx,
            "upe" => ResourceType::Upe,
            "ros" => ResourceType::Ros,
            "rst" => ResourceType::Rst,
            "ifx" => ResourceType::Ifx,
            "pfb" => ResourceType::Pfb,
            "zip" => ResourceType::Zip,
            "wmp" => ResourceType::Wmp,
            "bbx" => ResourceType::Bbx,
            "tfx" => ResourceType::Tfx,
            "wlk" => ResourceType::Wlk,
            "xml" => ResourceType::Xml,
            "scc" => ResourceType::Scc,
            "ptx" => ResourceType::Ptx,
            "ltx" => ResourceType::Ltx,
            "trx" => ResourceType::Trx,
            "mdb" => ResourceType::Mdb,
            "mda" => ResourceType::Mda,
            "spt" => ResourceType::Spt,
            "gr2" => ResourceType::Gr2,
            "fxa" => ResourceType::Fxa,
            "fxe" => ResourceType::Fxe,
            "jpg" => ResourceType::Jpg,
            "pwc" => ResourceType::Pwc,
            "ids" => ResourceType::Ids,
            "erf" => ResourceType::Erf,
            "bif" => ResourceType::Bif,
            "key" => ResourceType::Key,
            _ => ResourceType::Invalid,
        }
    }
}

impl From<u16> for ResourceType {
    #[coverage(off)]
    fn from(id: u16) -> Self {
        match id {
            0 => ResourceType::Res,
            1 => ResourceType::Bmp,
            2 => ResourceType::Mve,
            3 => ResourceType::Tga,
            4 => ResourceType::Wav,
            5 => ResourceType::Wfx,
            6 => ResourceType::Plt,
            7 => ResourceType::Ini,
            8 => ResourceType::Bmu,
            9 => ResourceType::Mpg,
            10 => ResourceType::Txt,
            2000 => ResourceType::Plh,
            2001 => ResourceType::Tex,
            2002 => ResourceType::Mdl,
            2003 => ResourceType::Thg,
            2005 => ResourceType::Fnt,
            2007 => ResourceType::Lua,
            2008 => ResourceType::Slt,
            2009 => ResourceType::Nss,
            2010 => ResourceType::Ncs,
            2011 => ResourceType::Mod,
            2012 => ResourceType::Are,
            2013 => ResourceType::Set,
            2014 => ResourceType::Ifo,
            2015 => ResourceType::Bic,
            2016 => ResourceType::Wok,
            2017 => ResourceType::Twoda,
            2018 => ResourceType::Tlk,
            2022 => ResourceType::Txi,
            2023 => ResourceType::Git,
            2024 => ResourceType::Bti,
            2025 => ResourceType::Uti,
            2026 => ResourceType::Btc,
            2027 => ResourceType::Utc,
            2029 => ResourceType::Dlg,
            2030 => ResourceType::Itp,
            2031 => ResourceType::Btt,
            2032 => ResourceType::Utt,
            2033 => ResourceType::Dds,
            2034 => ResourceType::Bts,
            2035 => ResourceType::Uts,
            2036 => ResourceType::Ltr,
            2037 => ResourceType::Gff,
            2038 => ResourceType::Fac,
            2039 => ResourceType::Bte,
            2040 => ResourceType::Ute,
            2041 => ResourceType::Btd,
            2042 => ResourceType::Utd,
            2043 => ResourceType::Btp,
            2044 => ResourceType::Utp,
            2045 => ResourceType::Dft,
            2046 => ResourceType::Gic,
            2047 => ResourceType::Gui,
            2048 => ResourceType::Css,
            2049 => ResourceType::Ccs,
            2050 => ResourceType::Btm,
            2051 => ResourceType::Utm,
            2052 => ResourceType::Dwk,
            2053 => ResourceType::Pwk,
            2054 => ResourceType::Btg,
            2055 => ResourceType::Utg,
            2056 => ResourceType::Jrl,
            2057 => ResourceType::Sav,
            2058 => ResourceType::Utw,
            2059 => ResourceType::Fourpc,
            2060 => ResourceType::Ssf,
            2061 => ResourceType::Hak,
            2062 => ResourceType::Nwm,
            2063 => ResourceType::Bik,
            2064 => ResourceType::Ndb,
            2065 => ResourceType::Ptm,
            2066 => ResourceType::Ptt,
            2067 => ResourceType::Bak,
            3000 => ResourceType::Osc,
            3001 => ResourceType::Usc,
            3002 => ResourceType::Trn,
            3003 => ResourceType::Utr,
            3004 => ResourceType::Uen,
            3005 => ResourceType::Ult,
            3006 => ResourceType::Sef,
            3007 => ResourceType::Pfx,
            3008 => ResourceType::Cam,
            3009 => ResourceType::Lfx,
            3010 => ResourceType::Bfx,
            3011 => ResourceType::Upe,
            3012 => ResourceType::Ros,
            3013 => ResourceType::Rst,
            3014 => ResourceType::Ifx,
            3015 => ResourceType::Pfb,
            3016 => ResourceType::Zip,
            3017 => ResourceType::Wmp,
            3018 => ResourceType::Bbx,
            3019 => ResourceType::Tfx,
            3020 => ResourceType::Wlk,
            3021 => ResourceType::Xml,
            3022 => ResourceType::Scc,
            3033 => ResourceType::Ptx,
            3034 => ResourceType::Ltx,
            3035 => ResourceType::Trx,
            4000 => ResourceType::Mdb,
            4001 => ResourceType::Mda,
            4002 => ResourceType::Spt,
            4003 => ResourceType::Gr2,
            4004 => ResourceType::Fxa,
            4005 => ResourceType::Fxe,
            4007 => ResourceType::Jpg,
            4008 => ResourceType::Pwc,
            9996 => ResourceType::Ids,
            9997 => ResourceType::Erf,
            9998 => ResourceType::Bif,
            9999 => ResourceType::Key,
            _ => ResourceType::Invalid,
        }
    }
}
