//! Parsing library for NWN1 and NWN2 data files
//!
//! See [`erf`], [`gff`], [`tlk`], [`twoda`], [`trn`] for parsing specific file formats.

#![doc(html_logo_url = "https://gitlab.com/CromFr/nwn-lib-rs/-/raw/main/assets/nwn-lib-rs.png")]
#![allow(incomplete_features)]
#![feature(generic_const_exprs, coverage_attribute, test)]

use git_version::git_version;

pub const NWN_LIB_RS_VERSION: &str =
    git_version!(args = ["--tags", "--dirty=-dirty"], fallback = "unknown");

pub mod parsing;

pub mod erf;
pub mod gff;
pub mod gui;
pub mod tlk;
pub mod trn;
pub mod twoda;
