#![allow(dead_code)]

use nom::multi::{count, fill};
use nom::number::complete::*;

use crate::parsing::*;

/// Terrain walkmesh data
#[derive(Debug)]
pub struct Aswm {
    /// ASWM version. NWN2Toolset generates version 0x6c, but some NWN2 official campaign files are between 0x69 and 0x6c.
    pub version: u32,
    /// ASWM name (probably useless)
    pub name: FixedSizeString<32>,
    pub vertices: Vec<Vertex>,
    pub edges: Vec<Edge>,
    pub triangles: Vec<Triangle>,
    /// Always 31 in TRX files, 15 in TRN files
    pub tiles_flags: u32,
    /// Width in meters of a terrain tile (most likely to be 10.0)
    pub tiles_width: f32,
    /// Number of tiles along Y axis
    /// TODO: double check height = Y
    pub tiles_grid_height: u32,
    /// Number of tiles along X axis
    /// TODO: double check width = X
    pub tiles_grid_width: u32,
    /// Width of the map borders in tiles (8 means that 8 tiles will be removed on each side)
    pub tiles_border_size: u32,
    /// Small walkmesh tiles, with pathing information
    pub tiles: Vec<Tile>,
    /// Walkmesh islands (groups of adjacent walkable triangles)
    pub islands: Vec<Island>,
    /// Pathing info between islands
    pub islands_path_nodes: Vec<IslandPathNode>,
}

#[derive(Debug)]
pub struct Vertex {
    /// 3d coordinates of the vertex
    pub pos: [f32; 3],
}

#[derive(Debug)]
pub struct Edge {
    pub vertices: [u32; 2],
    pub triangles: [u32; 2],
}
#[derive(Debug)]
pub struct Triangle {
    pub vertices: [u32; 3],
    pub linked_edges: [u32; 3],
    pub linked_triangles: [u32; 3],
    pub center: [f32; 2],
    pub normal: [f32; 3],
    pub dot_product: f32,
    pub island: u16,
    pub flags: u16,
}
#[derive(Debug)]
pub enum TriangleFlags {
    /// if the triangle can be walked on. Note the triangle needs path tables to be really walkable
    Walkable = 0x01,
    /// vertices are wound clockwise and not ccw
    Clockwise = 0x04,
    /// Floor type (for sound effects)
    Dirt = 0x08,
    /// ditto
    Grass = 0x10,
    /// ditto
    Stone = 0x20,
    /// ditto
    Wood = 0x40,
    /// ditto
    Carpet = 0x80,
    /// ditto
    Metal = 0x100,
    /// ditto
    Swamp = 0x200,
    /// ditto
    Mud = 0x400,
    /// ditto
    Leaves = 0x800,
    /// ditto
    Water = 0x1000,
    /// ditto
    Puddles = 0x2000,
}
#[derive(Debug)]
pub struct Tile {
    /// Last time I checked it was complete garbage
    pub name: FixedSizeString<32>,
    /// 1 if the tile stores vertices / edges. Usually 0
    /// Always 0 ?
    pub size_x: f32,
    /// Always 0 ?
    pub size_y: f32,
    /// This value will be added to each triangle index in the PathTable
    pub triangles_offset: u32,

    pub owns_data: u8,
    pub vertices: Vec<Vertex>,
    pub edges: Vec<Edge>,
    pub triangles: Vec<Triangle>,

    pub path_table: PathTable,
}

#[derive(Debug)]
pub struct PathTable {
    pub header_flags: u32,
    pub rle_table_size: u32,
    pub local_to_node: Vec<u8>,
    pub node_to_local: Vec<u32>,
    pub nodes: Vec<u8>,
    pub flags: u32,
}
#[derive(Debug)]
pub struct Island {
    pub index: u32,
    pub tile: u32,
    pub center: Vertex,
    pub triangles_count: u32,
    pub adjacent_islands: Vec<u32>,
    pub adjacent_islands_dist: Vec<f32>,
    pub exit_triangles: Vec<u32>,
}

#[derive(Debug)]
pub struct IslandPathNode {
    pub next: u16,
    pub weight: f32,
}

impl Aswm {
    /// Parses ASWM data
    pub fn from_bytes(input: &[u8]) -> NWNParseResult<Self> {
        let (input, compression) = FixedSizeString::<4>::from_bytes(input)?;

        fn parse_uncomp(input: &[u8]) -> NWNParseResult<Aswm> {
            // Header
            let (input, version) = le_u32(input)?;
            let (input, name) = FixedSizeString::from_bytes_lossy(input)?;

            let (input, owns_data) = le_u8(input)?;
            if owns_data != 1 {
                return Err(nom::Err::Error(NWNParseError::from_str(
                    "aswm packets without owned data are not supported",
                )));
            }
            let (input, vertices_count) = le_u32(input)?;
            let (input, edges_count) = le_u32(input)?;
            let (input, triangles_count) = le_u32(input)?;
            let (input, _unknown) = le_u32(input)?;

            let (input, vertices) = count(Vertex::from_bytes, vertices_count as usize)(input)
                .map_err(|e| e.map(|e| e.add_context_msg("while parsing vertices".to_string())))?;
            let (input, edges) = count(
                |input| Edge::from_bytes(input, version),
                edges_count as usize,
            )(input)
            .map_err(|e| e.map(|e| e.add_context_msg("while parsing edges".to_string())))?;
            let (input, triangles) = count(
                |input| Triangle::from_bytes(input, version),
                triangles_count as usize,
            )(input)
            .map_err(|e| e.map(|e| e.add_context_msg("while parsing triangles".to_string())))?;

            let (input, tiles_flags) = le_u32(input)?;
            let (input, tiles_width) = le_f32(input)?;
            let (input, tiles_grid_height) = le_u32(input)?;
            let (input, tiles_grid_width) = le_u32(input)?;

            let (input, tiles) = count(
                |input| Tile::from_bytes(input, version),
                tiles_grid_height as usize * tiles_grid_width as usize,
            )(input)
            .map_err(|e| e.map(|e| e.add_context_msg("while parsing tiles".to_string())))?;

            let (input, tiles_border_size) = le_u32(input)?;

            let (input, islands_count) = le_u32(input)?;
            let (input, islands) = count(Island::from_bytes, islands_count as usize)(input)
                .map_err(|e| e.map(|e| e.add_context_msg("while parsing islands".to_string())))?;

            let (input, islands_path_nodes) = count(
                |input| IslandPathNode::from_bytes(input),
                islands_count as usize * islands_count as usize,
            )(input)
            .map_err(|e| {
                e.map(|e| e.add_context_msg("while parsing island path nodes".to_string()))
            })?;

            Ok((
                input,
                Aswm {
                    version,
                    name,
                    vertices,
                    edges,
                    triangles,
                    tiles_flags,
                    tiles_width,
                    tiles_grid_height,
                    tiles_grid_width,
                    tiles_border_size,
                    tiles,
                    islands,
                    islands_path_nodes,
                },
            ))
        }

        if compression == "COMP" {
            let (input, comp_length) = le_u32(input)?;
            let (input, uncomp_length) = le_u32(input)?;

            // let (input, comp_data) = nom::complete::take(comp_length)(input)?;
            let comp_data = &input[..comp_length as usize]; // TODO: can panic
            let input = &input[comp_length as usize..];

            // Uncompress
            use std::io::Read;
            let mut z = flate2::read::ZlibDecoder::new(comp_data);
            let mut uncompressed = Vec::with_capacity(uncomp_length as usize);
            z.read_to_end(&mut uncompressed).map_err(|e| {
                nom::Err::Error(format!("cannot uncompress aswm data: {}", e).into())
            })?;

            let (_, res) =
                parse_uncomp(&uncompressed).map_err(|e| e.map(|e| e.to_string().into()))?; // TODO: replace into with to_owned
            Ok((input, res))
        } else {
            parse_uncomp(input)
        }
    }

    /// Serializes ASWM data
    pub fn to_bytes(&self) -> Vec<u8> {
        let mut output = vec![];
        output.extend(FixedSizeString::<4>::from_str("COMP").unwrap().as_bytes());

        // Compress data
        let uncomp_data = self.to_bytes_uncomp();
        let uncomp_len = uncomp_data.len();
        let mut e = flate2::write::ZlibEncoder::new(Vec::new(), flate2::Compression::default());
        use std::io::Write;
        e.write_all(&uncomp_data)
            .expect("failed to ingest data to compress");
        let comp_data = e.finish().expect("failed to compress data");

        // Serialize
        output.extend((comp_data.len() as u32).to_le_bytes());
        output.extend((uncomp_len as u32).to_le_bytes());
        output.extend(&comp_data);

        output
    }
    fn to_bytes_uncomp(&self) -> Vec<u8> {
        let mut output = vec![];

        output.extend(0x6cu32.to_le_bytes());
        output.extend(self.name.as_bytes());
        output.extend(1u8.to_le_bytes());

        output.extend((self.vertices.len() as u32).to_le_bytes());
        output.extend((self.edges.len() as u32).to_le_bytes());
        output.extend((self.triangles.len() as u32).to_le_bytes());
        output.extend(0u32.to_le_bytes()); // _unknown

        output.extend(self.vertices.iter().flat_map(|vtx| vtx.to_bytes()));
        output.extend(self.edges.iter().flat_map(|edg| edg.to_bytes()));
        output.extend(self.triangles.iter().flat_map(|tri| tri.to_bytes()));

        output.extend(self.tiles_flags.to_le_bytes());
        output.extend(self.tiles_width.to_le_bytes());
        output.extend(self.tiles_grid_height.to_le_bytes());
        output.extend(self.tiles_grid_width.to_le_bytes());

        output.extend(self.tiles.iter().flat_map(|tile| tile.to_bytes()));

        output.extend(self.tiles_border_size.to_le_bytes());

        output.extend((self.islands.len() as u32).to_le_bytes());
        output.extend(self.islands.iter().flat_map(|isl| isl.to_bytes()));
        output.extend(
            self.islands_path_nodes
                .iter()
                .flat_map(|ipn| ipn.to_bytes()),
        );

        output
    }
}

impl Vertex {
    fn from_bytes(input: &[u8]) -> NWNParseResult<Self> {
        let mut pos = [0f32; 3];
        let (input, ()) = fill(le_f32, &mut pos)(input)?;
        Ok((input, Self { pos }))
    }

    fn to_bytes(&self) -> Vec<u8> {
        let mut output = vec![];
        output.extend(self.pos.iter().flat_map(|v| v.to_le_bytes()));
        output
    }
}

impl Edge {
    fn from_bytes(input: &[u8], aswm_version: u32) -> NWNParseResult<Self> {
        match aswm_version {
            0x69 => {
                let mut vertices = [0u32; 2];
                let (input, ()) = fill(le_u32, &mut vertices)(input)?;
                let mut triangles = [0u32; 2];
                let (input, ()) = fill(le_u32, &mut triangles)(input)?;
                let mut _reserved = [0u32; 9];
                let (input, ()) = fill(le_u32, &mut _reserved)(input)?;
                Ok((
                    input,
                    Self {
                        vertices,
                        triangles,
                    },
                ))
            }
            _ => {
                let mut vertices = [0u32; 2];
                let (input, ()) = fill(le_u32, &mut vertices)(input)?;
                let mut triangles = [0u32; 2];
                let (input, ()) = fill(le_u32, &mut triangles)(input)?;
                Ok((
                    input,
                    Self {
                        vertices,
                        triangles,
                    },
                ))
            }
        }
    }

    fn to_bytes(&self) -> Vec<u8> {
        let mut output = vec![];
        output.extend(self.vertices.iter().flat_map(|v| v.to_le_bytes()));
        output.extend(self.triangles.iter().flat_map(|v| v.to_le_bytes()));
        output
    }
}

impl Triangle {
    fn from_bytes(input: &[u8], aswm_version: u32) -> NWNParseResult<Self> {
        match aswm_version {
            0x69 => {
                let (input, _reserved) = le_u32(input)?;
                let (input, flags) = le_u32(input)?;
                let mut vertices = [0u32; 3];
                let (input, ()) = fill(le_u32, &mut vertices)(input)?;
                let mut linked_edges = [0u32; 3];
                let (input, ()) = fill(le_u32, &mut linked_edges)(input)?;
                let mut linked_triangles = [0u32; 3];
                let (input, ()) = fill(le_u32, &mut linked_triangles)(input)?;
                let mut _reserved = [0u32; 3];
                let (input, ()) = fill(le_u32, &mut _reserved)(input)?;
                let mut center = [0f32; 2];
                let (input, ()) = fill(le_f32, &mut center)(input)?;
                let (input, _reserved) = le_u32(input)?;
                let mut normal = [0f32; 3];
                let (input, ()) = fill(le_f32, &mut normal)(input)?;
                let (input, dot_product) = le_f32(input)?;
                let (input, _reserved) = le_u16(input)?;
                let (input, island) = le_u16(input)?;

                Ok((
                    input,
                    Self {
                        vertices,
                        linked_edges,
                        linked_triangles,
                        center,
                        normal,
                        dot_product,
                        island,
                        flags: flags as u16,
                    },
                ))
            }
            0x6a => {
                let (input, _reserved) = le_u32(input)?;
                let mut vertices = [0u32; 3];
                let (input, ()) = fill(le_u32, &mut vertices)(input)?;
                let mut linked_edges = [0u32; 3];
                let (input, ()) = fill(le_u32, &mut linked_edges)(input)?;
                let mut linked_triangles = [0u32; 3];
                let (input, ()) = fill(le_u32, &mut linked_triangles)(input)?;
                let mut center = [0f32; 2];
                let (input, ()) = fill(le_f32, &mut center)(input)?;
                let mut normal = [0f32; 3];
                let (input, ()) = fill(le_f32, &mut normal)(input)?;
                let (input, dot_product) = le_f32(input)?;
                let (input, island) = le_u16(input)?;
                let (input, flags) = le_u16(input)?;

                Ok((
                    input,
                    Self {
                        vertices,
                        linked_edges,
                        linked_triangles,
                        center,
                        normal,
                        dot_product,
                        island,
                        flags,
                    },
                ))
            }
            0x6c => {
                let mut vertices = [0u32; 3];
                let (input, ()) = fill(le_u32, &mut vertices)(input)?;
                let mut linked_edges = [0u32; 3];
                let (input, ()) = fill(le_u32, &mut linked_edges)(input)?;
                let mut linked_triangles = [0u32; 3];
                let (input, ()) = fill(le_u32, &mut linked_triangles)(input)?;
                let mut center = [0f32; 2];
                let (input, ()) = fill(le_f32, &mut center)(input)?;
                let mut normal = [0f32; 3];
                let (input, ()) = fill(le_f32, &mut normal)(input)?;
                let (input, dot_product) = le_f32(input)?;
                let (input, island) = le_u16(input)?;
                let (input, flags) = le_u16(input)?;

                Ok((
                    input,
                    Self {
                        vertices,
                        linked_edges,
                        linked_triangles,
                        center,
                        normal,
                        dot_product,
                        island,
                        flags,
                    },
                ))
            }
            version => Err(nom::Err::Error(
                format!("unknown triangle version 0x{:x}", version).into(),
            )),
        }
    }
    fn to_bytes(&self) -> Vec<u8> {
        let mut output = vec![];
        output.extend(self.vertices.iter().flat_map(|v| v.to_le_bytes()));
        output.extend(self.linked_edges.iter().flat_map(|v| v.to_le_bytes()));
        output.extend(self.linked_triangles.iter().flat_map(|v| v.to_le_bytes()));
        output.extend(self.center.iter().flat_map(|v| v.to_le_bytes()));
        output.extend(self.normal.iter().flat_map(|v| v.to_le_bytes()));
        output.extend(self.dot_product.to_le_bytes());
        output.extend(self.island.to_le_bytes());
        output.extend(self.flags.to_le_bytes());
        output
    }
}

impl Tile {
    fn from_bytes(input: &[u8], aswm_version: u32) -> NWNParseResult<Self> {
        let (input, name) = FixedSizeString::from_bytes_lossy(input)?;
        let (input, owns_data) = le_u8(input)?;
        let (input, vertices_count) = le_u32(input)?;
        let (input, edges_count) = le_u32(input)?;
        let (input, triangles_count) = le_u32(input)?;
        let (input, size_x) = le_f32(input)?;
        let (input, size_y) = le_f32(input)?;
        let (input, triangles_offset) = le_u32(input)?;

        let mut input = input;
        let mut vertices = vec![];
        let mut edges = vec![];
        let mut triangles = vec![];
        if owns_data > 0 {
            (input, vertices) = count(Vertex::from_bytes, vertices_count as usize)(input)?;
            (input, edges) = count(
                |input| Edge::from_bytes(input, aswm_version),
                edges_count as usize,
            )(input)?;
            (input, triangles) = count(
                |input| Triangle::from_bytes(input, aswm_version),
                triangles_count as usize,
            )(input)?;
        }

        let (input, path_table) = PathTable::from_bytes(input, aswm_version)?;

        Ok((
            input,
            Self {
                name,
                size_x,
                size_y,
                triangles_offset,
                owns_data,
                vertices,
                edges,
                triangles,
                path_table,
            },
        ))
    }
    fn to_bytes(&self) -> Vec<u8> {
        let mut output = vec![];
        output.extend(self.name.as_bytes());
        output.extend(self.owns_data.to_le_bytes());
        output.extend((self.vertices.len() as u32).to_le_bytes());
        output.extend((self.edges.len() as u32).to_le_bytes());
        output.extend((self.triangles.len() as u32).to_le_bytes());
        output.extend(self.size_x.to_le_bytes());
        output.extend(self.size_y.to_le_bytes());
        output.extend(self.triangles_offset.to_le_bytes());

        output.extend(self.vertices.iter().flat_map(|vtx| vtx.to_bytes()));
        output.extend(self.edges.iter().flat_map(|edg| edg.to_bytes()));
        output.extend(self.triangles.iter().flat_map(|tri| tri.to_bytes()));

        output.extend(self.path_table.to_bytes());
        output
    }
}

impl Island {
    fn from_bytes(input: &[u8]) -> NWNParseResult<Self> {
        let (input, index) = le_u32(input)?;
        let (input, tile) = le_u32(input)?;
        let (input, center) = Vertex::from_bytes(input)?;
        let (input, triangles_count) = le_u32(input)?;

        let (input, adjacent_islands_count) = le_u32(input)?;
        let (input, adjacent_islands) = count(le_u32, adjacent_islands_count as usize)(input)?;

        let (input, adjacent_islands_dist_count) = le_u32(input)?;
        let (input, adjacent_islands_dist) =
            count(le_f32, adjacent_islands_dist_count as usize)(input)?;

        let (input, exit_triangles_count) = le_u32(input)?;
        let (input, exit_triangles) = count(le_u32, exit_triangles_count as usize)(input)?;

        Ok((
            input,
            Self {
                index,
                tile,
                center,
                triangles_count,
                adjacent_islands,
                adjacent_islands_dist,
                exit_triangles,
            },
        ))
    }
    fn to_bytes(&self) -> Vec<u8> {
        let mut output = vec![];
        output.extend(self.index.to_le_bytes());
        output.extend(self.tile.to_le_bytes());
        output.extend(self.center.to_bytes());
        output.extend(self.triangles_count.to_le_bytes());

        output.extend((self.adjacent_islands.len() as u32).to_le_bytes());
        output.extend(self.adjacent_islands.iter().flat_map(|ai| ai.to_le_bytes()));

        output.extend((self.adjacent_islands_dist.len() as u32).to_le_bytes());
        output.extend(
            self.adjacent_islands_dist
                .iter()
                .flat_map(|aid| aid.to_le_bytes()),
        );

        output.extend((self.exit_triangles.len() as u32).to_le_bytes());
        output.extend(self.exit_triangles.iter().flat_map(|et| et.to_le_bytes()));
        output
    }
}

impl IslandPathNode {
    fn from_bytes(input: &[u8]) -> NWNParseResult<Self> {
        let (input, next) = le_u16(input)?;
        let (input, _padding) = le_u16(input)?;
        let (input, weight) = le_f32(input)?;
        Ok((input, Self { next, weight }))
    }
    fn to_bytes(&self) -> Vec<u8> {
        let mut output = vec![];
        output.extend(self.next.to_le_bytes());
        output.extend(0u16.to_le_bytes());
        output.extend(self.weight.to_le_bytes());
        output
    }
}

impl PathTable {
    fn from_bytes(input: &[u8], aswm_version: u32) -> NWNParseResult<Self> {
        let (input, (header_flags, local_to_node_count, node_to_local_count, rle_table_size)) =
            match aswm_version {
                0x69 => {
                    let (input, flags) = le_u32(input)?;
                    let (input, local_to_node_count) = le_u8(input)?;
                    let (input, node_to_local_count) = le_u8(input)?;
                    let (input, rle_table_size) = le_u32(input)?;
                    (
                        input,
                        (
                            flags,
                            local_to_node_count as u32,
                            node_to_local_count,
                            rle_table_size,
                        ),
                    )
                }
                _ => {
                    let (input, flags) = le_u32(input)?;
                    let (input, local_to_node_count) = le_u32(input)?;
                    let (input, node_to_local_count) = le_u8(input)?;
                    let (input, rle_table_size) = le_u32(input)?;
                    (
                        input,
                        (
                            flags,
                            local_to_node_count,
                            node_to_local_count,
                            rle_table_size,
                        ),
                    )
                }
            };

        if (header_flags & 0b11) > 0 {
            return Err(nom::Err::Error(
                "compressed path tables are not supported".into(),
            ));
        }

        let (input, local_to_node) = count(le_u8, local_to_node_count as usize)(input)?;

        let (input, node_to_local) = match aswm_version {
            0x69 | 0x6a => (input, vec![]), // No node_to_local data
            0x6b => count(
                |input| {
                    let (input, ntl) = le_u8(input)?;
                    Ok((input, ntl as u32))
                },
                node_to_local_count as usize,
            )(input)?,
            0x6c => count(le_u32, node_to_local_count as usize)(input)?,
            version => {
                return Err(nom::Err::Error(
                    format!("unknown path table version 0x{:x}", version).into(),
                ));
            }
        };

        let (input, nodes) = count(
            le_u8,
            node_to_local_count as usize * node_to_local_count as usize,
        )(input)?;
        let (input, flags) = le_u32(input)?;

        Ok((
            input,
            Self {
                header_flags,
                rle_table_size,
                local_to_node,
                node_to_local,
                nodes,
                flags,
            },
        ))
    }

    fn to_bytes(&self) -> Vec<u8> {
        let mut output = vec![];
        output.extend(0u32.to_le_bytes());
        output.extend((self.local_to_node.len() as u32).to_le_bytes());
        output.extend((self.node_to_local.len() as u8).to_le_bytes());
        output.extend(self.rle_table_size.to_le_bytes());

        output.extend(&self.local_to_node);
        output.extend(self.node_to_local.iter().flat_map(|ntl| ntl.to_le_bytes()));
        output.extend(&self.nodes);

        output.extend(self.flags.to_le_bytes());
        output
    }
}
