//! NWN2 TRN file format (.trn, .trx)
//!
//! Example
//! ```rust
//! #![feature(generic_const_exprs)]
//! #![allow(incomplete_features)]
//! use nwn_lib_rs::trn::{Trn, Packet, PacketData};
//!
//! // Parse TRN file
//! let trn_data = std::fs::read("unittest/TestImportExportTRN.trx").unwrap();
//! let (_, trn) = Trn::from_bytes(&trn_data).expect("Failed to parse TRN data");
//!
//! // Go through each TRN packet
//! for packet in trn.packets.iter() {
//!     let PacketData::Parsed(packet) = packet else {
//!         panic!()
//!     };
//!     match packet.as_ref() {
//!         Packet::Trwh(_) => {
//!             println!("Terrain width/height packet");
//!         }
//!         Packet::Trrn(_) => {
//!             println!("Terrain geometry packet");
//!         }
//!         Packet::Watr(_) => {
//!             println!("Water geometry packet");
//!         }
//!         Packet::Aswm(_) => {
//!             println!("Walkmesh packet");
//!         }
//!     }
//! }
//!
//! // Serialize TRN data
//! let trn_data = trn.to_bytes();
//! ```
//!
#![allow(dead_code, unused_imports)]

pub mod aswm;
pub mod trrn;
pub mod trwh;
pub mod watr;
pub use aswm::Aswm;
pub use trrn::Trrn;
pub use trwh::Trwh;
pub use watr::Watr;

use crate::parsing::*;
use nom::number::complete::*;
use nom::{
    bytes, character,
    error::{ContextError, ErrorKind, ParseError, VerboseError},
    multi::{count, fill},
    Err, Finish, IResult, Needed, Offset,
};

/// TRN file
#[derive(Debug)]
pub struct Trn {
    /// TRN file type (TRN, TRX)
    pub file_type: FixedSizeString<4>,
    /// TRN file version
    pub version_major: u16,
    /// TRN file version
    pub version_minor: u16,
    /// TRN packets
    pub packets: Vec<PacketData>,
}

/// TRN packet type
#[derive(Debug, PartialEq, Copy, Clone)]
pub enum PacketType {
    Trwh,
    Trrn,
    Watr,
    Aswm,
}
impl PacketType {
    /// TRN packet type string to enum
    pub fn from(type_str: &str) -> Option<Self> {
        match type_str {
            "TRWH" => Some(PacketType::Trwh),
            "TRRN" => Some(PacketType::Trrn),
            "WATR" => Some(PacketType::Watr),
            "ASWM" => Some(PacketType::Aswm),
            _ => None,
        }
    }
    /// enum to TRN packet type string
    pub fn to_str(&self) -> &'static str {
        match self {
            PacketType::Trwh => "TRWH",
            PacketType::Trrn => "TRRN",
            PacketType::Watr => "WATR",
            PacketType::Aswm => "ASWM",
        }
    }
}

/// Parsed TRN packet
#[derive(Debug)]
pub enum Packet {
    Trwh(Trwh),
    Trrn(Trrn),
    Watr(Watr),
    Aswm(Aswm),
}
impl Packet {
    /// Returns the associated `PacketType`
    pub fn get_type(&self) -> PacketType {
        match self {
            Packet::Trwh(_) => PacketType::Trwh,
            Packet::Trrn(_) => PacketType::Trrn,
            Packet::Watr(_) => PacketType::Watr,
            Packet::Aswm(_) => PacketType::Aswm,
        }
    }
    /// Parse TRN packet of a given type (without the type and length header)
    pub fn from_bytes(packet_type: PacketType, input: &[u8]) -> NWNParseResult<Self> {
        match packet_type {
            PacketType::Trwh => {
                let (input, packet) = Trwh::from_bytes(input)?;
                Ok((input, Packet::Trwh(packet)))
            }
            PacketType::Trrn => {
                let (input, packet) = Trrn::from_bytes(input)?;
                Ok((input, Packet::Trrn(packet)))
            }
            PacketType::Watr => {
                let (input, packet) = Watr::from_bytes(input)?;
                Ok((input, Packet::Watr(packet)))
            }
            PacketType::Aswm => {
                let (input, packet) = Aswm::from_bytes(input)?;
                Ok((input, Packet::Aswm(packet)))
            }
        }
    }
    /// Serialize a TRN packet (without the type and length header)
    pub fn to_bytes(&self) -> Vec<u8> {
        match self {
            Packet::Trwh(pack) => pack.to_bytes(),
            Packet::Trrn(pack) => pack.to_bytes(),
            Packet::Watr(pack) => pack.to_bytes(),
            Packet::Aswm(pack) => pack.to_bytes(),
        }
    }
}

/// TRN packet data, that may or may not be loaded in memory or parsed
#[derive(Debug)]
pub enum PacketData {
    /// Packet data has been fully loaded and parsed
    Parsed(Box<Packet>),
    /// Packet data has been loaded in memory, but not parsed
    Buffer {
        /// Packet type
        packet_type: PacketType,
        /// Packet data block, without the type + length header
        data: Vec<u8>,
    },
    /// Unloaded packet. The packet has not been parsed nor loaded in memory.
    Offset {
        /// Expected packet type. Packet data block may indicate a different type.
        packet_type: PacketType,
        /// offset to the packet data block
        offset: u32,
    },
}
impl PacketData {
    fn parse_packet_data_block(input: &[u8]) -> NWNParseResult<(PacketType, &[u8])> {
        let (input, packet_type_str) = FixedSizeString::<4>::from_bytes(input)?;
        let (input, packet_len) = le_u32(input)?;

        let packet_type = PacketType::from(packet_type_str.as_str()).ok_or(nom::Err::Error(
            NWNParseError::from_string(format!(
                "invalid packet type {:?} in packet data",
                packet_type_str
            )),
        ))?;
        let input =
            input
                .get(..packet_len as usize)
                .ok_or(nom::Err::Error(NWNParseError::from_string(format!(
                    "packet data length {} is larger than the available data",
                    packet_len
                ))))?;

        Ok((&[], (packet_type, input)))
    }

    /// Returns the packet type, regardless of whether it has been parsed or not
    pub fn get_type(&self) -> PacketType {
        match self {
            Self::Parsed(packet) => packet.get_type(),
            Self::Buffer {
                packet_type,
                data: _,
            } => *packet_type,
            Self::Offset {
                packet_type,
                offset: _,
            } => *packet_type,
        }
    }

    /// Parse a packet data block, including its type and size header. Returns a PacketData::Parsed
    pub fn from_bytes_parse(
        input: &[u8],
        expected_packet_type: Option<PacketType>,
    ) -> NWNParseResult<Self> {
        let (packet_type, packet_data) = Self::parse_packet_data_block(input)?.1;

        if let Some(expected_packet_type) = expected_packet_type {
            if packet_type != expected_packet_type {
                return Err(nom::Err::Error(NWNParseError::from_string(format!(
                    "type does not match between packet index and packet data: {:?} in index vs \
                     {:?} in data",
                    packet_type, expected_packet_type
                ))));
            }
        }
        let (input, packet) = Packet::from_bytes(packet_type, packet_data)?;
        Ok((input, Self::Parsed(Box::new(packet))))
    }
    /// Store packet data without parsing its content. Returns a PacketData::Buffer
    pub fn from_bytes_clone(
        input: &[u8],
        expected_packet_type: Option<PacketType>,
    ) -> NWNParseResult<Self> {
        let (packet_type, packet_data) = Self::parse_packet_data_block(input)?.1;

        if let Some(expected_packet_type) = expected_packet_type {
            if packet_type != expected_packet_type {
                return Err(nom::Err::Error(NWNParseError::from_string(format!(
                    "type does not match between packet index and packet data: {:?} in index vs \
                     {:?} in data",
                    packet_type, expected_packet_type
                ))));
            }
        }

        Ok((
            &[],
            Self::Buffer {
                packet_type,
                data: packet_data.to_owned(),
            },
        ))
    }

    /// Returns the serialized data block for the packet, with the type and size header
    pub fn to_bytes(&self) -> Vec<u8> {
        let mut packet_data_block = vec![];
        match self {
            Self::Parsed(packet) => {
                let data = packet.to_bytes();
                packet_data_block.extend(packet.get_type().to_str().as_bytes());
                packet_data_block.extend((data.len() as u32).to_le_bytes());
                packet_data_block.extend(&data);
            }
            Self::Buffer { packet_type, data } => {
                packet_data_block.extend(packet_type.to_str().as_bytes());
                packet_data_block.extend((data.len() as u32).to_le_bytes());
                packet_data_block.extend(data);
            }
            Self::Offset {
                packet_type: _,
                offset: _,
            } => {
                panic!("Packet data has not been loaded / parsed. see load_offset or parse_offset")
            }
        }
        packet_data_block
    }

    /// Converts self from a PacketData::Offset into a PacketData::Buffer
    pub fn load_offset(&mut self, buffer: &[u8], buffer_offset: u32) -> NWNParseResult<&[u8]> {
        let Self::Offset {
            packet_type: expected_packet_type,
            offset,
        } = self
        else {
            panic!("packet data is not a PacketData::Offset")
        };

        // Get packet data
        let input = buffer
            .get((*offset - buffer_offset) as usize..)
            .ok_or(nom::Err::Error(NWNParseError::from_string(format!(
                "data offset {} is out of bounds",
                (*offset - buffer_offset)
            ))))?;

        // Update self
        *self = Self::from_bytes_clone(input, Some(*expected_packet_type))?.1;

        // Return stored buffer
        let PacketData::Buffer {
            packet_type: _,
            data,
        } = self
        else {
            panic!()
        };
        Ok((&[], data))
    }

    /// Converts self from a PacketData::Offset into a PacketData::Parsed
    pub fn parse_offset(&mut self, buffer: &[u8], buffer_offset: u32) -> NWNParseResult<()> {
        let Self::Offset {
            packet_type: expected_packet_type,
            offset,
        } = self
        else {
            panic!("packet data is not a PacketData::Offset")
        };

        // Get packet data
        let input = buffer
            .get((*offset - buffer_offset) as usize..)
            .ok_or(nom::Err::Error(NWNParseError::from_string(format!(
                "data offset {} is out of bounds",
                (*offset - buffer_offset)
            ))))?;

        // Update self
        *self = Self::from_bytes_parse(input, Some(*expected_packet_type))?.1;

        Ok((&[], ()))
    }

    /// Converts self from a PacketData::Buffer into a PacketData::Parsed
    pub fn parse_buffer(&mut self) -> NWNParseResult<()> {
        let PacketData::Buffer { packet_type, data } = self else {
            panic!("packet data is not a PacketData::Buffer")
        };

        // Update self
        *self = PacketData::Parsed(Box::new(Packet::from_bytes(*packet_type, data)?.1));

        Ok((&[], ()))
    }

    pub fn as_parsed(&self) -> Option<&Packet> {
        if let PacketData::Parsed(p) = self {
            Some(p.as_ref())
        } else {
            None
        }
    }
}

#[derive(Debug)]
struct PacketIndexEntry {
    packet_type: PacketType,
    offset: u32,
}

impl Trn {
    /// Parse the top of the TRN file, without loading / parsing all packets. Packets will be of type PacketData::Offset
    pub fn from_bytes_head(input: &[u8]) -> NWNParseResult<Self> {
        let (input, file_type) = FixedSizeString::from_bytes(input)?;
        let (input, version_major) = le_u16(input)?;
        let (input, version_minor) = le_u16(input)?;

        let (input, packets_count) = le_u32(input)?;
        let (input, packet_indices) = count(
            |input| -> NWNParseResult<PacketIndexEntry> {
                let (input, packet_type_str) = FixedSizeString::<4>::from_bytes(input)?;
                let (input, offset) = le_u32(input)?;

                let packet_type = PacketType::from(packet_type_str.as_str()).ok_or(
                    nom::Err::Error(NWNParseError::from_string(format!(
                        "invalid packet type {:?} in packet data",
                        packet_type_str
                    ))),
                )?;

                Ok((
                    input,
                    PacketIndexEntry {
                        packet_type,
                        offset,
                    },
                ))
            },
            packets_count as usize,
        )(input)
        .map_err(|e| e.map(|e| e.add_context_msg("while packet indices".to_string())))?;

        let packets = packet_indices
            .into_iter()
            .map(|pack_idx| {
                let packet_type = pack_idx.packet_type;
                let offset = pack_idx.offset;
                PacketData::Offset {
                    packet_type,
                    offset,
                }
            })
            .collect();

        Ok((
            input,
            Trn {
                file_type,
                version_major,
                version_minor,
                packets,
            },
        ))
    }

    /// Call PacketData::load_offset on all offset data (skipping non-offset packet data)
    pub fn load_offsets(&mut self, buffer: &[u8], buffer_offset: u32) -> NWNParseResult<()> {
        for packet in self.packets.iter_mut() {
            if let PacketData::Offset {
                packet_type: _,
                offset: _,
            } = packet
            {
                packet.load_offset(buffer, buffer_offset)?;
            }
        }
        Ok((&[], ()))
    }

    /// Call PacketData::parse_offset on all offset data (skipping non-offset packet data)
    pub fn parse_offsets(&mut self, buffer: &[u8], buffer_offset: u32) -> NWNParseResult<()> {
        for packet in self.packets.iter_mut() {
            if let PacketData::Offset {
                packet_type: _,
                offset: _,
            } = packet
            {
                packet.parse_offset(buffer, buffer_offset)?;
            }
        }
        Ok((&[], ()))
    }

    /// Parse the entire TRN file and each of its packets. Packets will be of type PacketData::Parsed
    pub fn from_bytes(input: &[u8]) -> NWNParseResult<Self> {
        let full_input = input;

        let (_, mut res) = Self::from_bytes_head(input)?;
        res.parse_offsets(full_input, 0)?;
        Ok((&[], res))
    }

    /// Writes the content of the TRN file and its packets to a stream. Packets must be of type PacketData::Buffer or PacketData::Parsed.
    pub fn write_stream<T>(&self, output: &mut T) -> Result<(), Box<dyn std::error::Error>>
    where
        T: std::io::Write + std::io::Seek,
    {
        // Write  header
        output.write_all(self.file_type.as_bytes())?;
        output.write_all(&self.version_major.to_le_bytes())?;
        output.write_all(&self.version_minor.to_le_bytes())?;
        output.write_all(&(self.packets.len() as u32).to_le_bytes())?;

        let index_offset = 12;
        let mut pack_data_offset = index_offset + (self.packets.len() * 8); // 4 chars type + u32 offset

        // skip writing indices and write packets data
        output.seek(std::io::SeekFrom::Start(pack_data_offset as u64))?;

        let mut pack_offsets = vec![0u32; self.packets.len()];
        for (packet_index, packet) in self.packets.iter().enumerate() {
            let packet_data = packet.to_bytes();
            output.write_all(&packet_data)?;

            pack_offsets[packet_index] = pack_data_offset as u32;
            pack_data_offset += packet_data.len();
        }

        // Write packet indices
        output.seek(std::io::SeekFrom::Start(index_offset as u64))?;
        for (packet_index, packet) in self.packets.iter().enumerate() {
            output.write_all(packet.get_type().to_str().as_bytes())?;
            output.write_all(&pack_offsets[packet_index].to_le_bytes())?;
        }

        Ok(())
    }

    /// Serialize the content of the TRN file and its packets to a buffer.
    /// Packets must be of type PacketData::Buffer or PacketData::Parsed.
    pub fn to_bytes(&self) -> Result<Vec<u8>, Box<dyn std::error::Error>> {
        let mut buf = std::io::Cursor::new(vec![]);
        self.write_stream(&mut buf)?;
        Ok(buf.into_inner())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_eauprofonde_portes() {
        let trn_bytes = include_bytes!("../../unittest/eauprofonde-portes.trx");
        let (_, trn) = Trn::from_bytes(trn_bytes).unwrap();

        // TRWH
        let Some(Packet::Trwh(trwh)) = trn.packets[0].as_parsed() else {
            panic!()
        };
        assert_eq!(trwh.width, 10);
        assert_eq!(trwh.height, 10);
        assert_eq!(trwh.id, 4370);

        let trwh_bytes = trwh.to_bytes();
        let (remaining, trwh2) = Trwh::from_bytes(&trwh_bytes).unwrap();
        assert_eq!(remaining.len(), 0);
        assert_eq!(trwh.width, trwh2.width);
        assert_eq!(trwh.height, trwh2.height);
        assert_eq!(trwh.id, trwh2.id);

        // WATR
        let Some(Packet::Watr(watr)) = trn.packets[101].as_parsed() else {
            panic!()
        };
        assert_eq!(watr.vertices.len(), 625);
        assert_eq!(watr.triangles.len(), 1152);
        let Some(Packet::Watr(watr)) = trn.packets[115].as_parsed() else {
            panic!()
        };
        assert_eq!(watr.vertices.len(), 625);
        assert_eq!(watr.triangles.len(), 1152);

        let watr_bytes = watr.to_bytes();
        let (remaining, watr2) = Watr::from_bytes(&watr_bytes).unwrap();
        assert_eq!(remaining.len(), 0);
        assert_eq!(watr.vertices.len(), watr2.vertices.len());
        assert_eq!(watr.triangles.len(), watr2.triangles.len());

        // TRRN
        let Some(Packet::Trrn(trrn)) = trn.packets[25].as_parsed() else {
            panic!()
        };
        assert_eq!(trrn.vertices.len(), 625);
        assert_eq!(trrn.triangles.len(), 1152);
        assert_eq!(trrn.grass.len(), 6);

        let trrn_bytes = trrn.to_bytes();
        let (remaining, trrn2) = Trrn::from_bytes(&trrn_bytes).unwrap();
        assert_eq!(remaining.len(), 0);
        assert_eq!(trrn.vertices.len(), trrn2.vertices.len());
        assert_eq!(trrn.triangles.len(), trrn2.triangles.len());
        assert_eq!(trrn.grass.len(), trrn2.grass.len());

        // ASWM
        let Some(Packet::Aswm(aswm)) = trn.packets[116].as_parsed() else {
            panic!()
        };
        assert_eq!(aswm.vertices.len(), 23123);
        assert_eq!(aswm.edges.len(), 66597);
        assert_eq!(aswm.triangles.len(), 43398);
        assert_eq!(aswm.tiles.len(), 1600);
        assert_eq!(aswm.islands.len(), 588);
        assert_eq!(aswm.islands_path_nodes.len(), 345744);

        let aswm_bytes = aswm.to_bytes();
        let (remaining, aswm2) = Aswm::from_bytes(&aswm_bytes).unwrap();
        assert_eq!(remaining.len(), 0);
        assert_eq!(aswm.vertices.len(), aswm2.vertices.len());
        assert_eq!(aswm.triangles.len(), aswm2.triangles.len());

        // Complete serialize + re-parse
        let ser_data = trn.to_bytes().unwrap();
        let (_, _trn2) = Trn::from_bytes(&ser_data).unwrap();
    }
}
