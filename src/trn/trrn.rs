#![allow(dead_code)]

use crate::parsing::*;
use nom::number::complete::*;
use nom::{
    bytes, character,
    multi::{count, fill},
    Err, Finish, IResult, Needed, Offset,
};

/// Terrain geometry, texture and grass
#[derive(Debug)]
pub struct Trrn {
    /// Name of the megatile
    pub name: FixedSizeString<128>,
    /// megatile terrain textures
    pub textures: [Texture; 6],
    /// vertices list
    pub vertices: Vec<Vertex>,
    /// triangles list
    pub triangles: Vec<Triangle>,
    /// 32 bit DDS bitmap. r,g,b,a defines the intensity of textures 0,1,2,3
    pub dds_a: Vec<u8>,
    /// 32 bit DDS bitmap. r,g defines the intensity of textures 4,5
    pub dds_b: Vec<u8>,
    /// Grass
    pub grass: Vec<Grass>,
}

/// Terrain texture name and color
#[derive(Debug, Copy, Clone)]
pub struct Texture {
    pub name: FixedSizeString<32>,
    pub color: [f32; 3],
}
/// Vertex information, with normals + colors + texture coordinates + weights
#[derive(Debug)]
pub struct Vertex {
    /// 3d coordinates of the vertex
    pub pos: [f32; 3],
    /// normal vector
    pub normal: [f32; 3],
    /// BGRA format. A is unused.
    pub color: [u8; 4],
    /// texture coordinates
    pub uv: [f32; 2],
    /// XY1 ?
    pub weights: [f32; 2],
}
/// Triangle information (vertices only)
#[derive(Debug)]
pub struct Triangle {
    pub vertices: [u16; 3],
}
/// Grass data
#[derive(Debug)]
pub struct Grass {
    /// Blades group name (unused?)
    pub name: FixedSizeString<32>,
    /// Grass texture name
    pub texture: FixedSizeString<32>,
    /// Grass blades list
    pub blades: Vec<GrassBlade>,
}
#[derive(Debug)]
pub struct GrassBlade {
    ///
    pub pos: [f32; 3],
    /// TODO: check if normal vector, or just a direction in XY plane
    pub normal: [f32; 3],
    ///
    pub uv: [f32; 3],
}

impl Trrn {
    /// Parses TRRN data
    pub fn from_bytes(input: &[u8]) -> NWNParseResult<Self> {
        let (input, name) = FixedSizeString::from_bytes(input).unwrap();
        let mut textures = [Texture {
            name: FixedSizeString::empty(),
            color: [0f32, 0f32, 0f32],
        }; 6];

        // Gather texture names & colors
        let mut input = input;
        for tex in &mut textures {
            (input, tex.name) = FixedSizeString::from_bytes(input)?;
        }
        for tex in &mut textures {
            (input, ()) = fill(le_f32, &mut tex.color)(input)?;
        }

        //
        let (input, vert_count) = le_u32(input)?;
        let (input, tri_count) = le_u32(input)?;

        let (input, vertices) = count(
            |input| -> NWNParseResult<Vertex> {
                let mut pos = [0f32; 3];
                let (input, ()) = fill(le_f32, &mut pos)(input)?;

                let mut normal = [0f32; 3];
                let (input, ()) = fill(le_f32, &mut normal)(input)?;

                let mut color = [0u8; 4];
                let (input, ()) = fill(le_u8, &mut color)(input)?;

                let mut uv = [0f32; 2];
                let (input, ()) = fill(le_f32, &mut uv)(input)?;

                let mut weights = [0f32; 2];
                let (input, ()) = fill(le_f32, &mut weights)(input)?;

                Ok((
                    input,
                    Vertex {
                        pos,
                        normal,
                        color,
                        uv,
                        weights,
                    },
                ))
            },
            vert_count as usize,
        )(input)
        .map_err(|e| e.map(|e| e.add_context_msg("while parsing vertices".to_string())))?;

        let (input, triangles) = count(
            |input| -> NWNParseResult<Triangle> {
                let mut vertices = [0u16; 3];
                let (input, ()) = fill(le_u16, &mut vertices)(input)?;
                Ok((input, Triangle { vertices }))
            },
            tri_count as usize,
        )(input)
        .map_err(|e| e.map(|e| e.add_context_msg("while parsing triangles".to_string())))?;

        // DDS
        let (input, dds_a_size) = le_u32(input)?;
        let (input, dds_a) = count(le_u8, dds_a_size as usize)(input).map_err(|e| {
            e.map(|e: NWNParseError| e.add_context_msg("while parsing DDS A".to_string()))
        })?;

        let (input, dds_b_size) = le_u32(input)?;
        let (input, dds_b) = count(le_u8, dds_b_size as usize)(input).map_err(|e| {
            e.map(|e: NWNParseError| e.add_context_msg("while parsing DDS B".to_string()))
        })?;

        let (input, grass_count) = le_u32(input)?;
        let (input, grass) = count(
            |input| -> NWNParseResult<Grass> {
                let (input, name) = FixedSizeString::from_bytes(input)?;
                let (input, texture) = FixedSizeString::from_bytes(input)?;

                let (input, blade_count) = le_u32(input)?;
                let (input, blades) = count(
                    |input| -> NWNParseResult<GrassBlade> {
                        let mut pos = [0f32; 3];
                        let (input, ()) = fill(le_f32, &mut pos)(input)?;

                        let mut normal = [0f32; 3];
                        let (input, ()) = fill(le_f32, &mut normal)(input)?;

                        let mut uv = [0f32; 3];
                        let (input, ()) = fill(le_f32, &mut uv)(input)?;

                        Ok((input, GrassBlade { pos, normal, uv }))
                    },
                    blade_count as usize,
                )(input)
                .map_err(|e| e.map(|e| e.add_context_msg("while parsing blades".to_string())))?;

                Ok((
                    input,
                    Grass {
                        name,
                        texture,
                        blades,
                    },
                ))
            },
            grass_count as usize,
        )(input)
        .map_err(|e| e.map(|e| e.add_context_msg("while parsing grass".to_string())))?;

        Ok((
            input,
            Self {
                name,
                textures,
                vertices,
                triangles,
                dds_a,
                dds_b,
                grass,
            },
        ))
    }

    /// Serializes TRRN data
    pub fn to_bytes(&self) -> Vec<u8> {
        let mut output = vec![];
        output.extend(self.name.as_bytes());

        // texture names & colors
        output.extend(self.textures.iter().flat_map(|t| t.name.as_bytes()));
        output.extend(
            self.textures
                .iter()
                .flat_map(|t| t.color.iter().flat_map(|v| v.to_le_bytes())),
        );

        output.extend((self.vertices.len() as u32).to_le_bytes());
        output.extend((self.triangles.len() as u32).to_le_bytes());

        // vertices
        output.extend(self.vertices.iter().flat_map(|vertex| {
            let mut output = vec![];

            output.extend(vertex.pos.iter().flat_map(|v| v.to_le_bytes()));
            output.extend(vertex.normal.iter().flat_map(|v| v.to_le_bytes()));
            output.extend(vertex.color.iter().flat_map(|v| v.to_le_bytes()));
            output.extend(vertex.uv.iter().flat_map(|v| v.to_le_bytes()));
            output.extend(vertex.weights.iter().flat_map(|v| v.to_le_bytes()));

            output
        }));

        // triangles
        output.extend(
            self.triangles
                .iter()
                .flat_map(|t| t.vertices.iter().flat_map(|v| v.to_le_bytes())),
        );

        // DDS
        output.extend((self.dds_a.len() as u32).to_le_bytes());
        output.extend(&self.dds_a);
        output.extend((self.dds_b.len() as u32).to_le_bytes());
        output.extend(&self.dds_b);

        // Grass
        output.extend((self.grass.len() as u32).to_le_bytes());
        output.extend(self.grass.iter().flat_map(|grass| {
            let mut output = vec![];

            output.extend(grass.name.as_bytes());
            output.extend(grass.texture.as_bytes());

            output.extend((grass.blades.len() as u32).to_le_bytes());
            output.extend(grass.blades.iter().flat_map(|blade| {
                let mut output = vec![];

                output.extend(blade.pos.iter().flat_map(|v| v.to_le_bytes()));
                output.extend(blade.normal.iter().flat_map(|v| v.to_le_bytes()));
                output.extend(blade.uv.iter().flat_map(|v| v.to_le_bytes()));

                output
            }));

            output
        }));

        output
    }
}
