#![allow(dead_code)]

use nom::multi::{count, fill};
use nom::number::complete::*;

use crate::parsing::*;

/// Terrain water data
#[derive(Debug)]
pub struct Watr {
    pub name: FixedSizeString<32>,
    pub unknown: [u8; 96],
    /// R,G,B
    pub color: [f32; 3],
    /// Ripples
    pub ripple: [f32; 2],
    /// Smoothness
    pub smoothness: f32,
    /// Reflection bias
    pub reflect_bias: f32,
    /// Reflection power
    pub reflect_power: f32,
    /// Specular map power
    pub specular_power: f32,
    /// Specular map coefficient
    pub specular_cofficient: f32,
    /// Water textures
    pub textures: [Texture; 3],
    /// x,y offset in water-space <=> megatile_coordinates/8
    pub uv_offset: [f32; 2],
    pub vertices: Vec<Vertex>,
    pub triangles: Vec<Triangle>,
    pub triangles_flags: Vec<u32>,
    pub dds: Vec<u8>,
    /// Position of the associated megatile in the terrain
    pub megatile_position: [u32; 2],
}

/// Water texture information
#[derive(Debug, Copy, Clone)]
pub struct Texture {
    /// Texture name
    pub name: FixedSizeString<32>,
    /// Scrolling direction
    pub direction: [f32; 2],
    /// Scrolling speed
    pub rate: f32,
    /// Scrolling angle in radiant
    pub angle: f32,
}
/// Vertex information, with texture coordinates
#[derive(Debug)]
pub struct Vertex {
    /// 3d coordinates of the vertex
    pub pos: [f32; 3],
    /// XY5 (set to uv * 5.0)
    pub uvx5: [f32; 2],
    /// texture coordinates
    pub uv: [f32; 2],
}
/// Triangle information (vertices only)
#[derive(Debug)]
pub struct Triangle {
    /// Triangle vertex indices
    pub vertices: [u16; 3],
}

impl Watr {
    /// Parses WATR data
    pub fn from_bytes(input: &[u8]) -> NWNParseResult<Self> {
        let (input, name) = FixedSizeString::from_bytes(input)?;

        let mut unknown = [0u8; 96];
        let (input, ()) = fill(le_u8, &mut unknown)(input)?;

        let mut color = [0f32; 3];
        let (input, ()) = fill(le_f32, &mut color)(input)?;

        let mut ripple = [0f32; 2];
        let (input, ()) = fill(le_f32, &mut ripple)(input)?;

        let (input, smoothness) = le_f32(input)?;
        let (input, reflect_bias) = le_f32(input)?;
        let (input, reflect_power) = le_f32(input)?;
        let (input, specular_power) = le_f32(input)?;
        let (input, specular_cofficient) = le_f32(input)?;

        let mut textures = [Texture {
            name: FixedSizeString::empty(),
            direction: [0f32; 2],
            rate: 0f32,
            angle: 0f32,
        }; 3];
        let (input, ()) = fill(
            |input| -> NWNParseResult<_> {
                let (input, name) = FixedSizeString::from_bytes(input)?;
                let mut direction = [0f32; 2];
                let (input, ()) = fill(le_f32, &mut direction)(input)?;
                let (input, rate) = le_f32(input)?;
                let (input, angle) = le_f32(input)?;

                Ok((
                    input,
                    Texture {
                        name,
                        direction,
                        rate,
                        angle,
                    },
                ))
            },
            &mut textures,
        )(input)
        .map_err(|e| e.map(|e| e.add_context_msg("while parsing textures".to_string())))?;

        let mut uv_offset = [0f32; 2];
        let (input, ()) = fill(le_f32, &mut uv_offset)(input)?;

        let (input, vert_count) = le_u32(input)?;
        let (input, tri_count) = le_u32(input)?;

        let (input, vertices) = count(
            |input| -> NWNParseResult<_> {
                let mut pos = [0f32; 3];
                let (input, ()) = fill(le_f32, &mut pos)(input)?;

                let mut uvx5 = [0f32; 2];
                let (input, ()) = fill(le_f32, &mut uvx5)(input)?;

                let mut uv = [0f32; 2];
                let (input, ()) = fill(le_f32, &mut uv)(input)?;

                Ok((input, Vertex { pos, uvx5, uv }))
            },
            vert_count as usize,
        )(input)
        .map_err(|e| e.map(|e| e.add_context_msg("while parsing vertices".to_string())))?;

        let (input, triangles) = count(
            |input| -> NWNParseResult<_> {
                let mut vertices = [0u16; 3];
                let (input, ()) = fill(le_u16, &mut vertices)(input)?;
                Ok((input, Triangle { vertices }))
            },
            tri_count as usize,
        )(input)
        .map_err(|e| e.map(|e| e.add_context_msg("while parsing triangles".to_string())))?;

        for (i, triangle) in triangles.iter().enumerate() {
            if triangle
                .vertices
                .iter()
                .any(|&a| a as usize >= vertices.len())
            {
                return Err(nom::Err::Error(
                    format!(
                        "triangle[{}] contains out-of-bounds vertex indices: {:?}",
                        i, triangle.vertices
                    )
                    .into(),
                ));
            }
        }

        let (input, triangles_flags) = count(le_u32::<_, NWNParseError>, tri_count as usize)(input)
            .map_err(|e| {
                e.map(|e| e.add_context_msg("while parsing triangles flags".to_string()))
            })?;

        let (input, dds_size) = le_u32(input)?;
        let (input, dds) =
            count(le_u8::<_, NWNParseError>, dds_size as usize)(input).map_err(|e| {
                e.map(|e| e.add_context_msg(format!("while fetching DDS data (size={})", dds_size)))
            })?;

        let mut megatile_position = [0u32; 2];
        let (input, ()) =
            fill(le_u32::<_, NWNParseError>, &mut megatile_position)(input).map_err(|e| {
                e.map(|e| e.add_context_msg("while parsing megatile position".to_string()))
            })?;

        Ok((
            input,
            Self {
                name,
                unknown,
                color,
                ripple,
                smoothness,
                reflect_bias,
                reflect_power,
                specular_power,
                specular_cofficient,
                textures,
                uv_offset,
                vertices,
                triangles,
                triangles_flags,
                dds,
                megatile_position,
            },
        ))
    }

    /// Serializes WATR data
    pub fn to_bytes(&self) -> Vec<u8> {
        let mut output = vec![];

        output.extend(self.name.as_bytes());
        output.extend(&self.unknown);

        output.extend(self.color.iter().flat_map(|v| v.to_le_bytes()));
        output.extend(self.ripple.iter().flat_map(|v| v.to_le_bytes()));

        output.extend(self.smoothness.to_le_bytes());
        output.extend(self.reflect_bias.to_le_bytes());
        output.extend(self.reflect_power.to_le_bytes());
        output.extend(self.specular_power.to_le_bytes());
        output.extend(self.specular_cofficient.to_le_bytes());

        output.extend(self.textures.iter().flat_map(|t| {
            let mut output = vec![];

            output.extend(t.name.as_bytes());
            output.extend(t.direction.iter().flat_map(|v| v.to_le_bytes()));
            output.extend(t.rate.to_le_bytes());
            output.extend(t.angle.to_le_bytes());

            output
        }));

        output.extend(self.uv_offset.iter().flat_map(|v| v.to_le_bytes()));

        output.extend((self.vertices.len() as u32).to_le_bytes());
        output.extend((self.triangles.len() as u32).to_le_bytes());

        // vertices
        output.extend(self.vertices.iter().flat_map(|vertex| {
            let mut output = vec![];

            output.extend(vertex.pos.iter().flat_map(|v| v.to_le_bytes()));
            output.extend(vertex.uvx5.iter().flat_map(|v| v.to_le_bytes()));
            output.extend(vertex.uv.iter().flat_map(|v| v.to_le_bytes()));

            output
        }));

        // triangles
        output.extend(
            self.triangles
                .iter()
                .flat_map(|t| t.vertices.iter().flat_map(|v| v.to_le_bytes())),
        );
        output.extend(self.triangles_flags.iter().flat_map(|v| v.to_le_bytes()));

        // DDS
        output.extend((self.dds.len() as u32).to_le_bytes());
        output.extend(&self.dds);

        output.extend(self.megatile_position.iter().flat_map(|v| v.to_le_bytes()));

        output
    }
}
