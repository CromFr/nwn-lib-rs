#![allow(dead_code)]

use crate::parsing::*;
use nom::number::complete::le_u32;

/// Terrain width height
#[derive(Debug)]
pub struct Trwh {
    /// Width in megatiles
    pub width: u32,
    /// Height in megatiles
    pub height: u32,
    /// Unknown
    pub id: u32,
}

impl Trwh {
    /// Parses TRWH data
    pub fn from_bytes(input: &[u8]) -> NWNParseResult<Self> {
        let (input, width) = le_u32(input)?;
        let (input, height) = le_u32(input)?;
        let (input, id) = le_u32(input)?;

        Ok((input, Self { width, height, id }))
    }

    /// Serializes TRWH data
    pub fn to_bytes(&self) -> Vec<u8> {
        let mut output = vec![];
        output.extend(self.width.to_le_bytes());
        output.extend(self.height.to_le_bytes());
        output.extend(self.id.to_le_bytes());
        output
    }
}
