//! 2DA table file format (.2da)
//!
//! Example
//! ```rust
//! #![feature(generic_const_exprs)]
//! #![allow(incomplete_features)]
//! use nwn_lib_rs::twoda::TwoDA;
//!
//! // Load a 2DA file
//! let twoda_data = std::fs::read_to_string("unittest/restsys_wm_tables.2da").unwrap();
//! let (_, twoda) = TwoDA::from_str(&twoda_data, true).unwrap();
//!
//! // Get 2DA value
//! assert_eq!(twoda.get_row_label(5, "TableName").as_deref(), Some("UpperBarrow"));
//! assert_eq!(twoda.get_row_label(5, "Enc1_ResRefs").as_deref(), Some("a_telthorwolf,a_telthorwolf"));
//!
//! // Serialize beautiful 2DA data
//! let twoda_data = twoda.to_string(false);
//! ```
//!

use std::cmp::max;
use std::collections::{BTreeMap, HashMap};

use crate::parsing::*;

use nom::character::complete as ncc;
use nom::error::VerboseError;
use nom::IResult;

use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize)]
struct TwoDARepr {
    file_type: FixedSizeString<4>,
    file_version: FixedSizeString<4>,
    default_value: Option<String>,
    columns: Vec<String>,
    rows: Vec<Vec<Option<String>>>,
}
impl From<TwoDARepr> for TwoDA {
    fn from(repr: TwoDARepr) -> Self {
        let column_lookup = repr
            .columns
            .iter()
            .enumerate()
            .map(|(i, col)| (col.to_lowercase(), i))
            .collect::<HashMap<_, _>>();
        let data = repr.rows.into_iter().flatten().collect(); // TODO: this does not handle well inconsistent row/col numbers
        Self {
            file_type: repr.file_type,
            file_version: repr.file_version,
            default_value: repr.default_value,
            columns: repr.columns.into_iter().collect(),
            column_lookup,
            data,
            merge_meta: None,
        }
    }
}
impl From<TwoDA> for TwoDARepr {
    fn from(twoda: TwoDA) -> Self {
        let rows: Vec<Vec<Option<String>>> = twoda.iter().map(|row| row.to_vec()).collect();
        Self {
            file_type: twoda.file_type,
            file_version: twoda.file_version,
            default_value: twoda.default_value,
            columns: twoda.columns,
            rows,
        }
    }
}

/// 2DA table file
#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(from = "TwoDARepr", into = "TwoDARepr")]
pub struct TwoDA {
    /// File type (2DA)
    pub file_type: FixedSizeString<4>,
    /// File version
    pub file_version: FixedSizeString<4>,
    pub default_value: Option<String>,
    columns: Vec<String>,
    column_lookup: HashMap<String, usize>,
    data: Vec<Option<String>>,
    merge_meta: Option<MergeMetadata>,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum MergeAction {
    Ignore,
    Expect,
    Set,
}

#[derive(Debug, Clone)]
struct MergeMetadata {
    /// If a row number is referenced here, only columns with a true mask must be merged
    merge_actions: BTreeMap<usize, Vec<MergeAction>>,
}

impl TwoDA {
    /// Parse 2DA data an input str
    ///
    /// # Args:
    /// * input: data to parse
    /// * repair: true to repair inconsistent 2DA data (bad column count, row indices, ...)
    pub fn from_str(input: &str, repair: bool) -> IResult<&str, Self, VerboseError<&str>> {
        // 2DA V2.0
        //
        //     Terrain               R    G    B    Material STR_REF
        // 0   TT_GD_Dirt_01         124  109  27   Dirt     ****
        // 1   TT_GD_Dirt_02         84   78   17   Dirt     ****

        let (input, twoda_header) = ncc::not_line_ending(input)?;
        let (input, _) = ncc::line_ending(input)?;
        let file_type = FixedSizeString::<4>::from_str(
            twoda_header
                .get(0..4)
                .ok_or(nom_context_error("Missing file type", input))?,
        )
        .unwrap(); // string cannot be too long
        let file_version = FixedSizeString::<4>::from_str(
            twoda_header
                .get(4..8)
                .ok_or(nom_context_error("Missing file version", input))?,
        )
        .unwrap(); // string cannot be too long

        let mut merge_meta = if file_type == "2DAM" {
            Some(MergeMetadata {
                merge_actions: BTreeMap::new(),
            })
        } else {
            None
        };

        enum ParserState {
            Defaults,
            Headers,
            Rows,
        }

        // Data
        let mut twoda_data = vec![];
        let mut state = ParserState::Defaults;
        let mut default_value = None;
        let mut columns = vec![];
        let mut input = input;
        while input.len() > 0 {
            // Consume entire line
            let line;
            (input, line) = ncc::not_line_ending(input)?;
            if input.len() > 0 {
                (input, _) = ncc::line_ending(input)?;
            }

            // Skip empty lines (do not increment row index)
            if line.is_empty() || line.chars().all(|c| c.is_whitespace()) {
                continue;
            }

            // Handle default value
            if let ParserState::Defaults = state {
                if line.starts_with("DEFAULT:") {
                    (_, default_value) = Self::field_parser(line)?;
                    state = ParserState::Headers;
                    continue;
                } else {
                    state = ParserState::Headers;
                    // continue parsing this line
                }
            }

            match state {
                ParserState::Defaults => panic!("handled previously"),
                ParserState::Headers => {
                    columns = Self::parse_row(line)?
                        .1
                        .into_iter()
                        .map(|c| c.unwrap_or("".to_string()))
                        .collect::<Vec<_>>();
                    if columns.len() == 0 {
                        return Err(nom_context_error("no columns detected", input));
                    }
                    state = ParserState::Rows;
                }
                ParserState::Rows => {
                    let (mut row_data, row_merge_actions): (_, Option<Vec<_>>) =
                        if merge_meta.is_none() {
                            (Self::parse_row(line)?.1, None)
                        } else {
                            Self::parse_merge_row(line)?.1
                        };

                    if row_data.is_empty() {
                        return Err(nom_context_error("missing row number", line));
                    }

                    let row_index_res = row_data.remove(0).map(|s| s.parse::<usize>());

                    let row_index = if let Some(Ok(idx)) = row_index_res {
                        idx
                    } else if repair {
                        0
                    } else {
                        return Err(nom_context_error("failed to parse row number", line));
                    };

                    if row_data.len() != columns.len() {
                        if repair {
                            row_data.resize(columns.len(), None);
                        } else {
                            return Err(nom_context_error("Invalid number of columns", line));
                        }
                    }

                    if let Some(ref mut merge_meta) = merge_meta {
                        // Trust row indices
                        let start = row_index * columns.len();
                        let end = start + columns.len();
                        if twoda_data.len() < end {
                            twoda_data.resize(end, None);
                        }

                        twoda_data[start..end].clone_from_slice(&row_data);

                        // Save merge mask if any
                        if let Some(mut row_merge_actions) = row_merge_actions {
                            // Remove ? "merge action" for index number
                            row_merge_actions.remove(0);

                            if row_merge_actions.len() != row_data.len() {
                                row_merge_actions.resize(row_data.len(), MergeAction::Ignore);
                            }
                            merge_meta
                                .merge_actions
                                .insert(row_index, row_merge_actions);
                        }
                    } else {
                        if row_index != twoda_data.len() / columns.len() && !repair {
                            return Err(nom_context_error("Inconsistent row index", line));
                        }

                        // Copy all data except row number field
                        twoda_data.extend_from_slice(&row_data);
                    }
                }
            }
        }

        let column_lookup = columns
            .iter()
            .enumerate()
            .map(|(i, col)| (col.to_lowercase(), i))
            .collect::<HashMap<_, _>>();

        Ok((
            input,
            Self {
                file_type,
                file_version,
                default_value,
                columns,
                column_lookup,
                data: twoda_data,
                merge_meta,
            },
        ))
    }
    /// Serializes the 2DA table
    ///
    /// # Args:
    /// * compact: true will generate minified 2DA data. NWN2 will be able to
    ///   parse this table, but this parser will require repair=true to be set
    ///   when parsing
    pub fn to_string(&self, compact: bool) -> String {
        let mut ret = String::new();
        ret.push_str("2DA V2.0\n\n");
        ret.push_str(&Self::encode_rows(
            &self.iter().enumerate().collect::<Vec<_>>(),
            Some(&self.columns),
            compact,
        ));

        ret
    }

    /// Returns the columns list
    pub fn get_columns(&self) -> &Vec<String> {
        &self.columns
    }

    /// Returns the number of columns
    pub fn len_cols(&self) -> usize {
        self.columns.len()
    }

    /// Returns the number of rows
    pub fn len_rows(&self) -> usize {
        self.data.len() / self.columns.len()
    }

    /// Resize the 2DA table to be able to contain `len` rows
    pub fn resize_rows(&mut self, len: usize) {
        self.data.resize(len * self.len_cols(), None)
    }

    /// Returns a row as a list of 2DA fields
    pub fn get_row(&self, row: usize) -> Option<&[Option<String>]> {
        let start = row * self.columns.len();
        let end = start + self.columns.len();
        self.data.get(start..end)
    }

    /// Returns a row as a list of 2DA fields
    pub fn get_row_mut(&mut self, row: usize) -> Option<&mut [Option<String>]> {
        let start = row * self.columns.len();
        let end = start + self.columns.len();
        self.data.get_mut(start..end)
    }

    /// Returns a field using its row index and column index
    pub fn get_row_col(&self, row: usize, column: usize) -> &Option<String> {
        let index = row * self.columns.len() + column;
        if let Some(field) = self.data.get(index) {
            field
        } else {
            &None
        }
    }

    /// Returns a field using its row index and column name. Column names are case-insensitive
    pub fn get_row_label(&self, row: usize, label: &str) -> &Option<String> {
        // TODO: only call to_lowercase if not lowercase
        if let Some(col) = self.column_lookup.get(&label.to_string().to_lowercase()) {
            self.get_row_col(row, *col)
        } else {
            &None
        }
    }

    fn str_encoded_len<S: AsRef<str>>(s: S) -> usize {
        let s = s.as_ref();
        if s == "****" || s.chars().any(char::is_whitespace) {
            // Quoted field
            let mut len = 2usize; // Two double quotes
            for c in s.chars() {
                match c {
                    '\\' => len += 1,
                    '\n' => len += 1,
                    '\t' => len += 1,
                    '"' => len += 1,
                    _ => {}
                }
                len += c.len_utf8();
            }
            len
        } else {
            // Raw field
            s.len()
        }
    }
    /// Returns the length of a 2DA field when encoded (using quotes and escape sequences when needed)
    pub fn field_encoded_len<S: AsRef<str>>(field: &Option<S>) -> usize {
        match field.as_ref() {
            None => 4,
            Some(s) => Self::str_encoded_len(s),
        }
    }

    /// Encode a 2DA field, adding quotes when needed, and returns the resulting string
    pub fn encode_field<S: AsRef<str>>(field: &Option<S>) -> String {
        // TODO: Return either &str or String to avoid unnecessary allocations?
        match field {
            None => "****".to_string(),
            Some(s) => {
                let s = s.as_ref();
                if s == "****" || s.chars().any(char::is_whitespace) {
                    // Quoted field
                    let mut res = "\"".to_string();
                    for c in s.chars() {
                        match c {
                            '\\' => res.push_str(r"\\"),
                            '\n' => res.push_str(r"\n"),
                            '\t' => res.push_str(r"\t"),
                            '"' => res.push_str("\\\""),
                            _ => res.push(c),
                        }
                    }
                    res.push('\"');
                    res
                } else {
                    // Raw field
                    s.to_string()
                }
            }
        }
    }

    fn field_parser(input: &str) -> IResult<&str, Option<String>, VerboseError<&str>> {
        if input.is_empty() || input.chars().all(char::is_whitespace) {
            return Err(nom_context_error("End of input", input));
        }

        if let Some(input) = input.strip_prefix('"') {
            let mut res = String::new();
            let mut escaped = false;
            let mut input_end = 0usize;
            for c in input.chars() {
                input_end += c.len_utf8();
                if escaped {
                    escaped = false;
                    match c {
                        '\\' => res.push('\\'),
                        'n' => res.push('\n'),
                        't' => res.push('\t'),
                        '"' => res.push('"'),
                        c => res.extend(&['\\', c]),
                    }
                } else {
                    match c {
                        '\\' => escaped = true,
                        '"' => break,
                        c => res.push(c),
                    }
                }
            }
            let input = &input[input_end..];
            // eprintln!("   => field_parser end:   input={:?}", input);
            Ok((input, Some(res)))
        } else {
            let end = input.find(char::is_whitespace);
            let (input, res) = if let Some(end) = end {
                (&input[end..], &input[..end])
            } else {
                (&input[input.len()..], input)
            };

            // eprintln!("   => field_parser end:   input={:?}", input);
            if res == "****" {
                Ok((input, None))
            } else {
                Ok((input, Some(res.to_string())))
            }
        }
    }

    /// Parses a 2DA row returning a list of fields (including the row index number)
    pub fn parse_row(input: &str) -> IResult<&str, Vec<Option<String>>, VerboseError<&str>> {
        let (input, _) = ncc::space0(input)?;
        nom::multi::separated_list0(ncc::space1, Self::field_parser)(input)
    }

    /// Parses a 2DAM row returning a list of fields and merge mask
    fn parse_merge_row(
        input: &str,
    ) -> IResult<&str, (Vec<Option<String>>, Option<Vec<MergeAction>>), VerboseError<&str>> {
        let (input, _) = ncc::space0(input)?;

        if input.starts_with('?') {
            let (input, fields_meta) = nom::multi::separated_list0(
                ncc::space1,
                nom::sequence::tuple((ncc::one_of("?=-"), Self::field_parser)),
            )(input)?;

            let (mda, fields) = fields_meta
                .into_iter()
                .map(|(c, field)| {
                    let action = match c {
                        '?' => MergeAction::Expect,
                        '-' => MergeAction::Ignore,
                        '=' => MergeAction::Set,
                        _ => panic!(),
                    };
                    (action, field)
                })
                .unzip();

            Ok((input, (fields, Some(mda))))

            // unimplemented!()
        } else {
            let (input, fields) = Self::parse_row(input)?;
            Ok((input, (fields, None)))
        }
    }

    /// Serializes a row to string
    ///
    /// # Args:
    /// * row_index: Row index number
    /// * row_data: List of row fields (without the row index)
    /// * column_sizes: If Some, the sizes will be used for padding the different fields, to align multiple columns together
    /// * compact: true to minify the row by removing empty trailing fields. Note: column_sizes is used even if compact is true
    pub fn encode_row<S: AsRef<str>>(
        row_index: usize,
        row_data: &[Option<S>],
        column_sizes: Option<&[usize]>,
        compact: bool,
    ) -> String {
        let get_col_size = |i: usize| {
            if let Some(column_sizes) = column_sizes {
                assert_eq!(row_data.len() + 1, column_sizes.len());
                column_sizes[i]
            } else {
                0
            }
        };

        let mut ret = String::new();
        ret.push_str(&format!("{:<w$}", row_index, w = get_col_size(0)));

        let end = if compact {
            let last_column = row_data
                .iter()
                .enumerate()
                .rfind(|&(_, field)| field.is_some());
            if let Some((i, _)) = last_column {
                i + 1
            } else {
                1
            }
        } else {
            row_data.len()
        };

        ret.push_str(
            &row_data[..end]
                .iter()
                .enumerate()
                .map(|(i, field)| {
                    format!("{:<w$}", Self::encode_field(field), w = get_col_size(i + 1))
                })
                .fold(String::new(), |mut res, s| {
                    res.push(' ');
                    res.push_str(&s);
                    res
                }),
        );
        ret
    }
    /// Serializes a list of row to string
    ///
    /// # Args:
    /// * rows: List of tuples containing the row index + fields list
    /// * columns: If Some, adds the columns list atop if the rows
    /// * compact: true to minify the output by not aligning columns and removing trailing empty fields
    pub fn encode_rows<S1: AsRef<str>, S2: AsRef<str>>(
        rows: &[(usize, &[Option<S1>])],
        columns: Option<&[S2]>,
        compact: bool,
    ) -> String {
        let mut column_sizes = vec![
            0usize;
            rows.iter()
                .map(|(_, cols)| cols.len())
                .max()
                .expect("no rows to encode")
                + 1
        ];

        if !compact {
            column_sizes[0] = rows
                .iter()
                .map(|(i, _)| i)
                .max()
                .expect("no rows to encode")
                .ilog10() as usize
                + 1;
            if let Some(columns) = columns {
                for (i, column) in columns.iter().enumerate() {
                    if i + 1 == columns.len() {
                        continue; // Last column must not get padded
                    }
                    column_sizes[i + 1] = max(column_sizes[i + 1], Self::str_encoded_len(column));
                }
            }

            for (_irow, row) in rows {
                for (ifield, field) in row.iter().enumerate() {
                    if ifield + 2 == column_sizes.len() {
                        continue; // Last column must not get padded
                    }
                    column_sizes[ifield + 1] =
                        max(column_sizes[ifield + 1], Self::field_encoded_len(field));
                }
            }
        }

        let mut ret = String::new();

        if let Some(columns) = columns {
            ret.push_str(&" ".repeat(column_sizes[0]));
            ret.push_str(
                &columns
                    .iter()
                    .enumerate()
                    .map(|(i, col)| format!("{:<w$}", col.as_ref(), w = column_sizes[i + 1]))
                    .fold(String::new(), |mut res, s| {
                        res.push(' ');
                        res.push_str(&s);
                        res
                    }),
            );
            ret.push('\n');
        }

        for (i, (irow, row)) in rows.iter().enumerate() {
            ret.push_str(&Self::encode_row(*irow, row, Some(&column_sizes), compact));
            if i + 1 < rows.len() {
                ret.push('\n');
            }
        }
        ret
    }

    /// Returns an iterator over each 2DA row
    pub fn iter(&self) -> std::slice::Chunks<'_, Option<String>> {
        self.data.chunks(self.columns.len())
    }

    /// Returns true if the 2DA is meant to be merged (2DAM file type)
    pub fn is_merge(&self) -> bool {
        self.merge_meta.is_some()
    }

    /// Returns the merge mask for the given row, if set. Only useful if it was parsed as a 2DAM file
    pub fn get_row_merge_actions(&self, row_index: usize) -> Option<&[MergeAction]> {
        if let Some(meta) = self.merge_meta.as_ref() {
            meta.merge_actions.get(&row_index).map(Vec::as_slice)
        } else {
            None
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_twoda() {
        let twoda_bytes = include_str!("../unittest/restsys_wm_tables.2da");

        let twoda = TwoDA::from_str(twoda_bytes, true).unwrap().1;
        assert_eq!(
            twoda.get_row_label(0, "TableName"),
            &Some("INVALID_TABLE".to_string())
        );
        assert_eq!(
            twoda.get_row_label(0, "Comment"),
            &Some(",start w/ comma (makes excel add quotes!) Dont_change this row!".to_string())
        );
        assert_eq!(
            twoda.get_row_label(0, "FeedBackStrRefSuccess"),
            &Some("83306".to_string())
        );
        assert_eq!(twoda.get_row_label(0, "Enc3_ResRefs"), &None);
        assert_eq!(
            twoda.get_row_label(21, "TableName"),
            &Some("b_wells_of_lurue".to_string())
        );
        assert_eq!(
            twoda.get_row_label(21, "Enc3_ResRefs"),
            &Some("b10_earth_elemental,b10_orglash".to_string())
        );

        // Bad number of columns
        // Row 10 has only 8 columns instead of 9
        assert!(TwoDA::from_str(twoda_bytes, false).is_err());
        assert_eq!(
            twoda.get_row_label(10, "TableName"),
            &Some("c_cave".to_string())
        );
        assert_eq!(
            twoda.get_row_label(10, "Comment"),
            &Some(",beasts".to_string())
        );
        assert_eq!(
            twoda.get_row_label(10, "Enc2_Prob"),
            &Some("50".to_string())
        );
        assert_eq!(twoda.get_row_label(10, "Enc3_ResRefs"), &None);
    }
    #[test]
    fn test_bloodmagus() {
        // Contains CRLF line endings
        let twoda_bytes = include_str!("../unittest/cls_feat_bloodmagus.2DA");
        let twoda = TwoDA::from_str(twoda_bytes, false).unwrap().1;

        assert_eq!(twoda.columns.len(), 5);
        assert_eq!(twoda.len_rows(), 22);

        assert_eq!(twoda.columns[0], "FeatLabel");
        assert_eq!(
            twoda.get_row_label(0, "FeatLabel"),
            &Some("FEAT_EPIC_SPELL_DRAGON_KNIGHT".to_string())
        );
        assert_eq!(twoda.columns[1], "FeatIndex");
        assert_eq!(
            twoda.get_row_label(8, "FeatIndex"),
            &Some("2784".to_string())
        );
    }
    #[test]
    fn test_savthr_nw9a() {
        // Contains empty lines between column names and data
        let twoda_bytes = include_str!("../unittest/cls_savthr_nw9a.2DA");
        let twoda = TwoDA::from_str(twoda_bytes, false).unwrap().1;

        assert_eq!(twoda.columns.len(), 4);
        assert_eq!(twoda.len_rows(), 5);

        assert_eq!(twoda.columns[0], "Level");

        assert_eq!(
            twoda.get_row(0).unwrap(),
            &[
                Some("1".to_string()),
                Some("2".to_string()),
                Some("0".to_string()),
                Some("2".to_string())
            ]
        );
        assert_eq!(
            twoda.get_row(1).unwrap(),
            &[
                Some("2".to_string()),
                Some("3".to_string()),
                Some("0".to_string()),
                Some("3".to_string())
            ]
        );
        assert_eq!(
            twoda.get_row(3).unwrap(),
            &[
                Some("4".to_string()),
                Some("4".to_string()),
                Some("1".to_string()),
                Some("4".to_string())
            ]
        );
    }
    #[test]
    fn test_nwn_2_colors() {
        // No newline on data end
        let twoda_bytes = include_str!("../unittest/NWN2_Colors.2DA");
        let twoda = TwoDA::from_str(twoda_bytes, false).unwrap().1;

        assert_eq!(twoda.columns.as_slice(), &["ColorName", "ColorHEX"]);
        assert_eq!(twoda.len_rows(), 153);

        assert_eq!(
            twoda.get_row(0).unwrap(),
            &[Some("AliceBlue".to_string()), Some("F0F8FF".to_string()),]
        );
        assert_eq!(
            twoda.get_row(152).unwrap(),
            &[
                Some("HotbarItmNoUse".to_string()),
                Some("FF0000".to_string()),
            ]
        );
    }
    #[test]
    fn test_cls_bfeat_dwdef() {
        let twoda_bytes = include_str!("../unittest/cls_bfeat_dwdef.2da");
        let twoda = TwoDA::from_str(twoda_bytes, true).unwrap().1;

        assert_eq!(twoda.columns.as_slice(), &["Bonus"]);
        assert_eq!(twoda.len_rows(), 60);
    }
    #[test]
    fn test_combatmodes() {
        // Has random text notes near the end
        let twoda_bytes = include_str!("../unittest/combatmodes.2da");
        let twoda = TwoDA::from_str(twoda_bytes, true).unwrap().1;

        assert_eq!(
            twoda.columns.as_slice(),
            &[
                "CombatMode",
                "ActivatedName",
                "DeactivatedName",
                "NeedsTarget"
            ]
        );
        assert_eq!(twoda.len_rows(), 19);
        assert_eq!(
            twoda.get_row(0).unwrap(),
            &[
                Some("Detect".to_string()),
                Some("58275".to_string()),
                Some("58276".to_string()),
                Some("0".to_string())
            ]
        );
        assert_eq!(
            twoda.get_row(13).unwrap(),
            &[
                Some("Taunt".to_string()),
                Some("11468".to_string()),
                Some("11468".to_string()),
                Some("0".to_string())
            ]
        );
        assert_eq!(
            twoda.get_row(14).unwrap(),
            &[Some("Attack".to_string()), None, None, None]
        );
        assert_eq!(
            twoda.get_row(15).unwrap(),
            &[Some("Power_Attack".to_string()), None, None, None]
        );
        assert_eq!(
            twoda.get_row(18).unwrap(),
            &[
                Some("stealth,".to_string()),
                Some("counterspell,".to_string()),
                Some("defensive_Cast,".to_string()),
                Some("taunt".to_string())
            ]
        );
    }
    #[test]
    fn test_creaturespeed() {
        // Has missing default line
        let twoda_bytes = include_str!("../unittest/creaturespeed.2da");
        let twoda = TwoDA::from_str(twoda_bytes, true).unwrap().1;

        assert_eq!(
            twoda.columns.as_slice(),
            &["Label", "Name", "2DAName", "WALKRATE", "RUNRATE"]
        );
        assert_eq!(twoda.len_rows(), 9);

        assert_eq!(
            twoda.get_row(0).unwrap(),
            &[
                Some("PC_Movement".to_string()),
                None,
                Some("PLAYER".to_string()),
                Some("2.00".to_string()),
                Some("4.00".to_string())
            ]
        );
    }
    #[test]
    fn test_des_treas_items() {
        // Has missing / non-numeric row numbers
        let twoda_bytes = include_str!("../unittest/des_treas_items.2da");
        let twoda = TwoDA::from_str(twoda_bytes, true).unwrap().1;

        assert_eq!(twoda.len_cols(), 59);
        assert_eq!(twoda.len_rows(), 25);

        assert_eq!(twoda.get_row_col(0, 0), &Some("2".to_string()));
        assert_eq!(twoda.get_row_col(1, 0), &Some("NW_WSWMGS003".to_string()));
    }
    // #[test]
    // fn test_nwn2_tips() {
    //     // Has invalid UTF8 sequences
    //     let twoda_bytes = include_bytes!("../unittest/nwn2_tips.2da");
    //     let twoda = TwoDA::from_bytes(, true).unwrap().1;
    // }

    #[test]
    fn test_twodam() {
        let twoda_bytes = include_str!("../unittest/restsys_wm_tables_merge.2da");
        let twoda = TwoDA::from_str(twoda_bytes, true).unwrap().1;
        assert_eq!(
            twoda.get_row_label(11, "TableName"),
            &Some("conflicting".to_string())
        );
        assert_eq!(twoda.get_row_merge_actions(11), None);

        assert_eq!(
            twoda.get_row_label(12, "TableName"),
            &Some("force".to_string())
        );
        assert_eq!(
            twoda.get_row_merge_actions(12),
            Some(
                [
                    MergeAction::Expect,
                    MergeAction::Set,
                    MergeAction::Set,
                    MergeAction::Ignore,
                    MergeAction::Ignore,
                    MergeAction::Ignore,
                    MergeAction::Ignore,
                    MergeAction::Ignore,
                    MergeAction::Ignore
                ]
                .as_slice()
            )
        );

        assert_eq!(
            twoda.get_row_label(19, "Comment"),
            &Some(",Death Knights, Mummified Priests, Dread Wraiths".to_string())
        );
        assert_eq!(
            twoda.get_row_label(19, "FeedBackStrRefSuccess"),
            &Some("hello world".to_string())
        );
        assert_eq!(
            twoda.get_row_merge_actions(19),
            Some(
                [
                    MergeAction::Ignore,
                    MergeAction::Expect,
                    MergeAction::Set,
                    MergeAction::Ignore,
                    MergeAction::Ignore,
                    MergeAction::Ignore,
                    MergeAction::Ignore,
                    MergeAction::Ignore,
                    MergeAction::Ignore
                ]
                .as_slice()
            )
        );

        assert_eq!(
            twoda.get_row_label(20, "TableName"),
            &Some("b_lower_vault".to_string())
        );
        assert_eq!(
            twoda.get_row_label(20, "FeedBackStrRefFail"),
            &Some("e1rref".to_string())
        );
        assert_eq!(twoda.get_row_label(20, "Enc1_ResRefs"), &None);
        assert_eq!(
            twoda.get_row_merge_actions(20),
            Some(
                [
                    MergeAction::Expect,
                    MergeAction::Ignore,
                    MergeAction::Ignore,
                    MergeAction::Set,
                    MergeAction::Set,
                    MergeAction::Set,
                    MergeAction::Set,
                    MergeAction::Set,
                    MergeAction::Set
                ]
                .as_slice()
            )
        );

        assert_eq!(
            twoda.get_row_label(22, "TableName"),
            &Some("non".to_string())
        );
        assert_eq!(twoda.get_row_label(22, "Enc1_ResRefs"), &None);
        assert_eq!(twoda.get_row_merge_actions(22), None);
    }
}
