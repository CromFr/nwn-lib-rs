#!/bin/bash
set -euo pipefail

LAST_TAG=$((git tag --sort=v:refname --list v\* | tail -n1) || echo "none")

echo "Last git version tag: $LAST_TAG"
read -p "New version: " VERSION

if [[ "$VERSION" != v*.*.* ]]; then
	echo "Versions must be like v1.2.3"
	exit 1
fi

VERSION_SEMVER="${VERSION:1}"

set -x
# Update Cargo.toml
sed -Ei 's/^\s*version\s*=\s*".+?"/version = "'"$VERSION_SEMVER"'"/' Cargo.toml tools/*/Cargo.toml
cargo build --workspace --quiet # Updates cargo.lock

git add Cargo.toml tools/*/Cargo.toml Cargo.lock
git commit -m "Bump version to $VERSION" --edit --verbose

git tag $VERSION